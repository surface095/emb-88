/*
* list3_1.c	-- blink LED
*
* Author : Osamu Tamura @ Recursion Co., Ltd.
*/

#include "common.h"

#define TM	6244

int main(void)
{
	int		i;

	init_clock(CLKPS_62500Hz);
	init_port();

	for (;;)
	{
		for(i = 0; i < TM; i++) {
			wdt_reset();
		}
		PORTB	^= 0x20;	//	LED �� ON/OFF
	}
}



