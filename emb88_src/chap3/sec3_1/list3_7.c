/*
* list3_7.c	-- Servo motor controller
*
* Author : Osamu Tamura @ Recursion Co., Ltd.
*/

/*
	接続：
		PB5 -> Servo_6 Control Signal
		PB4 -> Servo_5 Control Signal
		PB3 -> Servo_4 Control Signal
		PB2 -> Servo_3 Control Signal
		PB1 -> Servo_2 Control Signal
		PB0 -> Servo_1 Control Signal
*/

#include "common.h"

#define SERVO_MAX	6

volatile unsigned char	tick;
volatile unsigned char	n;
volatile unsigned char	angle[SERVO_MAX];

// 16mS ごとの割り込み
ISR(TIMER2_OVF_vect)
{
	//	サーボ１のパルスを L→H
	n = 0;
	PORTB = 1;
	//	サーボ１のパルス時間後に割り込み(COMPA）
	OCR2A = angle[0];
	TIFR2 = _BV(OCF2A);
	TIMSK2 |= _BV(OCIE2A);

	tick++;
}

ISR(TIMER2_COMPA_vect)
{
	if (++n >= SERVO_MAX) {
		//	サーボ n-1 のパルスを H→L
		PORTB = 0;
		//	割り込み停止
		TIMSK2 &= ~_BV(OCIE2A);
		return;
	}
	//	サーボ n-1 のパルスを H→L, n のパルスを L→H
	PORTB = 1 << n;
	//	サーボ n のパルス時間後に割り込み
	OCR2A += angle[n];
	TIFR2 = _BV(OCF2A);
}


int main(void)
{
	unsigned char	i;

	init_clock(CLKPS_1MHz);

	DDRB	= 0x3f;
	PORTB	= 0x00;

	TCCR2A	= 0x00;
	TCCR2B	= 0x04;		//	1/64
	TIMSK2	= _BV(TOIE2);

	// サーボ回転角の初期状態設定
	for (i = 0; i < SERVO_MAX; i++) {
		angle[i] = 8 + 5 * i;
	}

	sei();
	for (;;) {
		wdt_reset();

		// サーボ回転角の制御例
		if (tick >= 12) {
			tick = 0;
			for (i = 0; i < SERVO_MAX; i++) {
				if (++angle[i] >= 38) {
					angle[i] = 8;
				}
			}
		}
    }
}

