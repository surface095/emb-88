/*
* list3_5.c	-- interval execution 1
*
* Author : Osamu Tamura @ Recursion Co., Ltd.
*/

#include "common.h"


int main(void)
{
	unsigned int timeout1 = TM1;
	unsigned int timeout2 = TM2;

	init_clock(CLKPS_250kHz);
	init_port();

	TCCR1A = 0;
	TCCR1B = 3;		// 1/64

	TCNT1 = 0;
	for (;;)
	{
		wdt_reset();

		if ((int)(TCNT1 - timeout1) >= 0) {
			timeout1 += TM1;
			do_something(1);
		}

		if ((int)(TCNT1 - timeout2) >= 0) {
			timeout2 += TM2;
			do_something(2);
		}
	}
}


