/*
* list3_9.c	-- sleep mode
*
* Author : Osamu Tamura @ Recursion Co., Ltd.
*/

#include "common.h"

//	0.5秒ごとにスリープを解除
ISR(TIMER1_COMPA_vect)
{
	do_something(1);
}

int main(void)
{
	wdt_enable(WDTO_2S);
	init_clock(CLKPS_250kHz);
	init_port();

	/*	CLK = 250kHz, 2400bps 8N1	*/
	UBRR0	= (unsigned char)((250000 >> 3) / 2400 - 1);
	UCSR0A	= (1 << U2X0);
	UCSR0C	= 0x06;
	UCSR0B	= (1 << TXEN0);

	TCCR1A	= 0;
	TCCR1B	= 0x0d;		// CTC, 1/1024
	OCR1A	= 121;		// 0.5mS
	TCNT1	= 0;
	TIMSK1	|= (1<<OCIE1A);

	set_sleep_mode(SLEEP_MODE_IDLE);
	sei();
	for (;;)
	{
		wdt_reset();
		// スリープモード開始
		sleep_mode();

		//　割り込み後に以下も実行される
		do_something(5);
		UDR0 = '@';
	}
}


