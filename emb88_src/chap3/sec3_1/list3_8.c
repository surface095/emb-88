/*
* list3_8.c	-- Ultrasonic distance meter
*
* Author : Osamu Tamura @ Recursion Co., Ltd.
*/

#include "common.h"

volatile unsigned short		count;
volatile unsigned char		ready;

// ７セグメント表示パターン
volatile const unsigned char seg7[16] = {
	0xcf, 0x81, 0xd6, 0xd3, 0x99, 0x5b, 0x5f, 0xc9,
	0xdf, 0xdb, 0xdd, 0x1f, 0x5f, 0x97, 0x5e, 0x5c
};
volatile unsigned char		buf[4];

// 524mS ごとの割り込み
ISR(TIMER1_OVF_vect)
{
	// トリガ開始
	PORTD	|= 0x80;
	TIFR1	 = _BV(OCF1A);
	TIMSK1	|= _BV(OCIE1A);
}

// 50μS 後に割り込み
ISR(TIMER1_COMPA_vect)
{
	// トリガ終了
	PORTD	&= ~0x80;
	TIMSK1	&= ~_BV(OCIE1A);

	// 入力パルス待ち
	TCCR1B	|= _BV(ICES1);
	TIFR1	 = _BV(ICF1);
	TIMSK1	|= _BV(ICIE1);
}

// 入力パルス変化時に割り込み
ISR(TIMER1_CAPT_vect)
{
	// パルス幅のカウント開始
	if (TCCR1B & _BV(ICES1)) {
		count	= ICR1;
		TCCR1B	&= ~_BV(ICES1);
	}
	// パルス幅のカウント終了
	else {
		count	= ICR1 - count;
		ready	= 1;
		TIMSK1	&= ~_BV(ICIE1);
	}
}

// ７セグメント表示器の４桁ダイナミックスキャン
ISR(TIMER1_COMPB_vect)
{
	static unsigned char	digit = 0;
	unsigned char			n;

	// 4mS ごとに桁移動
	OCR1B	= TCNT1 + 499;

	n	= buf[3 - digit];
	PORTB = (PORTB & 0x03) | (n << 2);
	PORTD = (PORTD & 0xc3) | (n >> 2);
	PORTC = ~(1 << digit);
	digit = (digit + 1) & 3;
}

int main(void)
{
	unsigned short	x;
	short			n;
	unsigned char	i;

	init_clock(CLKPS_1MHz);
	DDRB	= 0x3c;
	DDRC	= 0x0f;
	DDRD	= 0xfe;
	PORTC	= 0x00;

	OCR1A	= 5;
	TCCR1A	= 0x00;
	TCCR1B	= _BV(ICNC1) | 0x02;	//	1/8 CLK
	TIMSK1	= _BV(TOIE1) | _BV(OCIE1B);

	sei();
	for (;;) {
		wdt_reset();

		if (ready) {
			ready = 0;

			// 距離の算出 (cm)
			x = count * 4 / 3;

			// 整数値を７セグメント表示パターンへ変換
			for (i = 0; i < 4; i++) {
				// ゼロブランキング
				if (i >= 2 && x == 0) {
					buf[i] = 0;
					continue;
				}
				n = x / 10;
				buf[i] = seg7[x - n * 10];
				x = n;
			}
			// 小数点
			buf[1]	|= 0x20;
		}
	}
}


