/*
* list3_10.c	-- clock1
*
* Created: 2017/01/11 16:56:35
* Author : Osamu Tamura @ Recursion Co., Ltd.
*
* Usage  :
*	AtmelStudio / Tools -> Command Prompt
*		con2com com3 38400 list3_10.hex /
*		con2com com3 600
*/

#include <stdio.h>
#include "common.h"

ISR(TIMER2_COMPA_vect)
{
}

int main(void)
{
	long		sec;
	char		time[12];

	init_clock(CLKPS_62500Hz);

	// Timer2�i1Sec�j
	OCR2A	= (62500 >> 8) - 1;
	TCCR2A	= 0x02;
	TCCR2B	= 0x06;	// 1/256
	TIMSK2	= (1 << OCIE2A);

	display_init();
	set_sleep_mode(SLEEP_MODE_PWR_SAVE);
	sei();
	for (sec = 0;; sec++)
	{
		wdt_reset();
		sleep_mode();
		do_something(1);

		sprintf(time, "%02d:%02d:%02d\r\n",
		(int)(sec / 3600) % 24,	// ��
		(int)(sec / 60) % 60,	// ��
		(int)(sec % 60));		// �b

		display_data(time, 0, 10);
	}
}
