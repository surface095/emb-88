/*
* list3_6.c	-- event driven 2
*
* Author : Osamu Tamura @ Recursion Co., Ltd.
*/

#include "common.h"


int main(void)
{
	int count;
	int timeout1 = TM1;
	int timeout2 = TM2;

	init_clock(CLKPS_125kHz);
	init_port();

	for (count = 0;; count++)
	{
		wdt_reset();

		if (count == timeout1) {
			timeout1 += TM1;
			do_something(1);
		}

		if (count == timeout2) {
			timeout2 += TM2;
			do_something(2);
		}
	}
}


