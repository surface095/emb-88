/*
* list3_6.c	-- event driven 1
*
* Author : Osamu Tamura @ Recursion Co., Ltd.
*/

#include "common.h"


int main(void)
{
	int count1 = TM1;
	int count2 = TM2;

	init_clock(CLKPS_62500Hz);
	init_port();

	for (;;)
	{
		wdt_reset();

		if (--count1 == 0) {
			count1 = TM1;
			do_something(1);
		}

		if (--count2 == 0) {
			count2 = TM2;
			do_something(2);
		}
	}
}
