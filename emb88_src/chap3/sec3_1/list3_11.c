/*
* list3_11.c	-- clock2
*
* Created: 2017/01/11 16:56:35
* Author : Osamu Tamura @ Recursion Co., Ltd.
*
* Usage  :
*	AtmelStudio / Tools -> Command Prompt
*		con2com com3 38400 list3_11.hex /
*		con2com com3 600
*/

#include <stdio.h>
#include "common.h"

ISR(TIMER2_COMPA_vect)
{
}

int main(void)
{
	unsigned char	n;
	char			time[12];

	init_clock(CLKPS_62500Hz);
	
	// Timer2�i1Sec�j
	OCR2A	= (62500 >> 8) - 1;
	TCCR2A	= 0x02;
	TCCR2B	= 0x06;	// 1/256
	TIMSK2	= (1 << OCIE2A);

	display_init();

	strcpy(time, "23:59:49\r\n");
	display_data(time, 0, 10);

	set_sleep_mode(SLEEP_MODE_PWR_SAVE);
	sei();
	for (;;)
	{
		wdt_reset();
		sleep_mode();

		n = 7;
		if (++time[n] > '9')
		{
			time[n--] = '0';
			if (++time[n] > '5')
			{
				time[n--] = '0';
				n--;
				if (++time[n] > '9')
				{
					time[n--] = '0';
					if (++time[n] > '5')
					{
						time[n--] = '0';
						n--;
						if (++time[n] > '9')
						{
							time[n--] = '0';
							++time[n];
						}
						else if (time[n] == '4' && time[n-1] == '2')
						{
							time[n--] = '0';
							time[n] = '0';
						}
					}
				}
			}
		}

		display_data(time, n, 8 - n);
	}
}
