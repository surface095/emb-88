/*
 * common.c
 *
 * Author : Osamu Tamura @ Recursion Co., Ltd.
 */

#include "common.h"

inline unsigned char SW()
{
	unsigned char t = ~PINC;
	return (t >> 4) & 3;
}

void init_clock(unsigned char scale)
{
	CLKPR = (1<<CLKPCE);
	CLKPR = scale;

	wdt_enable(WDTO_2S);
}

void init_port()
{
	DDRB = 0xff;
	DDRC = 0x0f;
	DDRD = 0xfa;
	PORTB = 0;
	PORTC = 0x30;
	PORTD = 0;
}

void do_something(unsigned char x)
{
	PORTB ^= (1 << x); //x==1? 1:0x80;
}


void display_init(void)
{
	UCSR0B	= 0;

	/*	CLK = 62500Hz, 600bps	*/
	UBRR0	= (62500 >> 3) / 600 - 1;
	UCSR0A	= (1 << U2X0);
	/*  data 8bit, no parity, 1 stopbit */
	UCSR0C	= 0x06;
	UCSR0B	= (1 << TXEN0);

	DDRD	|= 0x04;	// CTS
	PORTD	&= ~0x04;
}

void display_data(char *data, unsigned char offset, unsigned char num)
{
	static char		tmp[12];
	unsigned char	i;

	//	更新部分のみバッファにコピー
	memcpy(tmp + offset, data + offset, num);
	//	バッファ全体をシリアル送出
	for (i = 0; i < 10; i++) {
		while ((UCSR0A & (1 << UDRE0)) == 0) {
			wdt_reset();
		}
		UDR0 = tmp[i];
	}
	//	送出完了まで待つ
	UCSR0A |= (1 << TXC0);
	while ((UCSR0A & (1 << TXC0)) == 0) {
		wdt_reset();
	}
}
