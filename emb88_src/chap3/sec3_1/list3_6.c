/*
* list3_6.c	-- interval execution 3
*
* Author : Osamu Tamura @ Recursion Co., Ltd.
*/

#include "common.h"

enum {
	EVENT_PC0 = 1 << 0,
	EVENT_TM1 = 1 << 1,
	EVENT_TM2 = 1 << 2,
};

volatile unsigned int event = 0;

ISR(PCINT1_vect)
{
	if ((PINC & 0x10) == 0) {
//		PCICR &= ~(1 << PCIE1);	// 他のピン変化割り込みを禁止 -> 削除
		event |= EVENT_PC0;		// イベントフラグをセット
	}
}

ISR(TIMER1_COMPA_vect) {
	static unsigned int count = 0;
	static unsigned int timeout1 = TM1;
	static unsigned int timeout2 = TM2;

	count++;

	if (count == timeout1) {
		timeout1 += TM1;
		event |= EVENT_TM1;		// イベントフラグをセット
	}

	if (count == timeout2) {
		timeout2 += TM2;
		event |= EVENT_TM2;
	}
}

int main(void)
{
	init_clock(CLKPS_250kHz);
	init_port();

	// ピン変化割り込み始動
	PCICR	|= (1 << PCIE1);
	PCMSK1	|= (1 << PCINT12);

	// タイマ割り込み始動
	TCCR1A	= 0;
	TCCR1B	= 0x09;		// 1/1
	OCR1A	= 64;
	TCNT1	= 0;
	TIMSK1	|= (1 << OCIE1A);

	sei();
	for (;;)
	{
		wdt_reset();

		if (event) {

			if (event & EVENT_PC0) {
				do_something(0);	// ピン変化での処理本体
//				PCICR |= (1 << PCIE1);	-> 削除
				event &= ~EVENT_PC0;
			}

			if (event & EVENT_TM1) {
				do_something(1);	// タイムアウト時の処理本体
				event &= ~EVENT_TM1;
			}

			if (event & EVENT_TM2) {
				do_something(2);
				event &= ~EVENT_TM2;
			}
		}
	}
}


