/*
* list3_5.c	-- interval execution 2
*
* Author : Osamu Tamura @ Recursion Co., Ltd.
*/

#include "common.h"

#define TM3		7
#define TM4		11

//	100mS �Ԋu�̊��荞��
ISR(TIMER1_COMPA_vect) {
	static unsigned int count = 0;
	static unsigned int timeout1 = TM3;
	static unsigned int timeout2 = TM4;

	count++;

	if (count == timeout1) {	// 0.7 �b�Ԋu
		timeout1 += TM3;
		do_something(1);
	}

	if (count == timeout2) {	// 1.1 �b�Ԋu
		timeout2 += TM4;
		do_something(2);
	}
}

int main(void)
{
	init_clock(CLKPS_250kHz);
	init_port();

	TCCR1A	= 0;		// CTC
	TCCR1B	= 0x09;		// 1/1
	OCR1A	= 24999;	// 250kHz/25000 = 10Hz;
	TCNT1	= 0;
	TIMSK1	|= (1<<OCIE1A);

	sei();
	for (;;)
	{
		wdt_reset();
	}
}



