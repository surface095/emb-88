/*
	utility.c	- utilitiy routines for BASIC

	Copyright(c) 2017-2018 Osamu Tamura @ Recursion Co., Ltd.
					All rights reserved.
*/

#include "basic.h"

#define	NBUF_SZ		12

char	nbuf[NBUF_SZ];


data_t _atoi(unsigned char len)
{
	char	*ptr = nbuf;
	data_t	n = 0;

	if (*ptr == OP_HEX) {
		for (ptr++, len--; *ptr && len; ptr++, len--) {
			n = (n << 4) | (*ptr & 0x0f);
			if (*ptr >= 'A') {
				n += 9;
			}
		}
	}
	else {
		char	minus = 0;

		if (*ptr == '-') {
			ptr++;
			minus = 1;
		}
		for (; *ptr && len; len--) {
			n = n * 10 + (*ptr++ - '0');
		}
		if (minus) {
			n = -n;
		}
	}

	return n;
}

char *_itoa(data_t n)
{
	char	*ptr = nbuf + NBUF_SZ;
	data_t	d;
	char	minus = 0;

	*--ptr = 0;

	if (n < 0) {
		n = -n;
		minus = 1;
	}
	do {
		d = n / 10;
		*--ptr = (char)('0' + (n - d * 10));
		n = d;
	} while (n);

	if (minus) {
		*--ptr = '-';
	}

	return ptr;
}

char *_htoa(data_t n, unsigned char digit)
{
	char	*ptr = nbuf + NBUF_SZ;
	char	d;

	if (!digit) {
		digit = n & ~0xff ? 4 : 2;
	}
	*--ptr = 0;
	do {
		d = n & 0x0f;
		if (d > 9) {
			d += 'a' - '0' - 10;
		}
		*--ptr = (char)('0' + d);
		n >>= 4;
	} while (--digit);
	for (; digit; digit--) {
		*--ptr = '0';
	}

	return ptr;
}

