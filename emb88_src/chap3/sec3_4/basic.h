/*
	basic	- basic.h

	Copyright(c) 2017-2018 Osamu Tamura @ Recursion Co., Ltd.
					All rights reserved.
*/

#include <stddef.h>

#ifndef NULL
#define NULL	(void *)0
#endif

#define	MAX_SYM		27
#define	MAX_LABEL	32
#define	MAX_STACK	32

#ifdef AVR
#define	assert(x,y)
#else
#define	assert(x,y)	if(x){err=y;}
#define _CRT_SECURE_NO_WARNINGS

#define	MAX_CODE	2048
#endif

enum {
	SYS_NOP = 0,
	SYS_DO,
	SYS_ELSE,
	SYS_END,
	SYS_ENDIF,
	SYS_EXIT,
	SYS_GOSUB,
	SYS_IF,
	SYS_INKEY,
	SYS_LOOP,
	SYS_PRINT,
	SYS_RETURN,
	SYS_THEN,
	SYS_NULL,
};

enum {
	OP_NOT = '!',
	OP_STR = '"',
	OP_HEX = '#',
	OP_BYTE = '$',
	OP_MOD = '%',
	OP_AND = '&',
	OP_REM = '\'',
	OP_LP = '(',
	OP_RP = ')',
	OP_MUL = '*',
	OP_ADD = '+',
	OP_SUB = '-',
	OP_DIV = '/',

	OP_LBL = ':',

	OP_LT = '<',
	OP_LET = '=',
	OP_GT = '>',

	OP_REG = '@',

	OP_LB = '[',
	OP_CHR = '\\',
	OP_RB = ']',
	OP_XOR = '^',
	OP_NUM = '`',

	OP_NEG = 'a',
	OP_SFL,
	OP_SFR,
	OP_SFU,
	OP_LE,
	OP_GE,
	OP_EQ,
	OP_NE,
	OP_LAND,
	OP_LOR,

	OP_ARY = '{',
	OP_LC = '{',
	OP_OR = '|',
	OP_RC = '}',
	OP_COM = '~',
};

typedef unsigned short addr_t;
typedef short data_t;
typedef volatile unsigned char sfr_t;


extern data_t			parse(unsigned char *code, data_t size);
extern data_t			eval(data_t *data);
extern void				list(void);

/*	bios.c	*/
extern sfr_t			sfr_get(data_t addr);
extern sfr_t			sfr_set(data_t addr, unsigned char data);
extern char				_timeout(void);
extern void				timer_init(void);
extern data_t			_getchar(void);
extern data_t			_getc(void);
extern void				_putc(char c);
extern void				_puts(char *s);
extern void				_puts_P(const char *s, unsigned char offset);
extern unsigned char	conio_init(void);

/*	pgm.c	*/
extern unsigned char	_this(void);
extern unsigned char	_next(void);
extern void				_skip(void);
extern unsigned char	_after(data_t x);
extern data_t			_operand(void);
extern data_t			p_byte(data_t x);
extern data_t			p_word(data_t x);
extern addr_t			p_addr(data_t x);
extern void				memcpy2_P(unsigned char *buf, size_t size);

/*	utility.c	*/
extern data_t			_atoi(unsigned char len);
extern char				*_itoa(data_t n);
extern char				*_htoa(data_t n, unsigned char digit);
extern void				dump_P(void);


extern const addr_t			*label;
extern const unsigned char	*prog;
extern const unsigned char	*p;

extern char					nbuf[];
