/*
	basic	- main.c

	Copyright(c) 2017-2018 Osamu Tamura @ Recursion Co., Ltd.
					All rights reserved.
*/

#include "basic.h"

#define	TITLE	"\n\tBASIC interpreter by Osamu Tamura\n"

#ifdef AVR
#include <stdlib.h>
#include <avr/pgmspace.h>
#include <avr/eeprom.h>
#include <avr/io.h>

#else
#define PSTR(x)	x

static unsigned char	heap[MAX_CODE];
#endif

static void show_size(void);


int main( void )
{
	data_t		c;
	data_t		err;
	data_t		sz;
#ifdef AVR
	unsigned char	*heap = (unsigned char *)__malloc_heap_start;
	data_t	szmax = (RAMEND + 1 - 0x40) - (addr_t)__malloc_heap_start;
#else
	data_t	szmax = MAX_CODE;
#endif

	if (!conio_init()) {
		/*	auto-start	*/
		eval((data_t *)heap);
	}

	_puts_P(PSTR(TITLE), 0);

	for (;;) {
		_putc('>');
		do {
			c = _getc();
		} while (c <= ' ');
		_putc((char)c);
		_putc('\n');

		switch (c) {
			case 'n':	// new
						_putc('#');
						sz = parse(heap, szmax);
						memcpy2_P(heap, sz);
						show_size();
						break;

			case 'l':	// list
						list();
						show_size();
						break;

			case 'r':	// run
						err = eval((data_t *)heap);
						if (err) {
							_puts_P(PSTR("\n\terror: "), 0);
							_puts(_itoa(err));
							_putc('\n');
						}
						break;

			case 'd':	// dump
						dump_P();
						show_size();
						break;
#ifdef AVR
			case 'a':	// set auto-start
						eeprom_write_byte(0, 0);
						break;

			case 't':	// test
						*(sfr_t *)0x23 = 0xff;
						break;
#else
			case 'q':	// quit
						goto end;
#endif
			default:	// ?
						_putc('?');
		}
	}
#ifndef AVR
end:
#endif
	   return 0;
}

static void show_size(void)
{
	_puts_P(PSTR("\n\tsize: "), 0);
	_puts(_itoa((data_t)p_addr(0)));
	_putc('\n');
	_putc('\n');
}

