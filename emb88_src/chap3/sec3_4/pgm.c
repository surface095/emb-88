/*
	basic	- pcode.c

	Copyright(c) 2017-2018 Osamu Tamura @ Recursion Co., Ltd.
					All rights reserved.
*/

#include "basic.h"

#ifdef AVR
#include <avr/boot.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <avr/wdt.h>

const unsigned char	code[2] __attribute__ ((section (".prog"))) = { 0, 0 };
void memcpy2_P(unsigned char *buf, size_t size) __attribute__ ((section (".boot")));

const addr_t		*label = (addr_t *)code;
const unsigned char	*prog = code + sizeof(addr_t) * MAX_LABEL;


unsigned char _this(void)
{
	return pgm_read_byte(p);
}

unsigned char _next(void)
{
	return pgm_read_byte(p++);
}

void _skip(void)
{
	p += pgm_read_byte(p) + 1;
}

unsigned char _after(data_t x)
{
	return pgm_read_byte(p+x);
}

data_t _operand(void)
{
	data_t	t = pgm_read_word(p);
	p += sizeof(data_t);
	return t;
}

data_t p_byte(data_t x)
{
	return (data_t)pgm_read_byte(prog + x);
}

data_t p_word(data_t x)
{
	return (data_t)pgm_read_word((const short *)(prog + x));
}

addr_t p_addr(data_t x)
{
	return (addr_t)pgm_read_word(label + (x & 0x1f));
}

void memcpy2_P(unsigned char *buf, size_t size)
{
	const unsigned char *page = code;
	unsigned char	i, len;

	cli();
	size = (size + 1) & ~1;
	for (; size; page += SPM_PAGESIZE) {
		wdt_reset();

		eeprom_busy_wait ();

		boot_page_erase (page);
		boot_spm_busy_wait ();      // Wait until the memory is erased.

		//	Fill the bootloader temporary page buffer for flash
		// address with data word.
		len = size < SPM_PAGESIZE? size:SPM_PAGESIZE;
		for (i = 0; i < len; i += 2) {
            // Set up little-endian word.
            uint16_t w = *buf++;
            w += (*buf++) << 8;
			boot_page_fill(i, w);
		}
		for (; i < SPM_PAGESIZE; i += 2) {
			//			boot_page_fill(i, 0xffff);
		}
		size -= len;

        boot_page_write (page);     // Store buffer in flash page.
        boot_spm_busy_wait();       // Wait until the memory is written.
	}

    // Re-enable RWW-section again. We need this if we want to jump back
    // to the application after bootloading.
	boot_rww_enable();
	sei();
}

#else

static unsigned char	code[MAX_CODE] = { 0, 0, };

const addr_t		*label = (addr_t *)code;
const unsigned char	*prog = code + sizeof(addr_t) * MAX_LABEL;


unsigned char _this(void)
{
	return *p;
}

unsigned char _next(void)
{
	return *p++;
}

void _skip(void)
{
	p += *p + 1;
}

unsigned char _after(data_t x)
{
	return *(p + x);
}

data_t _operand(void)
{
	const data_t	t = *(data_t *)p;
	p += sizeof(data_t);
	return t;
}

data_t p_byte(data_t x)
{
	return (data_t)*(prog + x);
}

data_t p_word(data_t x)
{
	return *((data_t *)(prog + x));
}

addr_t p_addr(data_t x)
{
	return *(label + (x & 0x1f));
}

void memcpy2_P(unsigned char *buf, size_t size)
{
	unsigned char	*b = code;

	while (size--) {
		*b++ = *buf++;
	}
}
#endif


void dump_P(void)
{
	int		i, size;

	p = code;
	size = sizeof(addr_t) * MAX_LABEL + p_addr(0);
	for (i = 0; i < size; i++) {
		_putc(i & 15? '-':'\n');
		if (((i - (sizeof(addr_t) * MAX_LABEL)) & 255) == 0) {
			_putc('\n');
		}
		_puts(_htoa(_next(), 2));
	}
	_putc('\n');
}
