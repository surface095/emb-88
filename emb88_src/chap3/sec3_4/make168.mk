################################################################################
#	BASIC interpreter for AVR
#
#	Osamu Tamura @ Recursion Co., Ltd.
################################################################################

# プロジェクト名
PROJECT = basic168

# ビルドに必要なオブジェクトファイル名
OBJECTS = bios.o eval.o list.o main.o parse.o pgm.o utility.o

# MPU や Clock が異なる場合は変更（Arduino なら atmega328p, 16000000UL）
#MPU = atmega88pa
MPU = atmega168p
#MPU = atmega328p
F_CPU = 8000000UL
#F_CPU = 16000000UL


MAPFILE = ${PROJECT}.map
LSSFILE = ${PROJECT}.lss
ELFFILE = ${PROJECT}.elf
HEXFILE = ${PROJECT}.hex


CFLAGS = -mmcu=${MPU} -DF_CPU=${F_CPU} -mrelax -g2 -Wall
CFLAGS += -funsigned-char -funsigned-bitfields
CFLAGS += -Os -ffunction-sections -fdata-sections -fpack-struct -fshort-enums -mshort-calls


# セクション開始は .boot=0x1f00(atmega88pa), .boot=0x7f00(atmega328p)
LDFLAGS = -mmcu=${MPU} -Wl,-Map=${MAPFILE} -mrelax -Wl,-lm -Wl,--gc-sections -Wl,--start-group -Wl,--end-group
#LDFLAGS += -Wl,-section-start=.prog=0x1b00 -Wl,-section-start=.boot=0x1f00 
LDFLAGS += -Wl,-section-start=.prog=0x1b00 -Wl,-section-start=.boot=0x3f00 
#LDFLAGS += -Wl,-section-start=.prog=0x1b00 -Wl,-section-start=.boot=0x7f00 


# All Target
all: $(ELFFILE)


CC = avr-gcc.exe
RM = rm


OUTPUT_FILE_PATH :=basic.elf


.c.o:
	@echo Building file: $<
	${CC} -c ${CFLAGS} -o $@ $<
	@echo .
	

$(ELFFILE): $(OBJECTS)
	@echo Building target: $@
	${CC} ${LDFLAGS} ${OBJECTS} -o ${ELFFILE}
	@echo .
	avr-objcopy -O ihex -R .eeprom -R .fuse -R .lock ${ELFFILE} ${HEXFILE}
	avr-objdump -h -S ${ELFFILE} > ${LSSFILE}
	@echo .
	avr-size -C --mcu=${MPU} ${ELFFILE}


distclean: clean
	$(RM) -f ${HEXFILE}

clean:
	$(RM) -f ${OBJECTS} ${MAPFILE} ${LSSFILE} ${ELFFILE}
	