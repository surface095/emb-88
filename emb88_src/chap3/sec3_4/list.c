﻿/*
	basic	- list.c

	Copyright(c) 2017-2018 Osamu Tamura @ Recursion Co., Ltd.
					All rights reserved.
*/

#include "basic.h"

extern const char			*syskeys[];

#ifdef AVR
#include <avr/pgmspace.h>

static const char _t00[] PROGMEM = "-";
static const char _t01[] PROGMEM = "<<";
static const char _t02[] PROGMEM = ">>";
static const char _t03[] PROGMEM = ">>>";
static const char _t04[] PROGMEM = "<=";
static const char _t05[] PROGMEM = ">=";
static const char _t06[] PROGMEM = "==";
static const char _t07[] PROGMEM = "!=";
static const char _t08[] PROGMEM = "&&";
static const char _t09[] PROGMEM = "||";

static PGM_P const op[] PROGMEM = {
	_t00, _t01, _t02, _t03, _t04, _t05, _t06, _t07, _t08, _t09
};

#define	strchr(a,b)	strchr_P(PSTR(a),b)

#else
#include <string.h>

static const char *op[] = {
	"-", "<<", ">>", ">>>", "<=", ">=", "==", "!=", "&&", "||"
};
#endif


static data_t next_label(data_t index, addr_t here)
{
	data_t		i, n;
	addr_t		m, mx;

	mx = p_addr(0);
	n = 0;
	for (i = 1; i < MAX_LABEL; i++) {
		m = p_addr(i);
		if (m == here && i > index) {
			return i;
		}
		if (here < m && m < mx) {
			n = i;
			mx = m;
		}
	}
	return n;
}

static void draw_indent(unsigned char n)
{
	for (; n; n--) {
		_putc('\t');
	}
}

static void draw_pstring(void)
{
	unsigned char	t = _next();

	_puts_P((const char *)p, 0);
	p += t;
}


static void _space(void)
{
	_putc(' ');
}

static void _lf(void)
{
	_putc('\n');
}


void list(void)
{
	const unsigned char	*cp;
	unsigned char	c, cm, b, tb;
	unsigned char	i, j, nl, pnl;
	data_t		k, n;
	addr_t		r;

	
	tb = 1;
	pnl = 0;
	cp = prog + (data_t)p_addr(0);
	b = 0;
	cm = 0;

	//	find the 1st label
	k = next_label(0, 0);
	r = p_addr(k);

	for (p = prog; p < cp;) {
		nl = 0;

		c = _next();

		//	label
		if (r == (addr_t)(p - prog - 1)) {
			p--;
			if (!pnl) {
				_lf();
			}
			n = (data_t)('@' + k);
			if (k >= MAX_SYM) {
				_putc('_');
				n -= 0x12;
			}
			_putc((char)n);
			_putc(':');
			k = next_label(k, r);
			r = p_addr(k);
			nl++;
			goto tail;
		}

		//	comment
		if (c == OP_REM) {
			if (!pnl) {
				_lf();
			}
			_putc(c);
			p++;
			c = _this();
			p--;
			if (c != '\t') {
				draw_indent(tb);
			}
			else {
				_putc(OP_REM);
			}
			draw_pstring();
			nl++;
			goto tail;
		}

		//	string
		if (c == OP_STR) {
			_space();
			_putc(c);
			draw_pstring();
			_putc(c);
			goto tail;
		}

		//	array
		if (c == OP_LC) {
			i = _next() / sizeof(data_t);
			_space();
			_putc(c);
			for (j = 0; i; j++) {
				if (j & 7) {
					_space();
				}
				else {
					_lf();
					draw_indent(tb + 1);
				}
				_putc(OP_HEX);
				_puts(_htoa(_operand(), 0));
				if (--i)
					_putc(',');
			}
			_lf();
			draw_indent(tb);
			_putc('}');
			nl++;
			goto tail;
		}

		//	number (dec/hex/SFR)
		if (c == OP_NUM ||
			c == OP_HEX ||
			c == OP_REG) {
			n = _operand();
//			if (pgm_read_byte(p) == OP_LET) {
			if (_this() == OP_LET) {
				if (!pnl) {
					_lf();
					pnl = 1;
				}
				draw_indent(tb);
			}
			else if (strchr("[(", b) == NULL) {
				_space();
			}

			if (c == OP_NUM) {
				_puts(_itoa(n));
			}
			else {
				_putc(c);
				_puts(_htoa(n, 0));
			}
			goto tail;
		}

		//	reserved words
		if (c < SYS_NULL) {
			const char	*s;

			//	pre LF
			if (!pnl &&
				!(cm == SYS_IF && c == SYS_EXIT) &&
				c != SYS_THEN) {
				_lf();
				pnl = 1;
			}
			if (pnl) {
				//	outdent
				if (c == SYS_ELSE ||
					c == SYS_ENDIF ||
					c == SYS_LOOP ) {
					tb--;
				}
				draw_indent(tb);
			}
			else {
				_space();
			}

#ifdef AVR
			s = pgm_read_ptr(&syskeys[c]);
			_puts_P(s, 0x20);
#else
			s = syskeys[c];
			while (*s != 0)
				_putc(*s++ | 0x20);
#endif
			if (c == SYS_DO ||
				c == SYS_ELSE ||
				c == SYS_THEN) {
				p += sizeof(addr_t);	//	skip offset
				tb++;					//	indent
			}

			if (c == SYS_GOSUB) {
				_space();
				_putc(_next());
			}
			if (c == SYS_INKEY) {
				_space();
				_putc(_next() | 0x20);
			}

			//	post LF
			if (c != SYS_PRINT &&
				c != SYS_IF) {
				nl++;
			}

			cm = c;
			goto tail;
		}

		//	identifier
		if ('A' <=c && c <= 'Z') {
			if (!pnl &&
				strchr("A#@`$;\\])\"", b) != NULL) {
				_lf();
				pnl = 1;
			}
			if (!pnl &&
				(strchr("[(", b) == NULL &&
				strchr("$,;\\[])", c) == NULL)) {
				_space();
			}
			if (pnl) {
				draw_indent(tb);
			}

			_putc(c | 0x20);
			c = 'A';
			goto tail;
		}

		//	operator (multi characters)
		if (OP_NEG <= c && c <= OP_LOR) {
			const char	*o;

			_space();
#ifdef AVR
			o = pgm_read_ptr(&op[c - OP_NEG]);
			_puts_P(o, 0);
#else
			o = op[c - OP_NEG];
			while (*o) {
				_putc(*o++);
			}
#endif
			goto tail;
		}

		//	operator (single character)
		if (!pnl &&
			!(b == c && b =='(') &&
			strchr("$,;\\[])", c) == NULL) {
			_space();
		}
		_putc(c);

tail:
		if (nl) {
			_lf();
		}
		pnl = nl;
		b = c;
	}
}
