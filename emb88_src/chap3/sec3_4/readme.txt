

		BASIC Interpreter for AVR


１．書き込み

BASIC インタプリタはフラッシュメモリへの自己書き込みを行うため、
SPI プログラマで書き込みます。

(a) Atmel Studio / Tools -> Device Programming から書き込む（校正なし）

・書き込み器：	AVRISP mkII, AVR Dragon

・ヒューズビット：

MCU		LOW-HIGH-EXTENDED	LockBits	HEX		校正書き込み
ATmega88PA	E2 - DE - FF		EF		basic.hex	basic.bat
ATmega168PA	E2 - DE - FF		EF		basic168.hex	basic328.bat
ATmega328P	E2 - DD - FE		EF		basic328.hex	basic168.bat


(b) AtmelStudio / Tools -> Command Prompt から書き込む（校正あり）

書き込み時に内蔵発振器の校正パラメータを埋め込むことができます。
校正書き込み用のバッチファイルをコマンドプロンプトから起動します。
バッチファイルは AVRISP mkII 用です。AVR Dragon では始めのコメント行を入れ替えてください。


２．使い方

con2com などのターミナルソフトから使います。設定は 38400bps, 8N1。

・コマンド

n:	プログラムの読み込み (New)
	ターミナルソフトへコピー＆ペーストでプログラムテキストを送る
	転送中にドット表示。正しく読み込まれると復元リストを表示

l:	中間コードからプログラムを復元して表示 (List)

r:	プログラムの実行 (Run)

a:	オートスタート (Auto-Start)
	書き込まれたプログラムを直ちに開始
	SW1 を押しながらリセットすれば解除

d:	中間コードのメモリイメージの表示 (Dump)

t:	テスト（マトリクスLED の全灯・消灯） (Test)

※ プログラムが end および区切り文字で終端されていないと読み込み待ちとなります。
　ピリオドで打ち切ります。

※ 実行を止めるときは ESC キーでリセットします。

※ 中間コードの最大サイズは 840（ATmega88/168), 1840 (ATmega328) バイトです。

※ プログラム中では SFR は @## としてメモリアドレスでアクセスします。
　データシートのレジスタ一覧を参照してください。



(c)Copyright 2018 Osamu Tamura @ Recursion Co., Ltd. All rights reserved.
