:start
cls

@setlocal

@set prog=atprogram -t avrispmk2 -i isp -d atmega328p -cl 125khz
@rem set prog=atprogram -t avrdragon -i isp -d atmega328p -cl 125khz

@echo off
	@ECHO *********************************************************
	@ECHO  Batch file for calibration of Atmel AVR Mega328
	@ECHO  oscillator through the ISP interface

	@ECHO  - The internal RC is calibrated to value and accuracy 
	@ECHO    given in RC_Calibration.asm (fuses set for 8 MHz)
	@ECHO  - Programming FLASH and Fuses is performed initially.
	@ECHO *********************************************************
	
	@ECHO ---------------------------------------------------------
	@ECHO.
	@ECHO   ** S T A R T   P R O G R A M M I N G **
	@ECHO.		
@ECHO ---------------------------------------------------------

%prog% program -c -f rc_calib328.hex write -lb --values ff write -fs --values e2d9ff

@IF ERRORLEVEL ==1 GOTO prog_Calib_code_error
		
	@ECHO.
	@ECHO  ** Start Calibration

@ECHO ---------------------------------------------------------

%prog% calibrate

@IF ERRORLEVEL ==1 GOTO Calibration_error

@ECHO ---------------------------------------------------------
	@ECHO.
	@ECHO Verify that OSCCAL value is different from 0xFF.
	@ECHO Note, this test is intended to fail. If it does
	@ECHO not fail, OSCCAL equals 0xFF which is considered 
	@ECHO an error.

%prog% read -os --format hex
%prog% verify -ee -s 1 --format bin -f ff.bin

@IF ERRORLEVEL ==1 GOTO continue

@GOTO EEPROM_OSCCAL_value_error

:continue
		
@ECHO           ^^ Ignore Error above ^^
@ECHO.
@ECHO ---------------------------------------------------------
	@ECHO Read out new OSCCAL value from EEPROM and erase the device.
	@ECHO Program in customers code to FLASH, in this case boot.hex.
	@ECHO Verify programming of customers code.
	@ECHO Program in new OSCCAL value in flash at byte addr 01FFF.
	@ECHO Verify programming of new OSCCAL value in Flash.

%prog% read -ee -s 1 --format bin -f osc.bin
%prog% program -c -f basic328.hex write -fs --values e2ddfe write -lb --values ef
%prog% program -fl -f osc.bin -o 0x7FFF
%prog% verify -fl -s 1 -o 0x7FFF --format bin -f osc.bin

@IF ERRORLEVEL ==1 GOTO prog_customer_code_error

	@ECHO.
	@ECHO *********************************************************
	@ECHO 		P R O G R A M M I N G   O K
	@ECHO *********************************************************
	@GOTO END

:prog_Calib_code_error
	@ECHO.
@ECHO ---------------------------------------------------------
	@ECHO.
	@ECHO 		E R R O R		
	@ECHO Programming calibration program to AVR failed.
	@ECHO Programming aborted.
	@GOTO END
@ECHO !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


:Calibration_error
	@ECHO.
@ECHO ---------------------------------------------------------
	@ECHO.
	@ECHO 		E R R O R
	@ECHO Calibration failed.
	@ECHO Programming aborted.
	@GOTO END
@ECHO !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

:EEPROM_OSCCAL_value_error
	@ECHO.
@ECHO ---------------------------------------------------------
	@ECHO.
	@ECHO 		E R R O R
	@ECHO EEPROM OSCCAL location contain an invalid value: 0xFF
	@ECHO Programming aborted.
	@GOTO END
@ECHO !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

:prog_customer_code_error
	@ECHO.
@ECHO ---------------------------------------------------------
	@ECHO.
	@ECHO 		E R R O R
	@ECHO Programming main application program or OSCCAL to AVR failed.
	@ECHO Programming aborted.
	@GOTO END
@ECHO !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

:END
	%del osc.bin
	@endlocal
@exit /b
				