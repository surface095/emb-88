﻿/*
	basic	- eval.c

	Copyright(c) 2017-2018 Osamu Tamura @ Recursion Co., Ltd.
					All rights reserved.
*/

#include "basic.h"

const unsigned char	*p;

static data_t	*var;
static data_t	*heap;
static data_t	err = 0;

static data_t	expression(void);

static data_t primary(void)
{
	data_t		o, n;
	unsigned char	c = _next();

	switch (c) {
	//	special function register
	case OP_REG:	return (data_t)sfr_get(_operand());

	//	constant value
	case OP_NUM:
	case OP_HEX:	return _operand();

	//	constant-array pointer
	//	string-literal pointer
	case OP_ARY:
	case OP_STR:	n = -(data_t)(p + 1 - prog);	_skip();
					return n;

	//	( expression )
	case OP_LP:		o = expression();	assert(_this() != OP_RP, -1)
					p++;	// skip )
					return o;
	//	variable
	default:		assert(c < 'A' || 'Z' < c, -1)
					return var[c & 0x1f];
	}
}

static data_t postfix(void)
{
	data_t		o = primary();
	unsigned char	c = _next();
	unsigned char	b = 0;

	if (c == OP_BYTE) {
		b = 1;
		c = _next();
	}
	if (c == OP_LB) {
		data_t		n = expression();	assert(_this() != OP_RB, -2)
		p++;	//	skip ]

		if (o < 0) {
			o = -o;
			//	char of string-literal
			if (b) {
				return p_byte(o + n);
			}
			//	value of integer-array
			else {
				return p_word(o + (n << 1));
			}
		}
		else {
			//	char of string-literal
			if (b) {
				return (data_t)*((char *)heap + o + n);
			}
			//	value of integer-array
			else {
				return *(heap + o + n);
			}
		}
	}

	p--;
	return o;
}

static data_t unary(void)
{
	switch (_next()) {
		case OP_NEG:	return -unary();
		case OP_COM:	return ~unary();
		case OP_NOT:	return !unary();
		default:p--;	return postfix();
	}
}

static data_t multiplicative(void)
{
	data_t	o = unary();

	switch (_next()) {
		case OP_MUL:	return o * multiplicative();
		case OP_DIV:	return o / multiplicative();
		case OP_MOD:	return o % multiplicative();
		default:p--;	return o;
	}
}

static data_t additive(void)
{
	data_t	o = multiplicative();

	switch (_next()) {
		case OP_ADD:	return o + additive();
		case OP_SUB:	return o - additive();
		default:p--;	return o;
	}
}

static data_t shift(void)
{
	data_t	o = additive();

	switch (_next()) {
		case OP_SFL:	return o << shift();
		case OP_SFR:	return o >> shift();
		case OP_SFU:	return (unsigned int)o >> shift();
		default:p--;	return o;
	}
}

static data_t relational(void)
{
	data_t	o = shift();

	switch (_next()) {
		case OP_LT:		return o < relational();
		case OP_GT:		return o > relational();
		case OP_LE:		return o <= relational();
		case OP_GE:		return o >= relational();
		default:p--;	return o;
	}
}

static data_t equality(void)
{
	data_t	o = relational();

	switch (_next()) {
		case OP_EQ:		return o == equality();
		case OP_NE:		return o != equality();
		default:p--;	return o;
	}
}

static data_t bitwise(void)
{
	data_t	o = equality();

	switch (_next()) {
		case OP_AND:	return o & bitwise();
		case OP_XOR:	return o ^ bitwise();
		case OP_OR:		return o | bitwise();
		default:p--;	return o;
	}
}

static data_t logical(void)
{
	data_t	o = bitwise();

	switch (_next()) {
		case OP_LAND:	return logical() && o;
		case OP_LOR:	return logical() || o;
		default:p--;	return o;
	}
}

static data_t assignment(void)
{
	const unsigned char	*pp = p;
	unsigned char	c = _next();
	unsigned char	b = 0;
	data_t			n = 0, m;

	//	special function register
	if (c == OP_REG) {
		n = _operand();
	}
	//	byte access
	if (_this() == OP_BYTE) {
		b = 1;
		p++;	//	skip $
	}
	//	array
	if (_this() == OP_LB) {
		n = var[c & 0x1f];	assert(n < 0, -2)
		if (b) {
			n <<= 1;
		}
		c = _next();	//	skip [
		n += assignment();	assert(_this() != OP_RB, -2)
		p++;			//	skip ]
	}

	if (_this() == OP_LET) {
		p++;	//	skip =
		m = assignment();

		if (c == OP_REG) {
			return sfr_set(n, (unsigned char)m);
		}
		if (c == OP_LB) {
			if (b) {
				return *((unsigned char *)heap + n) = (unsigned char)m;
			}
			else {
				return heap[n] = m;
			}
		}
		return var[c & 0x1f] = m;
	}

	p = pp;
	return logical();
}

static data_t expression(void)
{
	return assignment();
}


data_t eval(data_t *data)
{
	const unsigned char	**lp;
	const unsigned char	*intT0;
	addr_t			offset;
	data_t			tf;
	unsigned char	c;
	unsigned char	iret;


	lp = (const unsigned char **)data;
	var = (data_t *)(data + sizeof(unsigned char *) * MAX_STACK);
	heap = var + MAX_SYM;

	p	= prog + p_addr(0x1f);	// _M:

	intT0 = prog;
	iret = 0;
	tf = p_addr(0x1b);
	if (tf > 0) {
		intT0 += tf;	// _I:
		iret--;
		// default 2ms timer for interrupt
		timer_init();
	}

	for (c = 0; c < MAX_SYM; c++)
		var[c] = 0;

	tf = 1;
	do {
		//	interrupt
		if (iret & _timeout()) {
			iret = 0;
			*lp++ = p;	// push pc
			*lp++ = (const unsigned char *)-1;
			*lp++ = 0;
			p = intT0;	//	call _I:
		}

		c = _next();

		switch (c) {
		case OP_REM:
			_skip();
			break;

		case SYS_GOSUB:
			*lp++ = p + 1;	// push pc
			*lp++ = 0;
			p = prog + p_addr(_this());
			break;

		case SYS_RETURN:
			if (lp == (const unsigned char **)data) {
				return err;
			}
			while (*--lp)
				;
			lp--;

			//	return from interrupt
			if (*lp == (const unsigned char *)-1) {
				lp--;
				iret = 0xff;
			}

			p = *lp;
			break;

		case SYS_IF:
			tf = logical();
			assert(_this() != SYS_THEN && _this() != SYS_EXIT, 1)
			break;

		case SYS_THEN:
			offset = _operand();
			if (!tf) {
				p += offset;
				tf = 1;
				assert(*(p - sizeof(addr_t) - 1) != SYS_ELSE && *(p-1) != SYS_ENDIF, 2)
			}
			break;

		case SYS_ELSE:
			p += _operand();
			assert(*(p-1) != SYS_ENDIF, 3)
			break;

		case SYS_ENDIF:
			break;

		case SYS_DO:
			offset = _operand();
			*lp++ = p + offset;	//	exit
			*lp++ = p;		//	start
			break;

		case SYS_EXIT:
			if (tf) {
				lp--;
				p = *--lp;
				assert(*(p - 1) != SYS_LOOP, 4)
			}
			tf = 1;
			break;

		case SYS_LOOP:
			p = *(lp - 1);
			assert(*(p - sizeof(addr_t) - 1) != SYS_DO, 5)
			break;


		case SYS_INKEY:
			c = _next();
			var[c & 0x1f] = _getchar();
			break;

		case SYS_PRINT:
			do {
				unsigned char	c0, c1, c2;
				data_t			n;

				c0 = _this();
				c1 = _after(1);
				c2 = _after(2);

				n = logical();

				//	string-literal
				if (c0 == OP_STR) {
					_puts_P((const char *)prog - n, 0);
				}
				else if (c1 == '$') {
					//	char of string-literal
					if (c2 == OP_LB) {
						_putc((char)n);
					}
					//	string-literal
					else if (n < 0) {
						_puts_P((const char *)prog - n, 0);
					}
					else {
						_puts((char *)heap + n);
					}
				}
				//	character
				else if (_this() == OP_CHR) {
					_putc((char)n);
					p++;
				}
				//	value
				else {
					_puts(_itoa(n));
				}
			} while (_next() == ',');
			p--;

			if (_this() == ';') {
				p++;
			}
			else {
				_putc('\n');
			}
			break;

		case SYS_END:
			return 0;

		default:
			p--;
			expression();
		}
	} while (!err);

	return err;
}
