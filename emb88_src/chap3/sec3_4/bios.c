/*
	basic	- bios.c

	Copyright(c) 2017-2018 Osamu Tamura @ Recursion Co., Ltd.
					All rights reserved.
*/

#include "basic.h"

#ifdef AVR
#include <stdlib.h>
#include <avr/boot.h>
#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/pgmspace.h>
#include <avr/eeprom.h>
#include <avr/wdt.h>

#define SW1_PIN		(1<<PORT4)
#define _SW1		(PINC & SW1_PIN)

#define BAUD		38400
#define RXBUF_SZ	16
#define TXBUF_SZ	8

static volatile unsigned char	urptr, uwptr;
static volatile unsigned char	irptr, iwptr;
static char	rbuf[RXBUF_SZ];
static char	tbuf[TXBUF_SZ];


sfr_t sfr_get(data_t addr)
{
	return *(sfr_t *)addr;
}

sfr_t sfr_set(data_t addr, unsigned char data)
{
	return *(sfr_t *)addr = data;
}


//	interrupt
char _timeout(void)
{
	if (TIFR0 & (1 << OCF0A)) {
		TIFR0 = (1 << OCF0A);
		return 1;
	}
	return 0;
}

void timer_init(void)
{
	OCR0A = 249;	// 2mS;
	TCCR0A = 2;
	TCCR0B = 3;
	TCNT0 = 0;
	TIFR0 = (1 << OCF0A);
}


ISR(USART_RX_vect)
{
	char	c;
	unsigned char	nptr;

	c = UDR0;
	if (c == '\r') {
		return;
	}
	nptr = (iwptr + 1) & (RXBUF_SZ - 1);
	if (nptr != urptr) {
		rbuf[iwptr] = c;
		iwptr = nptr;
	}
}

ISR(USART_UDRE_vect)
{
	if (irptr == uwptr) {
		UCSR0B &= ~(1 << UDRIE0);
	}
	else {
		UDR0 = tbuf[irptr];
		irptr = (irptr + 1) & (TXBUF_SZ - 1);
	}
}

data_t _getchar(void)
{
	unsigned char	c;

	wdt_reset();
	if (urptr != iwptr) {
		c = rbuf[urptr];
		urptr = (urptr + 1) & (RXBUF_SZ - 1);
		return (data_t)c;
	}
	return (data_t)-1;
}

data_t _getc(void)
{
	unsigned char	c;

	while (urptr == iwptr) {
		wdt_reset();
	}
	c = rbuf[urptr];
	urptr = (urptr + 1) & (RXBUF_SZ - 1);
	return (data_t)c;
}

void _putc(char c)
{
	unsigned char	nptr;

	if (c == '\n') {
		_putc('\r');
	}
	do {
		wdt_reset();
		nptr = (uwptr + 1) & (TXBUF_SZ - 1);
	} while (nptr == irptr);

	tbuf[uwptr] = c;
	uwptr = nptr;
	UCSR0B |= (1 << UDRIE0);
}

void _puts(char *s)
{
	while (*s) {
		_putc(*s++);
	}
}

void _puts_P(const char *s, unsigned char offset)
{
	unsigned char	c;

	for (;;) {
		c = pgm_read_byte(s++);
		if (!c) {
			break;
		}
		_putc(c + offset);
	}
}

unsigned char conio_init(void)
{
	unsigned char	osc;
	unsigned char	auto_start = 0xff;

	PORTC = SW1_PIN;	// pull-up SW1

	//	calibrate the internal RC osc
	osc = pgm_read_byte((uint8_t *)FLASHEND);
	if (osc != 0xff) {
		OSCCAL = osc;
	}

	DDRD = 0x06;

	/* USART */
	UBRR0 = (F_CPU >> 4) / BAUD - 1;
	UCSR0A = 0;
	UCSR0C = 3 << UCSZ00;	/* 8N1 */
	UCSR0B = (1 << RXCIE0) | (1 << UDRIE0) | (1 << RXEN0) | (1 << TXEN0);

	if (_SW1) {
		/* read auto-start flag	*/
		auto_start = eeprom_read_byte(0);
	}
	else {
		/* release auto-start	*/
		eeprom_write_byte(0, 0xff);
	}

	sei();

	return auto_start;
}

#else

#include <conio.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

static clock_t	tm;
static sfr_t	_SFR[256];


sfr_t sfr_get(data_t addr)
{
	return _SFR[addr];
}

sfr_t sfr_set(data_t addr, unsigned char data)
{
	return _SFR[addr] = data;
}


#define	TM_INTERVAL	(CLOCKS_PER_SEC * 2 / 1000)		// 2mSec

char _timeout(void)
{
	if (tm <= clock()) {
		tm += TM_INTERVAL;
		return 1;
	}
	return 0;
}

void timer_init(void)
{
	tm = clock() + TM_INTERVAL;
}


data_t _getchar(void)
{
	return _kbhit() ? (data_t)_getch() : (data_t)-1;
}

data_t _getc(void)
{
	int		c;
	//	return (data_t)fgetc(fp);

	while (!_kbhit())
		;
	c = _getch();
	if (c == '\r') {
		c = '\n';
	}
	return (data_t)c;
}

void _putc(char c)
{
	putchar(c);
}

void _puts(char *s)
{
	while (*s) {
		_putc(*s++);
	}
}

void _puts_P(const char *s, unsigned char offset)
{
	while (*s) {
		_putc(*s++ + offset);
	}
}

unsigned char conio_init(void)
{
	return 1;
}
#endif
