﻿/*
	basic	- parse.c	- parser for BASIC

	Copyright(c) 2017-2018 Osamu Tamura @ Recursion Co., Ltd.
					All rights reserved.
*/

#include "basic.h"

enum {
	LX_DEFAULT = 0,
	LX_RESERVED,
	LX_COMMENT,
	LX_STRING,
	LX_ARRAY,
	LX_IDENTIFIER,
	LX_NUMBER,
	LX_HEXNUMBER,
	LX_SYMBOL,
};

#ifdef AVR
#include <avr/pgmspace.h>
#define	_strcmp(a,b)	strcmp_P((const char *)a, pgm_read_ptr(&b))

static const char _s00[] PROGMEM = "";
static const char _s01[] PROGMEM = "DO";
static const char _s02[] PROGMEM = "ELSE";
static const char _s03[] PROGMEM = "END";
static const char _s04[] PROGMEM = "ENDIF";
static const char _s05[] PROGMEM = "EXIT";
static const char _s06[] PROGMEM = "GOSUB";
static const char _s07[] PROGMEM = "IF";
static const char _s08[] PROGMEM = "INKEY";
static const char _s09[] PROGMEM = "LOOP";
static const char _s10[] PROGMEM = "PRINT";
static const char _s11[] PROGMEM = "RETURN";
static const char _s12[] PROGMEM = "THEN";

PGM_P const syskeys[] PROGMEM = {
	_s00, _s01, _s02, _s03, _s04, _s05, _s06, _s07, _s08, _s09, _s10, _s11, _s12
};

#else
#include <stdio.h>
#include <string.h>
#define	_strcmp(a,b)	strcmp((char *)a,(char *)b)

const char	*syskeys[] = {
	"",
	"DO",
	"ELSE",
	"END",
	"ENDIF",
	"EXIT",
	"GOSUB",
	"IF",
	"INKEY",
	"LOOP",
	"PRINT",
	"RETURN",
	"THEN",
};
#endif


data_t parse(unsigned char *code, data_t size)
{
	addr_t			*table;
	unsigned char	*pcode;
	unsigned char	**th;
	unsigned char	**lp;
	unsigned char	*cp, *len, *a;
	unsigned char	c, cc, prev;
	unsigned char	s, ss;
	unsigned char	i, cnt;
	data_t			n;


	table = (addr_t *)code;
	pcode = code + sizeof(addr_t) * MAX_LABEL;
	cp = pcode;

	th = (unsigned char **)(code + size - sizeof(unsigned char *) * MAX_STACK);
	lp = th - sizeof(unsigned char *) * MAX_STACK;
	for (i = 1; i < (MAX_LABEL - 1); i++) {
		table[i] = (addr_t)-1;
	}
	table[i] = (addr_t)0;

	n = 0;
	cnt = 0;
	len = cp;
	a = cp;
	prev = 0;
	s = LX_DEFAULT;
	ss = s;

	for (;;) {

		if (prev) {
			c = prev;
			prev = 0;
		}
		else {
			c = (unsigned char)_getc();
		}

		switch (s) {
		case LX_DEFAULT:
			switch (c) {
			case OP_REM:	//	comment
				*cp++ = c;
				len = cp++;
				s = LX_COMMENT;
				break;

			case OP_STR:	//	string
				*cp++ = c;
				len = cp++;
				s = LX_STRING;
				break;

			case OP_ARY:	//	integer array
				*cp++ = c;
				len = cp++;
				cnt = 0;
				s = LX_ARRAY;
				break;

			case OP_LBL:	//	table
				cc = *--cp;
				if (*(cp - 1) == '_') {
					cp--;
					cc += 0x12;
				}
				table[cc & 0x1f] = (addr_t)(cp - pcode);
				break;

			case OP_HEX:	//	hexadecimal number
			case OP_REG:	//	special function register
				*cp++ = c;
				cnt = 0;
				nbuf[cnt++] = OP_HEX;	// c;
				s = LX_HEXNUMBER;
				break;

			default:
				if (c <= ' ' || 0x7f <= c) {
					_putc('.');
					break;
				}
				if ('0' <= c && c <= '9') {
					cnt = 0;
					nbuf[cnt++] = c;
					s = LX_NUMBER;
					break;
				}
				a = cp;
				cc = c & ~0x20;
				if ('A' <= cc && cc <= 'Z') {
					c = cc;
					s = LX_IDENTIFIER;
				}
				else {
					if (c == '.') {
						goto end;
					}
					s = LX_SYMBOL;
				}
				*cp++ = c;
				break;
			}
			break;

		case LX_COMMENT:
			if (c == '\n' || c == '\r') {
				*len = (unsigned char)(cp - len);
				*cp++ = 0;
				s = LX_DEFAULT;
			}
			else {
				if ((cp - 1) == len) {
					if (c <= ' ') {
						break;
					}
					if (c == '\'') {
						c = '\t';
					}
				}
				*cp++ = c;
			}
			break;

		case LX_STRING:
			if (c == OP_STR) {
				*len = (unsigned char)(cp - len);
				*cp++ = 0;
				ss = s;
				s = LX_DEFAULT;
			}
			else {
				*cp++ = c;
			}
			break;

		case LX_ARRAY:
			if (c == ',' || c == '}') {
				*(data_t *)cp = _atoi(cnt);
				cp += sizeof(data_t);
				if (c == '}') {
					*len = (unsigned char)(cp - len - 1);
					ss = s;
					s = LX_DEFAULT;
				}
				cnt = 0;
			}
			else if (c > ' ') {
				nbuf[cnt++] = c;
			}
			break;

		case LX_IDENTIFIER:
			cc = c & ~0x20;
			if ('A' <= cc && cc <= 'Z') {
				*cp++ = cc;
				break;
			}
			prev = c;

			if ((cp - a) > 1) {
				*cp = 0;
				cp = a + 1;
				for (i = SYS_DO; i <= SYS_THEN; i++) {
					n = (data_t)_strcmp(a, syskeys[i]);
					if (n <= 0)
						break;
				}
				if (!n) {
					//				printf( "[%s] ", syskeys[i] );
					_putc('.');

					*a = (unsigned char)i;
					s = LX_RESERVED;
					switch (i) {
					case SYS_THEN:	*th++ = cp;
						cp += sizeof(addr_t);
						break;
					case SYS_ELSE:	th--;
						*(addr_t *)*th = (addr_t)(cp - *th);
						*th++ = cp;
						cp += sizeof(addr_t);
						break;
					case SYS_ENDIF:	th--;
						*(addr_t *)*th = (addr_t)(cp - *th - sizeof(addr_t));
						break;
					case SYS_DO:	*lp++ = cp;
						cp += sizeof(addr_t);
						break;
					case SYS_LOOP:	lp--;
						*(addr_t *)*lp = (addr_t)(cp - *lp - sizeof(addr_t));
						break;
					case SYS_END:
						goto end;
					}
				}
			}
			ss = s;
			s = LX_DEFAULT;
			break;

		case LX_NUMBER:
			if ('0' <= c && c <= '9') {
				nbuf[cnt++] = c;
				break;
			}
			prev = c;

			n = _atoi(cnt);
			if (ss == LX_SYMBOL && *(cp - 1) == OP_NEG) {
				n = -n;
				cp--;
			}
			*cp++ = OP_NUM;
			*(data_t *)cp = n;
			cp += sizeof(data_t);
			ss = s;
			s = LX_DEFAULT;
			break;

		case LX_HEXNUMBER:
			if (('0' <= c && c <= '9') ||
				('A' <= c && c <= 'F') ||
				('a' <= c && c <= 'f')) {
				nbuf[cnt++] = c;
				break;
			}
			prev = c;

			if (cnt == 1) {
				cp--;
				s = LX_SYMBOL;
			}
			else {
				n = _atoi(cnt);
				*(data_t *)cp = n;
				cp += sizeof(data_t);
			}
			ss = s;
			s = LX_DEFAULT;
			break;

		case LX_SYMBOL:
			if (('#' < c && c < '0') ||
				('9' < c && c < '@') ||
				('Z' < c && c < 'a') ||
				('z' < c) ||
				(c == '!')) {
				*cp++ = c;
				break;
			}
			prev = c;

			*cp = 0;
			cp = a;
			while (*a) {
				c = *a++;
				switch (c) {
				case '&':	//	& &&
					if (*a == '&') {
						a++;	c = OP_LAND;
					}
					break;
				case '|':	//	| ||
					if (*a == '|') {
						a++;	c = OP_LOR;
					}
					break;
				case '>':	//	> >= >> >>>
					switch (*a++) {
					case '=':	c = OP_GE;	break;
					case '>':	c = OP_SFR;
						if (*a == '>') {
							a++;
							c = OP_SFU;
						}
						break;
					default:	a--;
					}
					break;
				case '<':	//	< <= << <>
					switch (*a++) {
					case '=':	c = OP_LE;	break;
					case '<':	c = OP_SFL;	break;
					case '>':	c = OP_NE;	break;
					default:	a--;
					}
					break;
				case '=':	//	= ==
					if (*a == '=') {
						a++;	c = OP_EQ;
					}
					break;
				case '!':	//	! !=
					if (*a == '=') {
						a++;	c = OP_NE;
					}
					break;
				case '-':	//	sub minus
					if (ss == LX_SYMBOL) {
						cc = *(cp - 1);
						if (cc != ')' && cc != ']' && cc != '$') {
							c = OP_NEG;
						}
					}
					break;
				}
				*cp++ = c;
				ss = s;
			}
			s = LX_DEFAULT;
		}
	}

end:
	_putc('\n');

	if (*(cp - 1) != SYS_END) {
		*cp++ = SYS_END;
	}
	*cp++ = 0;
	table[0] = (addr_t)(cp - pcode);

	return (data_t)(cp - code);
}
