'
'	Invader
'
_M:
	@24 = #ff
	@27 = #0f
	@2a = #fe
	@28 = #30
	@b1 = #44
'
	f = 8
	e = 16
	do
		m[i] = 0
		f[i] = 0
		e[i] = 0
		i = i + 1
		if i >= 8 exit
	loop
	g = #08
	h = #1c
'
	do
		if t >= 50 then
			t = t - 50
			n = n + 1
			@b0 = 0
'
			if (n & 31) == 0 then
				@b3 = 168
				@b0 = #12
				i = 7
				do
					e[i] = e[i - 1]
					i = i - 1
					if i == 0 exit
				loop
				if n & 32 then
					e[0] = #56
				else
					e[0] = #6a
				endif
			endif
'
			i = 0
			do
				f[i] = f[i + 1]
				i = i + 1
				if i >= 5 exit
			loop
'
			b = ~ (@26 >> 4) & 3
'
			if b == 1 && h < #80 then
				g = g << 1
				h = h << 1
			endif
'
			if b == 2 && (h & 1) == 0 then
				g = g >> 1
				h = h >> 1
			endif
'
			f[6] = g
			f[7] = h
'
			if b == 3 then
				f[5] = f[6]
			else
				f[5] = 0
			endif
'
			i = 0
			do
				if e[i] & f[i] then
					e[i] = e[i] ^ f[i]
					@b3 = 46
					@b0 = #12
				endif
				i = i + 1
				if i >= 7 exit
			loop
'
			i = 0
			do
				m[i] = e[i] | f[i]
				i = i + 1
				if i >= 8 exit
			loop
		endif
	loop
'
'	Scan
_I:
	s = ~ (1 << c)
	@25 = 0
	@2b = s & #f0
	@28 = s | #30
	@25 = m[c]
	c = (c + 1) & 7
'
	t = t + 1
	return
'
	end
