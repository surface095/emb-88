'
'	Elevator
'
_M:
	gosub I
'
	do
		if t >= 60 then
			t = t - 60
			@b0 = 0
'
			do
''	Standby
				if s == 0 then
'
'					Push?
					b = ~ (@26 >> 4) & 3
'
'					Down?
					if f > 1 && b == 1 then
						n = 0
						w = 10
						s = 1
						exit
					endif
'
'					Up?
					if f < 6 && b == 2 then
						n = 7
						w = 15
						s = 2
						exit
					endif
'
					@b0 = 0
					exit
				endif
''	Go Up
				if s == 1 then
					w = w - 1
					if w == 0 then
						f = f - 1
						n = f
						k = 0
						gosub H
						s = 0
					else
'						Rotate Arrow (Up)
						gosub L
						k = k - 1
					endif
					exit
				endif
''	Go Down
				if s == 2 then
					w = w - 1
					if w == 0 then
						f = f + 1
						n = f
						k = 0
						gosub H
						s = 0
					else
'						Rotate Arrow (Down)
						gosub L
						k = k + 1
					endif
					exit
				endif
				exit
			loop
		endif
	loop
'
'
'	Initialize
I:
'	'��', 1�`6, '��'
	m = {
		#18, #18, #18, #18, #7e, #3c, #18, #00,
		#08, #18, #08, #08, #08, #08, #1C, #00,
		#3C, #42, #02, #0C, #10, #20, #7E, #00,
		#3C, #42, #02, #3C, #02, #42, #3C, #00,
		#04, #0C, #14, #24, #44, #7E, #04, #00,
		#7E, #40, #40, #7C, #02, #02, #7C, #00,
		#3C, #42, #40, #7C, #42, #42, #3C, #00,
		#00, #18, #3c, #7e, #18, #18, #18, #18
	}
'
	f = 1
	n = f
'
'	DDRB, DDRC, DDRD
	@24 = #ff
	@27 = #0f
	@2a = #fe
	@28 = #30
'
'	TCCR2A
	@b1 = #44

'	TCCR0B, OCR0A 
	@45 = #04
	@47 = 77
	return
'
'
'	Buzzer (Low)
L:
	@b3 = 168
	@b0 = #12
	return
'
'	Buzzer (High)
H:
	@b3 = 46
	@b0 = #12
	return
'
'
'	Scan
_I:
	q = ~(1 << c)
	@25 = 0
	@2b = q & #f0
	@28 = q | #30
	@25 = m[n * 8 + ((c + k) & 7)]
	c = (c + 1) & 7
'
	t = t + 1
	return
'
	end
