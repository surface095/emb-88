'
'	Dice
'
_M:
	d = {
                #00, #00, #00, #18, #18, #00, #00, #00,
                #03, #03, #00, #00, #00, #00, #c0, #c0,
                #03, #03, #00, #18, #18, #00, #c0, #c0,
                #c3, #c3, #00, #00, #00, #00, #c3, #c3,
                #c3, #c3, #00, #18, #18, #00, #c3, #c3,
                #c3, #c3, #00, #c3, #c3, #00, #c3, #c3
	}
'
'	DDRB, DDRC, DDRD
	@24 = #ff
	@27 = #0f
	@2a = #fe
	@28 = #30
'
	m = 0
	n = 0
	c = 0
	i = 0
	do
		m[i] = 0
		i = i + 1
		if i >= 8 exit
	loop
	do
		if t >= 250 then
			t = t - 250
			i = 0
			do
				m[i] = d[n * 8 + i]
				i = i + 1
				if i >= 8 exit
			loop
			n = n + 1
			if n >= 6 then
				n = 0
			endif
		endif
	loop
'
'
'	Scan matrix LED
_I:
	s = ~ (1 << c)
	@25 = 0
	@2b = s & #f0
	@28 = s | #30
	@25 = m[c]
	c = (c + 1) & 7
'
	t = t + 1
	return
'
	end
