'
'	Echo
'
_M:
'	DDRB, DDRC, DDRD
	@24 = #ff
	@27 = #3f
	@2a = #fe
	@28 = #10
'
	m = 0
	i = 0
	do
		m[i] = 0
		i = i + 1
		if i >= 8 exit
	loop
'
	n = 0
	k = 0
	w = #10
	do
		inkey c
		if c != -1 then
			print c\;

			y = (n >> 3) & 7
			x = #80 >> (n & 7)
			m[y] = m[y] ^ x
			n = n + 1

			@28 = w = w ^ #30
		endif
	loop
'
'	Scan matrix LED
_I:
	s = ~ (1 << k)
	@25 = 0
	@2b = s & #f0
	@28 = (s & #0f) | w
	@25 = m[k]
	k = (k + 1) & 7
	return
'
	end

