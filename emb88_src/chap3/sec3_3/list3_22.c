/*
	list3_22.c	-- Coroutine

    協調的マルチタスク： コルーチン

    Copyright(c) Recursion Co., Ltd. All rights reserved.
*/

#include <avr/io.h>
#include <avr/wdt.h>

/*	コルーチンのマクロ	*/
#define TASK_BEGIN		static char _cortn=0;switch(_cortn){case 0:
#define TASK_END		default:_cortn=0;return 1;}
#define TASK_SLEEP(x)	_cortn=__LINE__;return x;case __LINE__:


static int task_A( void )
{
    static unsigned char m = 1;	// static 宣言が必要

	TASK_BEGIN
			PORTB ^= m;			// マトリクスLED 表示
			m = (m << 1) | (m >> 7);
		TASK_SLEEP(150);
	TASK_END
}

static int task_B( void )
{
	static unsigned char tone[8] = { 238, 212, 189, 178, 158, 141, 126, 118 };
    static unsigned char n = 0;

	TASK_BEGIN
			OCR2A = tone[n++];	// 音階発生
			n &= 7;
			PORTC |= 0x10;		// 緑LED ON
		TASK_SLEEP(200);
			PORTC ^= 0x30;		// 緑LED OFF
		TASK_SLEEP(300);
	TASK_END
}

int main(void)
{
	int		timeout1 = 1;
	int		timeout2 = 1;

	DDRB	= 0xff;
	DDRC	= 0x3f;
	DDRD	= 0xfe;

	// Timer0  1mS, CTC, 1/64
	OCR0A	= ((F_CPU >> 6) / 1000) - 1;
	TCCR0A	= 0x02;
	TCCR0B	= 0x03;

	// Timer2 ブザー音程
	TCCR2A	= 0x12;
	TCCR2B	= 0x44;

    for( ;; ) {
 		if ((TIFR0 & _BV(OCF0A)) == 0) {
			continue;
		}
		TIFR0 = _BV(OCF0A);
		wdt_reset();

		// 指定の待ち時間後に起動
		if (--timeout1 == 0) {
			timeout1 = task_A();
		}
		if (--timeout2 == 0) {
			timeout2 = task_B();
		}
	}

    return 0;
}

