/*
	list3_27.c	--	Preemptive Multitasking 2

	プリエンプティブ・マルチタスク： コンテキスト・スイッチ２

	Copyright(c) Recursion Co., Ltd. All rights reserved.
*/

#include <stdlib.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/wdt.h>
#include "list3_27.h"

#define TASK_MAX	12

#define PUSH_SIZE   35		/* pushed register size(33) + return address(2)	*/

static unsigned int		heap;

typedef struct tcb {
	int				delay;
	unsigned int	stackp;
} TCB;

static volatile TCB	*t_idle, *t_run, *t_end, *t_last;
static volatile TCB	tcb[TASK_MAX+1];

void TIMER2_COMPA_vect(void) __attribute__((signal,naked));

ISR( TIMER2_COMPA_vect )
{
	asm volatile( \
	"push   r1 \n"\
	"push   r0 \n"\
	"in     r0, __SREG__ \n"\
	"push   r0 \n"\
	"clr    r1 \n"\
	"push   r2 \n"\
	"push   r3 \n"\
	"push   r4 \n"\
	"push   r5 \n"\
	"push   r6 \n"\
	"push   r7 \n"\
	"push   r8 \n"\
	"push   r9 \n"\
	"push   r10 \n"\
	"push   r11 \n"\
	"push   r12 \n"\
	"push   r13 \n"\
	"push   r14 \n"\
	"push   r15 \n"\
	"push   r16 \n"\
	"push   r17 \n"\
	"push   r18 \n"\
	"push   r19 \n"\
	"push   r20 \n"\
	"push   r21 \n"\
	"push   r22 \n"\
	"push   r23 \n"\
	"push   r24 \n"\
	"push   r25 \n"\
	"push   r26 \n"\
	"push   r27 \n"\
	"push   r28 \n"\
	"push   r29 \n"\
	"push   r30 \n"\
	"push   r31 \n"
	);

	wdt_reset();

	{
		TCB	*p;

		t_run->stackp  = SP;		/*  SP を保存	*/

		/*	待ち時間をカウント	*/
		for (p = (TCB *)t_idle + 1; p <= t_last; p++) {
			if (p->delay > 0) {
				p->delay--;
			}
		}

		/*	つぎのタスクに切り替え	*/
		p = (TCB *)t_run;
		do {
			if (++p > (TCB *)t_last) {
				p = (TCB *)t_idle;
			}
		} while (p->delay && p != t_run);
		t_run = p->delay? t_idle:p;

		SP  = t_run->stackp;		/*  SP を復元	*/
	}

	asm volatile( \
	"pop    r31 \n"\
	"pop    r30 \n"\
	"pop    r29 \n"\
	"pop    r28 \n"\
	"pop    r27 \n"\
	"pop    r26 \n"\
	"pop    r25 \n"\
	"pop    r24 \n"\
	"pop    r23 \n"\
	"pop    r22 \n"\
	"pop    r21 \n"\
	"pop    r20 \n"\
	"pop    r19 \n"\
	"pop    r18 \n"\
	"pop    r17 \n"\
	"pop    r16 \n"\
	"pop    r15 \n"\
	"pop    r14 \n"\
	"pop    r13 \n"\
	"pop    r12 \n"\
	"pop    r11 \n"\
	"pop    r10 \n"\
	"pop    r9 \n"\
	"pop    r8 \n"\
	"pop    r7 \n"\
	"pop    r6 \n"\
	"pop    r5 \n"\
	"pop    r4 \n"\
	"pop    r3 \n"\
	"pop    r2 \n"\
	"pop    r0 \n"\
	"out    __SREG__, r0 \n"\
	"pop    r0 \n"\
	"pop    r1 \n"
	);
	reti();
}

/*	タスク切り替えの初期化	*/
void task_init(void)
{
	volatile TCB	*p;

	/*  Timer2 1mS, CTC, 1/64  */
	OCR2A	= (F_CPU >> 6) / 1000 - 1;
	TCCR2A	= 0x02;
	TCCR2B	= 0x04;
	TIMSK2	= (1 << OCIE2A);

	heap = (unsigned int)__malloc_heap_start;

	t_idle = (volatile TCB *)&tcb[0];
	t_end = (volatile TCB *)&tcb[TASK_MAX+1];

	for (p = t_idle + 1; p < t_end; p++) {
		p->delay = TASK_UNUSED;
	}
	t_run = t_idle;
	t_last = t_run;
}

/*	タスクの生成と起動    */
char task_create( void *func, int memsize )
{
	volatile TCB	*p;
	unsigned char	n;
	unsigned int    bottom;
	int				i;

	/*	空き TCB を探す	*/
	for (p = t_idle + 1, n = 1; p < t_end; p++,n++) {
		if (p->delay == TASK_UNUSED) {
			break;
		}
	}
	if (p == t_end) {
		return 0;
	}
	if (t_last < p) {
		t_last = p;
	}
	
	/*	メモリ空間を確保		*/
	if (memsize < MEM_DEFAULT) {
		memsize = MEM_DEFAULT;
	}
	for (i = 0; i < memsize; i++) {
		*(char *)heap++ = 0;
	}

	/*	タスクのエントリをスタックに PUSH    */
	bottom = heap - 1;
	*(unsigned char *)(bottom) = (unsigned int)func & 0xff;
	*(unsigned char *)(bottom-1) = (unsigned int)func >> 8;

	/*	スタックポインタを設定    */
	p->stackp = bottom - PUSH_SIZE;
	p->delay = TASK_START;

	return (char)n;
}

/*	タスクの時間待ちと休止・再開	*/
void task_sleep( char id, int wait )
{
	volatile TCB	*p;
	
	p = (id == THIS_TASK)? t_run:&tcb[(unsigned char)id];

	cli();
	p->delay = wait;
	sei();
	if (id == THIS_TASK) {
		do {
			wdt_reset();
		} while( p->delay );
	}
}



