/*
	list3_25.c	--	Preemptive Multitasking 1

	プリエンプティブ・マルチタスク： コンテキスト・スイッチ１

	Copyright(c) Recursion Co., Ltd. All rights reserved.
*/

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/wdt.h>

#define TASK_MAX	10

#define MEM_DEFAULT  64
/* pushed register size(33) + return address(2)	*/
#define PUSH_SIZE   35

static unsigned int		  heap = 0x200;

volatile unsigned int     stackp[TASK_MAX];
volatile unsigned int     *sp_min = &stackp[0];
volatile unsigned int     *sp_max = &stackp[0];
volatile unsigned int     *sp = &stackp[TASK_MAX-1];

volatile unsigned int     tick = 0;


void TIMER2_COMPA_vect(void) __attribute__((signal,naked));

ISR( TIMER2_COMPA_vect )
{
	asm volatile( \
	"push   r1 \n"\
	"push   r0 \n"\
	"in     r0, __SREG__ \n"\
	"push   r0 \n"\
	"clr    r1 \n"\
	"push   r2 \n"\
	"push   r3 \n"\
	"push   r4 \n"\
	"push   r5 \n"\
	"push   r6 \n"\
	"push   r7 \n"\
	"push   r8 \n"\
	"push   r9 \n"\
	"push   r10 \n"\
	"push   r11 \n"\
	"push   r12 \n"\
	"push   r13 \n"\
	"push   r14 \n"\
	"push   r15 \n"\
	"push   r16 \n"\
	"push   r17 \n"\
	"push   r18 \n"\
	"push   r19 \n"\
	"push   r20 \n"\
	"push   r21 \n"\
	"push   r22 \n"\
	"push   r23 \n"\
	"push   r24 \n"\
	"push   r25 \n"\
	"push   r26 \n"\
	"push   r27 \n"\
	"push   r28 \n"\
	"push   r29 \n"\
	"push   r30 \n"\
	"push   r31 \n"
	);

	wdt_reset();
	tick++;

	*sp = SP;   /*  SP を保存	*/

	/*  つぎのタスクへ切り替え  */
	if (++sp >= sp_max) {
		sp = sp_min;
	}
	SP  = *sp;	/*  SP を復元	*/


	asm volatile( \
	"pop    r31 \n"\
	"pop    r30 \n"\
	"pop    r29 \n"\
	"pop    r28 \n"\
	"pop    r27 \n"\
	"pop    r26 \n"\
	"pop    r25 \n"\
	"pop    r24 \n"\
	"pop    r23 \n"\
	"pop    r22 \n"\
	"pop    r21 \n"\
	"pop    r20 \n"\
	"pop    r19 \n"\
	"pop    r18 \n"\
	"pop    r17 \n"\
	"pop    r16 \n"\
	"pop    r15 \n"\
	"pop    r14 \n"\
	"pop    r13 \n"\
	"pop    r12 \n"\
	"pop    r11 \n"\
	"pop    r10 \n"\
	"pop    r9 \n"\
	"pop    r8 \n"\
	"pop    r7 \n"\
	"pop    r6 \n"\
	"pop    r5 \n"\
	"pop    r4 \n"\
	"pop    r3 \n"\
	"pop    r2 \n"\
	"pop    r0 \n"\
	"out    __SREG__, r0 \n"\
	"pop    r0 \n"\
	"pop    r1 \n"
	);
	reti();
}

/*	タスク切り替えの初期化	*/
void task_init(void)
{
	/*  Timer2 1mS, CTC, 1/64  */
	OCR2A	= (F_CPU >> 6) / 1000 - 1;
	TCCR2A	= 0x02;
	TCCR2B	= 0x04;
	TIMSK2	= (1 << OCIE2A);
}

/*	タスクの生成と起動    */
void task_create( void *func )
{
	unsigned int    bottom;
	int				i;

	/*	メモリ空間を確保		*/
	for (i = 0; i < MEM_DEFAULT; i++) {
		*(char *)heap++ = 0;
	}

	/*	タスクのエントリをスタックに PUSH    */
	bottom = heap - 1;
	*(unsigned char *)(bottom) = (unsigned int)func & 0xff;
	*(unsigned char *)(bottom-1) = (unsigned int)func >> 8;

	/*    スタックポインタを設定    */
	*sp_max++    = bottom - PUSH_SIZE;
}

