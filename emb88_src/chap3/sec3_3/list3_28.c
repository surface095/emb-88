/*
	list3_28.c

	プリエンプティブ・マルチタスク： コンテキスト・スイッチ２

	Copyright(c) Recursion Co., Ltd. All rights reserved.
*/

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>
#include "list3_27.h"

static char t1, t2, t3, t4, t5;
static int f = 1;

/*	task_1 : LED 点滅２秒周期		*/
static void task_1( void )
{
	for( ;; ) {
		PORTC   ^= 0x14;
		task_sleep(THIS_TASK, 1000);
	}
}

/*	task_2 : ブザー音発生 2f mS 周期		*/
static void task_2( void )
{
	for( ;; ) {
		PORTD	^= 0x08;
		task_sleep(THIS_TASK, f);
	}
}

/*	task_3 : ブザー OFF １秒間	*/
static void task_3( void )
{
	for( ;; ) {
		PORTC   &= ~0x20;
		task_sleep(t2, TASK_SUSPEND);
		task_sleep(t4, 1000);
		task_sleep(THIS_TASK, TASK_SUSPEND);
	}
}

/*	task_4 : ブザー ON 0.5秒間	*/
static void task_4( void )
{
	for( ;; ) {
		PORTC   |= 0x20;
		task_sleep(t2, TASK_START);
		task_sleep(t3, 500);
		task_sleep(THIS_TASK, TASK_SUSPEND);
	}
}

/*	task_5 : ブザー音切り替え ２秒毎	*/
static void task_5( void )
{
	for( ;; ) {
		PORTD   ^= 0x40;
		f = 4 - f;
		task_sleep(THIS_TASK, 2000);
	}
}

int main(void)
{
	/*  LED ポートの設定    */
	DDRB    = 0xff;
	DDRC    = 0x3f;
	DDRD    = 0xfe;
	PORTB   = 0x42;
	PORTC   = 0x0f;
	PORTD   = 0xf0;

	task_init();

	/*    タスクの生成と起動    */
	t1 = task_create(task_1, MEM_DEFAULT);
	t2 = task_create(task_2, MEM_DEFAULT);
	t3 = task_create(task_3, MEM_DEFAULT);
	t4 = task_create(task_4, MEM_DEFAULT);
	task_sleep(t4, TASK_SUSPEND);
	t5 = task_create(task_5, MEM_DEFAULT);

	set_sleep_mode(SLEEP_MODE_PWR_SAVE);
	sei();
	for( ;; ) {
		sleep_mode();
	}

	return 0;
}
