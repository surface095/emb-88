/*
	list3_23.h	--	Non-preemptive Multitasking

	?????I?}???`?^?X?N?F ?f?B?X?p?b?`?E?L???[

	Copyright(c) Recursion Co., Ltd. All rights reserved.
*/

extern void task_init(void);
extern char task_register(void (*func)(), int param, unsigned int delay);
extern void task_dispatch(void);

