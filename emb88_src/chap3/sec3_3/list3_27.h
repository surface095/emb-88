
/*
	list3_27.h

    ?R???e?L?X?g?E?X?C?b?`

    Copyright(c) Recursion Co., Ltd. All rights resereved.
*/

extern void task_init( void );
extern char task_create( void *func, int memsize );
extern void task_sleep( char id, int wait );

#define MEM_DEFAULT		64
#define THIS_TASK		0xff
#define TASK_START		0
#define TASK_SUSPEND	-1
#define TASK_UNUSED		-2
