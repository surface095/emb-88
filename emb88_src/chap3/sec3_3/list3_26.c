/*
	list3_26.c

	プリエンプティブ・マルチタスク： コンテキスト・スイッチ１

	Copyright(c) Recursion Co., Ltd. All rights reserved.
*/

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/wdt.h>

extern void task_init(void);
extern void task_create( void *func );
extern volatile unsigned int     tick;	/* mS	*/

/* マトリクスLED 0,1 段を左ローテ―ト	*/
static void task_1( void )
{
	unsigned int    timeout = 0;
	unsigned char	m = 1;

	for( ;; ) {
		PORTB	= m;
		PORTC	= 0x1c;
		PORTD	= 0xf0;
		if ((int)(tick - timeout) >= 0) {
			timeout += 500;
			m = (m << 1) | (m >> 7);
		}
	}
}

/* マトリクスLED 2,3 段を右ローテ―ト	*/
static void task_2( void )
{
	unsigned int    timeout = 0;
	unsigned char	m = 1;

	for( ;; ) {
		PORTB	= m;
		PORTC	= 0x03;
		PORTD	= 0xf0;
		if ((int)(tick - timeout) >= 0) {
			timeout += 200;
			m = (m >> 1) | (m << 7);
		}
	}
}

/* マトリクスLED 4,5 段を左ローテ―ト	*/
static void task_3( void )
{
	unsigned int    timeout = 0;
	unsigned char	m = 1;

	for( ;; ) {
		PORTB	= m;
		PORTC	= 0x0f;
		PORTD	= 0xc0;
		if ((int)(tick - timeout) >= 0) {
			timeout += 700;
			m = (m << 1) | (m >> 7);
		}
	}
}

/* マトリクスLED 6,7 段を右ローテ―ト	*/
static void task_4( void )
{
	unsigned int    timeout = 0;
	unsigned char	m = 1;

	for( ;; ) {
		PORTB	= m;
		PORTC	= 0x0f;
		PORTD	= 0x30;
		if ((int)(tick - timeout) >= 0) {
			timeout += 900;
			m = (m >> 1) | (m << 7);
		}
	}
}

int main(void)
{
	/*  LED ポートの設定    */
	DDRB    = 0xff;
	DDRC    = 0x1f;
	DDRD    = 0xfe;
	PORTB   = 0x42;

	/*    タスク切り替え割り込みの設定    */
	task_init();

	/*    タスクの生成と起動    */
	task_create(task_1);
	task_create(task_2);
	task_create(task_3);
	task_create(task_4);

	/*    タスクスケジュールの開始    */
	sei();
	for( ;; ) {           /*    開始時のみ    */
		wdt_reset();
	}

	return 0;
}

