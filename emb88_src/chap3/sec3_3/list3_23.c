/*
	list3_23.c	--	Non-preemptive Multitasking

	協調的マルチタスク： ディスパッチ・キュー

	Copyright(c) Recursion Co., Ltd. All rights reserved.
*/

#include <avr/interrupt.h>
#include <avr/wdt.h>
#include "list3_23.h"


#define	NULL	((void *)0)

#define	TASK_MAX    8

/*    関数型宣言        */
typedef void (*FUNC)(int);

/*    タスク管理構造    */
typedef struct {
	FUNC			func;
	int				param;
	unsigned int	time;
} TASK;


static volatile unsigned int	tick = 0;
static TASK		task[TASK_MAX];


//    タイマー割り込み処理
ISR( TIMER1_COMPA_vect )
{
	tick++;
}

void task_init( void )
{
	unsigned char	i;

	/*  Timer1 割り込み 32mS, CTC, 1/256  */
	OCR1A  = F_CPU / 8000 - 1;
	TCCR1A = 0;
	TCCR1B = 0x0c;
	TIMSK1 |= (1 << OCIE1A);

	for (i = 0; i < TASK_MAX; i++) {
		task[i].func = NULL;
	}
}

/*    タスク登録    */
char task_register(void (*func)(), int param, unsigned int delay)
{
	unsigned char	i;

	for (i = 0; i < TASK_MAX; i++) {
		if( task[i].func == NULL) {
			task[i].func    = func;
			task[i].param   = param;
			task[i].time    = tick + delay;
			return 0;
		}
	}
	return -1;
}

/*    タスク実行ディスパッチャ    */
void task_dispatch( void )
{
	unsigned char	i;

	for (i = 0; i < TASK_MAX; i++) {
		if( task[i].func != NULL && (int)(tick - task[i].time) >= 0) {
			task[i].func(task[i].param);
			task[i].func = NULL;
		}
		wdt_reset();
	}
}

