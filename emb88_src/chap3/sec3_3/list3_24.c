/*
	list3_24.c

	協調的マルチタスク： ディスパッチ・キュー

	Copyright(c) Recursion Co., Ltd. All rights reserved.
*/

#include <avr/io.h>
#include <avr/interrupt.h>
#include "list3_23.h"

// pitch
enum {
	C4 = 238, D4 = 212, E4 = 189, F4 = 178, G4 = 158, A4 = 141, B4 = 126,
	C5 = 118, D5 = 105, E5 = 94, F5 = 88, RST = 0
};

// interval
enum {
	L4 = 32,
	L16	= (L4>>2), L8 = (L4>>1), L2 = (L4<<1), L1 = (L4<<2),
	L8h = (L8+L16), L4h = (L4+L8), L2h = (L2+L4)
};

typedef struct NOTE
{
	unsigned char tone;
	unsigned char length;
} NOTE;

// melody
const NOTE melody[] = {
	{C4, L8}, {F4, L8},	{F4, L4h}, {G4, L8}, 
	{A4, L8}, {C5, L8}, {F5, L8}, {D5, L8}, {C5, L4},
	{D5, L8}, {F4, L8}, {F4, L4}, {G4, L4}, 
	{A4, L2}, {RST, L4},

	{A4, L8}, {D5, L8},	{C5, L4h}, {D5, L8}, 
	{F5, L8}, {D5, L8}, {C5, L8}, {D5, L8}, {C5, L8}, {A4, L8},
	{C5, L8}, {A4, L8},	{F4, L8}, {A4, L8},	{G4, L8}, {F4, L8},	
	{F4, L2}, {RST, L4}, 
	{RST, 0}
};

/*  タスク  (曲演奏）     */

static void sound_off(int param)
{
	TCCR2A = 0;		// 音停止	
}

static void sound_on(int n)
{
	const NOTE	*note = &melody[n++];

	if (note->length == 0) {
		return;
	}
	PORTC ^= 0x10;

	if (note->tone != RST) {
		// 音を発生
		OCR2A = note->tone;
		TCCR2A = 0x12;
		// 音を早めに止めるタスクを登録
		task_register(sound_off, 0, note->length-(note->length>>3));
	}
	// つぎの音を発生させるタスクを登録
	task_register(sound_on, n, note->length);
}

int main(void)
{
	DDRB = 0xff;
	DDRC = 0x3f;
	DDRD = 0xfa;
	PORTB = 0;
	PORTC = 0;
	PORTD = 0;

	// Timer2 ブザー音程  CTC, 1/64
	TCCR2B	= 0x04;

	task_init();
	task_register(sound_on, 0, L2);

	sei();
	for (;;) {
		task_dispatch();
	}
	return 0;
}
