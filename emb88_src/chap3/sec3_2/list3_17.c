/*
* list3_17.c	-- softUART
*
* Created: 2017/01/11 16:56:35
* Author : Osamu Tamura @ Recursion Co., Ltd.
*
* Usage  :
*	AtmelStudio / Tools -> Command Prompt
*		con2com com3 38400 list3_17.hex /
*		con2com com3 4800
*/

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/wdt.h>

#define SOFT_RX
#define SOFT_TX

#define BAUD		4800
#define DATA_BIT	8
#define PARITY		0	// even: 2, odd: 3

#define BUF_SZ	4

#define RX_BIT	(1<<0)
#define TX_BIT	(1<<1)

const unsigned char	baudrate = (unsigned char)((F_CPU / 8) / BAUD - 1);

static volatile unsigned char	errstat;

static volatile char	rx_state = 0;
static volatile char	tx_state = 0;
static volatile unsigned char	iwptr, urptr;
static volatile unsigned char	irptr, uwptr;
static volatile unsigned char	rbuf[BUF_SZ];
static volatile unsigned char	tbuf[BUF_SZ];

#ifdef SOFT_RX
static void sequence_rx()
{
	static unsigned char	data, parity;
	static signed char	databit, pcheck;

	switch (rx_state) {

		case 1:		// スタートビットの確認
			if (PIND & 1) {
				TIMSK0	&= ~_BV(OCIE0A);
				PCMSK2	|= _BV(PCINT16);
				rx_state = 0;
				break;
			}
			databit = DATA_BIT;
			pcheck = PARITY; // even: 2, odd: 3
			data = 0;
			parity = 0;
			errstat = 0;
			rx_state++;
			break;

		case 2:		// LSB -> MSB
			data >>= 1;
			if (PIND & 1) {
				data |= 0x80;
				parity++;
			}
			if (--databit == 0) {
				rx_state++;
				if (pcheck == 0) {
					rx_state++;
				}
			}
			break;

		case 3:		// パリティビットの検査
			if (PIND & 1) {
				parity++;
			}
			if ((parity+pcheck) & 1) {
				errstat |= 1;
			}
			rx_state++;
			break;

		default:	// ストップビットの確認
			if ((PIND & 1) == 0) {
				errstat |= 2;
			}
			// 受信データをバッファに格納
			rbuf[iwptr++] = data;
			iwptr &= (BUF_SZ - 1);

			// タイマ割り込みを停止
			TIMSK0	&= ~_BV(OCIE0A);
			// ピン変化割り込みを再開
			PCMSK2	|= _BV(PCINT16);
			PCIFR	|= _BV(PCIF2);
			rx_state = 0;
			break;
	}
}

//	スタートビット開始位置で割り込み
ISR(PCINT2_vect)
{
	if ((PIND & 1) == 0) {
		//	ピン変化割り込みを停止
		PCMSK2	&= ~_BV(PCINT16);

		//	0.5ビット時間後のタイマ割り込み起動
		OCR0A	= TCNT0 + (baudrate >> 1);	// 0.5
		TIFR0	= _BV(OCF0A);
		TIMSK0	|= _BV(OCIE0A);
		rx_state = 1;
	}
}

//	受信ビットの中間位置で割り込み
ISR(TIMER0_COMPA_vect)
{
	//	１ビット時間ごとにサンプリング
	OCR0A += baudrate;
	sequence_rx();
}
#endif

#ifdef SOFT_TX
static void sequence_tx()
{
	static unsigned char	data, parity;
	static unsigned char	databit, pcheck;

	switch (tx_state) {
		case 0:
			if (irptr == uwptr) {
				break;
			}
			//	１ビット時間ごとのタイマ割り込み起動
			OCR0B	= TCNT0 + baudrate;
			TIFR0	= _BV(OCF0B);
			TIMSK0 |= _BV(OCIE0B);
			tx_state++;

		case 1:
			// スタートビット送出
			PORTD &= ~TX_BIT;
			//	送出データの準備
			data = tbuf[irptr++];
			irptr &= (BUF_SZ - 1);
			databit = DATA_BIT;
			pcheck = PARITY; // even: 2, odd: 3
			parity = pcheck;
			tx_state++;
			break;

		case 2:		// LSB -> MSB
			if (data & 1) {
				PORTD |= TX_BIT;
				parity++;
			}
			else {
				PORTD &= ~TX_BIT;
			}
			data >>= 1;
			if (--databit == 0) {
				tx_state++;
				if (pcheck == 0) {
					tx_state++;
				}
			}
			break;

		case 3:		// パリティビット送出
			if (parity & 1) {
				PORTD |= TX_BIT;
			}
			else {
				PORTD &= ~TX_BIT;
			}
			tx_state++;
			break;

		case 4:		// ストップビット送出
			PORTD |= TX_BIT;
			if (irptr != uwptr) {
				tx_state = 1;
				break;
			}
			tx_state++;
			break;

		default:
			if (irptr != uwptr) {
				tx_state = 1;
				break;
			}
			// タイマ割り込みを停止
			TIMSK0 &= ~_BV(OCIE0B);
			tx_state = 0;
			break;
	}
}

//	送信ビットの開始位置で割り込み
ISR(TIMER0_COMPB_vect)
{
	//	１ビット時間ごとに送出
	OCR0B += baudrate;
	sequence_tx();
}
#endif


int main(void)
{
	DDRD	= TX_BIT;
	PORTD	= TX_BIT;

#ifdef SOFT_RX
	PCICR |= _BV(PCIE2);
	PCMSK2 = _BV(PCINT16);
#endif

	/*  タイマ設定 */
#if defined(SOFT_RX) || defined(SOFT_TX)
	TCCR0A	= 0x00;
	TCCR0B	= 0x02;	/*  1/8    */
#endif

	/*  USART設定 */
#if !defined(SOFT_RX) || !defined(SOFT_TX)
	UBRR0	= baudrate >> 1;
	UCSR0C	= (PARITY << 4) | ((DATA_BIT - 5) << 1);
#if !defined(SOFT_RX)
	UCSR0B	|= _BV(RXEN0);
#endif
#if !defined(SOFT_TX)
	UCSR0B	|= _BV(TXEN0);
#endif
#endif

	uwptr = irptr = 0;
	sei();
	for (;;) {
		wdt_reset();

		//	echo back

#if !defined(SOFT_RX) && !defined(SOFT_TX)
		if (UCSR0A & _BV(RXC0)) {
			UDR0 = UDR0;
		}
#endif

#if defined(SOFT_RX) && !defined(SOFT_TX)
		if (urptr != iwptr && UCSR0A & _BV(UDRE0)) {
			UDR0 = rbuf[urptr++];
			urptr &= (BUF_SZ - 1);
		}
#endif

#if !defined(SOFT_RX) && defined(SOFT_TX)
		if (UCSR0A & _BV(RXC0)) {
			tbuf[uwptr++]	= UDR0;
			uwptr &= (BUF_SZ - 1);
		}
		if (uwptr != irptr && tx_state == 0) {
			sequence_tx();
		}
#endif

#if defined(SOFT_RX) && defined(SOFT_TX)
		if (urptr != iwptr) {
			unsigned char	nptr;

			nptr = (uwptr + 1) & (BUF_SZ - 1);
			if (nptr != irptr) {
				tbuf[uwptr++] = rbuf[urptr++];
				urptr &= (BUF_SZ - 1);
				uwptr &= (BUF_SZ - 1);
			}
			if (uwptr != irptr && tx_state == 0) {
				sequence_tx();
			}
		}
#endif
	}

	return 0;
}
