/*
 * list3_19.c	-- PLC simulator
 *
 * Author : Osamu Tamura @ Recursion Co., Ltd.
 */

#if 1

#include <string.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include "plc.h"

extern volatile unsigned char	r, w;
extern volatile char	buf[];

#define PLS_X(n)	(!x[n]&&X[n])
#define _SET(x,y)	{if(y)x=-1;}
#define _RESET(x,y)	{if(y)x=0;}
#define _T(x,y,z)	(t[x]=(z)?(t[x]<0?y*100:t[x]):-1)
#define _C(x,y,p,r)	(cp[x]=p,c[x]=r?-1:(c[x]<0?y:c[x]))

volatile char	_M[M_MAX], M[M_MAX];
volatile int	 t[T_MAX];
volatile char	 T[T_MAX];
volatile int	 c[T_MAX];
volatile char	 cx[C_MAX], cp[C_MAX], C[C_MAX];

//	初期化
void plc_init(void)
{
	memset((void *)X, 0, sizeof(int)*X_MAX);
	memset((void *)_Y, 0, sizeof(int)*Y_MAX);
	memset((void *)_M, 0, sizeof(int)*M_MAX);
	memset((void *)t, -1, sizeof(int)*T_MAX);
	memset((void *)c, -1, sizeof(int)*C_MAX);
	memset((void *)cp, 0, sizeof(int)*C_MAX);
}

//	状態の更新 (D-FF)
static void update(void)
{
	unsigned char	i;

	//	入出力値の固定
	memcpy((void *)x, (const void *)X, sizeof(char)*X_MAX);
	memcpy((void *)Y, (const void *)_Y, sizeof(char)*Y_MAX);
	memcpy((void *)M, (const void *)_M, sizeof(char)*M_MAX);

	//	タイマの更新
	for (i=0; i < T_MAX; i++) {
		if (t[i] > 0) {
			t[i]--;
		}
		T[i] = !t[i];
	}
	//	カウンタの更新
	for (i=0; i < C_MAX; i++) {
		if (c[i] > 0 && !cx[i] && cp[i]) {
			c[i]--;
		}
		C[i] = !c[i];
		cx[i] = cp[i];
	}
}
	
//	変数表示（デバッグ用）
static void dump(void)
{
	unsigned char	i;

	for (i = 0; i < 8; i++) {
		if ((_M[i] && !M[i]) || (!_M[i] && M[i]))
		break;
	}
	if (i < 8) {
		for (i = 0; i < 8; i++) {
			buf[w++] = _M[i]? '1':'0';
		}
	}
}

//	スキャン
ISR( TIMER2_COMPA_vect )
{
	update();
	target_system();
	
	//	ラダーロジック
#include "ladder.c"

	dump();
}
#endif

