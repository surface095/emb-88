/*
 * plc.h
 *
 * Author : Osamu Tamura @ Recursion Co., Ltd.
 */
 
#define X_MAX	4		//	入力接点
#define Y_MAX	4		//	出力接点
#define M_MAX	8		//	内部リレー
#define T_MAX	8		//	タイマ
#define C_MAX	2		//	カウンタ


extern volatile char	 x[], X[];
extern volatile char	_Y[], Y[];

extern void plc_init(void);
extern void target_system(void);


