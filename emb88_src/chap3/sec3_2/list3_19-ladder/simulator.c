/*
* simulator.c	-- Cylinder Control Simulator
*
* Author : Osamu Tamura @ Recursion Co., Ltd.
*
* Usage  :
*	AtmelStudio / Tools -> Command Prompt
*		con2com com3 38400 list3_19.hex
*		SW1: start
*		SW2: object (press SW2 when the cylinder hits the right)
*/

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/wdt.h>
#include "plc.h"

#define BAUD	38400

volatile char	 x[X_MAX], X[X_MAX];
volatile char	_Y[Y_MAX], Y[Y_MAX];

volatile char	buf[256];
volatile unsigned char	r = 0, w = 0;

//
//	シリンダによるワーク検出システム
//
void target_system(void)
{
	static unsigned char	tick = 0;
	static unsigned char	pos = 1;

	//	スイッチ読み取り
	X[0] = ~PINC & 0x10;	//	開始ボタン(SW1)
	X[3] = ~PINC & 0x20;	//	ワーク検出(SW2)

	//	シリンダ制御（シミュレーション）
	if (++tick >= 100) {
		tick = 0;
		if (Y[0] && pos < 8) {	//	駆動信号
			pos++;
		}
		if (!Y[0] && pos > 1) {
			pos--;
		}
		X[1] = pos <= 1;		//	復帰検出
		X[2] = pos >= 8;		//	伸長検出
		PORTB = ~(0xff >> pos);
	}

	//	ワーク検出ランプ
	PORTB = (PORTB & ~1) | (Y[1]? 1:0);
}

/*  文字の送信  */
static void _putchar(unsigned char c)
{
	if (c == '\n') {
		_putchar('\r');
	}
	while ((UCSR0A & (1 << UDRE0)) == 0) {
		wdt_reset();
	}
	UDR0 = c;
}

int main(void)
{
	//	ラダー用タイマ割り込み設定
	OCR2A	= 124;			/* 1mS      */
	TCCR2A	= 0x02;
	TCCR2B	= 0x04;			/* 1/64     */
	TIMSK2	= (1 << OCIE2A);

	//	シリンダ表示用ポート設定
	DDRB	= 0xff;
	DDRC	= 0x0f;
	DDRD	= 0xfa;
	PORTB	= 0;
	PORTC	= 0x33;
	PORTD	= 0xc0;

	//	デバッグ用シリアルポート設定
	UBRR0 = ((F_CPU >> 3) + (BAUD >> 1)) / BAUD - 1;
	UCSR0A = (1 << TXC0) | (1 << U2X0);
	UCSR0B = (1 << TXEN0);

	{
		unsigned char	i;
		_putchar('\n');
		_putchar('\n');
		_putchar('M');
		for (i = 0; i < 8; i++)
			_putchar('0' + i);
	}
	plc_init();

	sei();
	for( ;; ) {	
		wdt_reset();
		while (r!=w) {
			if ((r&7)==0) {
				_putchar('\n');
				_putchar(' ');
			}
			_putchar(buf[r++]);
		}
	}
}

