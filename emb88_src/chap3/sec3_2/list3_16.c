/*
 * list3_16.c	-- Traffic Light 2
 *
 * Author : Osamu Tamura @ Recursion Co., Ltd.
 */

#include "common.h"

enum {
	SG_GREEN,
	SG_WALK_BLINK,
	SG_GREEN_RED,
	SG_YELLOW,
	SG_RED,
};

enum {
	TM_GREEN		= 16,
	TM_WALK_BLINK	= 8,
	TM_YELLOW		= 6,
	TM_BLINK		= 1
};

/*
	接続：
		PB6 -> 抵抗 -> 緑 LED（車両用）-> GND
		PB5 -> 抵抗 -> 黄 LED（車両用）-> GND
		PB4 -> 抵抗 -> 赤 LED（車両用）-> GND
		PB2 -> 抵抗 -> 緑 LED（歩行者用）-> GND
		PB1 -> 抵抗 -> 赤 LED（歩行者用）-> GND
*/
enum {
	LED_GREEN		= (1 << 6),
	LED_YELLOW		= (1 << 5),
	LED_RED			= (1 << 4),
	LED_WALK_GREEN	= (1 << 2),
	LED_WALK_RED	= (1 << 1),
};

int main(void)
{
	unsigned char	s = SG_YELLOW;
	unsigned char	dir = 0;
	unsigned char	wait = 1;
	unsigned char	blink = 0;
	unsigned char	button = 0;
	unsigned char	out = 0;
	unsigned char	outNS = 0;
	unsigned char	outEW = 0;

	init_clock(CLKPS_62500Hz);
	init_port();

	TCCR1A	= 0x00;
	TCCR1B	= 0x01;
	OCR1A	= 31249;	// 0.5 秒

	for (;;)
	{
		wdt_reset();
		if ((TIFR1 & (1 << OCF1A)) == 0) {
			continue;
		}
		TIFR1 = (1 << OCF1A);

		switch (s) {
			case SG_GREEN:
			//	一定時間後に　緑（歩行者） →　緑点滅（歩行者）
			if (--wait == 0) {
				out = LED_GREEN;
				wait = TM_WALK_BLINK;
				blink = TM_BLINK;
				s = SG_WALK_BLINK;
				break;
			}
			//	交差方向歩行者の押しボタン →　タイムアウトを早める
			if (wait >= 2) {
				button = sw();
				if ((dir == 0 && button == SW1) ||
					(dir == 1 && button == SW2)) {
					out ^= 0xff;
					wait = 2;
				}
			}
			break;

			case SG_WALK_BLINK:
			//	一定時間後に　緑点滅（歩行者）→ 赤（歩行者）／黄（車両）
			if (--wait == 0) {
				out = LED_YELLOW | LED_WALK_RED;
				wait = TM_YELLOW;
				s = SG_YELLOW;
				break;
			}
			//	緑点滅（歩行者）
			if (--blink == 0) {
				blink = TM_BLINK;
				out ^= LED_WALK_GREEN;
			}
			break;

			case SG_YELLOW:
			//	一定時間後に　黄（車両）→ 赤（車両）
			if (--wait == 0) {
				out = LED_GREEN | LED_WALK_GREEN;
				wait = TM_GREEN;
				s = SG_GREEN;
				dir ^= 1;	// 交差方向の切り替え
				break;
			}
			break;
		}

		// 信号パターンを出力
		if (dir) {
			// 南北方向
			outNS = out;
			outEW = LED_RED | LED_WALK_RED;
		}
		else {
			// 東西方向
			outNS = LED_RED | LED_WALK_RED;
			outEW = out;
		}
		PORTB = outNS;
	}
}


