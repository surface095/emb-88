/*
 * list3_14.c	-- Elevator
 *
 * Author : Osamu Tamura @ Recursion Co., Ltd.
 */

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/wdt.h>

#define SW	((~PINC >> 4) & 3)
#define LED_SZ	8

#define BEEP_OFF	0
#define BEEP_ON		0x44
#define BEEP_LOW	141	// 440Hz
#define	BEEP_HIGH	71	// 880Hz

//    表示のドットパターン
const unsigned char led[8][8] = {
	{ 0x18, 0x18, 0x18, 0x18, 0x7e, 0x3c, 0x18, 0x00 },	//	↓
	{ 0x08, 0x18, 0x08, 0x08, 0x08, 0x08, 0x1C, 0x00 },	//	1
	{ 0x3C, 0x42, 0x02, 0x0C, 0x10, 0x20, 0x7E, 0x00 },	//	2
	{ 0x3C, 0x42, 0x02, 0x3C, 0x02, 0x42, 0x3C, 0x00 },	//	3
	{ 0x04, 0x0C, 0x14, 0x24, 0x44, 0x7E, 0x04, 0x00 },	//	4
	{ 0x7E, 0x40, 0x40, 0x7C, 0x02, 0x02, 0x7C, 0x00 },	//	5
	{ 0x3C, 0x42, 0x40, 0x7C, 0x42, 0x42, 0x3C, 0x00 },	//	6
	{ 0x00, 0x18, 0x3c, 0x7e, 0x18, 0x18, 0x18, 0x18 }	//	↑
};

volatile unsigned char n = 0;	//	表示パターン
volatile unsigned char k = 0;	//	ローテート

/*  マトリクスLEDの走査	*/
ISR(TIMER0_COMPA_vect)
{
	static unsigned char sc = 0xfe;
	static unsigned char scan = 0;

	sc = (sc << 1) | (sc >> 7);
	PORTD = (PORTD & 0x0f) | (sc & 0xf0);
	PORTC = (PORTC & 0xf0) | (sc & 0x0f);
	scan = (scan + 1) & 7;
	PORTB = led[n][(scan + k) & 7];
}

/*  エレベーターの操作	*/
ISR(TIMER1_COMPA_vect)
{
	static unsigned char state = 1;		// ステート
	static unsigned char flr = 2;		// 階
	static unsigned char repeat = 1;	// 繰り返し

	TCCR2B = BEEP_OFF;

	switch (state) {
		case 0:		// 待機中
			if (SW == 1 && flr > 1) {		// 下降ボタン ON
				n = 0;
				repeat = 20;				// 2秒タイマ
				OCR2A = BEEP_LOW;
				TCCR2B = BEEP_ON;
				state = 1;
			}
			else if (SW == 2 && flr < 6) {	// 上昇ボタン ON
				n = 7;
				repeat = 30;				// 3秒タイマ
				OCR2A = BEEP_LOW;
				TCCR2B = BEEP_ON;
				state = 2;
			}
			break;

		case 1:		// 下降中
			repeat--;
			if (repeat == 0) {	// 到着
				flr--;
				n = flr;		// 到着階を表示
				k = 0;
				OCR2A = BEEP_HIGH;
				TCCR2B = BEEP_ON;
				state = 0;
			}
			else {
				k--;			// 下方向ローテ―ト
				if ((repeat & 7) == 0) {
					TCCR2B = BEEP_ON;
				}
			}
			break;

		case 2:		// 上昇中
			repeat--;
			if (repeat == 0) {	// 到着
				flr++;
				n = flr;		// 到着階を表示
				k = 0;
				OCR2A = BEEP_HIGH;
				TCCR2B = BEEP_ON;
				state = 0;
			}
			else {
				k++;			// 上方向ローテ―ト
				if ((repeat & 7) == 0) {
					TCCR2B = BEEP_ON;
				}
			}
			break;
	}
}

int main(void)
{
	DDRB = 0xff;
	DDRC = 0x0f;
	DDRD = 0xfe;
	PORTC = 0x30;

	/*  ダイナミックスキャン割り込み  */
	OCR0A = 249;	/*  2mS     */
	TCCR0A = 2;
	TCCR0B = 3;	/*  1/64    */
	TIMSK0 = (1 << OCIE0A);

	/*  ステートマシン割り込み  */
	OCR1A = 3124;	/*  100mS     */
	TCCR1A = 0;
	TCCR1B = 0x0c;	/*  1/256    */
	TIMSK1 = (1 << OCIE1A);

	/*  ブザー用タイマ	*/
	TCCR2A = 0x12;
	TCCR2B = 0;

	sei();
	for (;;) {
		wdt_reset();
	}
	return 0;
}

