/*
 * list3_13.c	-- State Machine
 *
 * Author : Osamu Tamura @ Recursion Co., Ltd.
 */

#include "common.h"

enum {
	ST_OFF = SWOFF,
	ST_SW1 = SW1,
	ST_SW2 = SW2,
	ST_SW3 = SW3,
	ST_RELEASE
};

int main(void)
{
	char	state = ST_RELEASE;

	init_port();

	TCCR0A	= 0x00;
	TCCR0B	= 0x05;	// normal, 1/1024 clk

	for (;;)
	{
		wdt_reset();
		if ((TIFR0 & (1 << TOV0)) == 0) {
			continue;
		}
		TIFR0 = (1 << TOV0);

		switch (state) {
			case ST_OFF:	state = sw();
							break;

			case ST_SW1:	PORTB = (PORTB << 1) | 1;
							state = ST_RELEASE;
							break;

			case ST_SW2:	PORTB >>= 1;
							state = ST_RELEASE;
							break;

			default:		if (sw() == SWOFF) {
								state = ST_OFF;
							}
		}
	}
}

