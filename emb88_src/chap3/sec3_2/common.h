/*
 * common.h
 *
 * Author : Osamu Tamura @ Recursion Co., Ltd.
 */

#ifndef _COMMON_H_
#define _COMMON_H_

#include <stdint.h>
#include <string.h>
#include <avr/io.h>
#include <avr/wdt.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>

#if F_CPU == 8000000UL
#define CLKPS_8MHz		(0)
#define CLKPS_1MHz		((1<<CLKPS1)|(1<<CLKPS0))
#define CLKPS_500kHz	(1<<CLKPS2)
#define CLKPS_250kHz	((1<<CLKPS2)|(1<<CLKPS0))
#define CLKPS_125kHz	((1<<CLKPS2)|(1<<CLKPS1))
#define CLKPS_62500Hz	((1<<CLKPS2)|(1<<CLKPS1)|(1<<CLKPS0))
#define CLKPS_31250Hz	(1<<CLKPS3)
#elif F_CPU == 16000000UL
#define CLKPS_16MHz		(0)
#define CLKPS_2MHz		((1<<CLKPS1)|(1<<CLKPS0))
#define CLKPS_1MHz		(1<<CLKPS2)
#define CLKPS_500kHz	((1<<CLKPS2)|(1<<CLKPS0))
#define CLKPS_250kHz	((1<<CLKPS2)|(1<<CLKPS1))
#define CLKPS_125kHz	((1<<CLKPS2)|(1<<CLKPS1)|(1<<CLKPS0))
#define CLKPS_62500Hz	(1<<CLKPS3)
#endif

#define TM1	3000
#define	TM2	4000

extern void init_clock(unsigned char scale);
extern void init_port();
extern unsigned char sw();
extern void do_something(unsigned char x);
extern void display_init(void);
extern void display_data(char *data, unsigned char offset, unsigned char num);

enum {
	SWOFF = 0,
	SW1,
	SW2,
	SW3
};

#endif	// _COMMON_H_



