
/*
	ladder2c.c

	ラダー図をC言語の論理式に変換

	Created: 2017/09/11 13:26:55
 
	Copyright(c) 2017 Osamu Tamura @ Recursion Co., Ltd.
	All rights reserved.
*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>

#define	TOK_SZ	64
#define	TBUF_SZ	1024
#define	CBUF_SZ	256

static char	*cp;
static char	*tok[TOK_SZ];
static char	tbuf[TBUF_SZ];
static char	cbuf[CBUF_SZ];

static int	rp;
static int	pls;
static char	*cnt;

static int gen_num(char *t)
{
	char	*u = t;

	while ('0' <= *t && *t <= '9') {
		*cp++ = *t++;
	}
	return (int)(t - u);
}

static int gen_obj(char *t)
{
	char	*u = t;
	int		eq = 0;

	*cp++ = ' ';
	if (pls) {	// pulse (rising edge)
		t++;
		*cp++ = 'P';
		*cp++ = 'L';
		*cp++ = 'S';
		*cp++ = '_';
		*cp++ = *t++;
		*cp++ = '(';
		t += gen_num(t);
		*cp++ = ')';
		goto end;
	}
	if (*t == 'P') {
		t++;
		pls = 1;
	}
	switch (*t) {
	case '/':	*cp++ = '!';	break;
	case '(':	cp--;	*cp++ = '_';	eq = 1;		break;
	case '[':
		t++;
		cp--;
		*cp++ = '_';
		*cp++ = *t++;
		*cp++ = '(';
		t += gen_num(t) + 1;
		*cp++ = ',';
		*cp++ = ' ';
		t += gen_num(t);
		*cp++ = ',';
		rp = 1;
		if (*t == ',') {
			cnt = t;
		}
		goto end;
	}
	t++;
	*cp++ = *t++;
	*cp++ = '[';
	t += gen_num(t);
	*cp++ = ']';

	if (eq) {
		*cp++ = ' ';
		*cp++ = '=';
	}
end:
	return (int)(t - u);
}

static int split(char *p[], int n, char *s, const char *d)
{
	char	*t;
	int		i = 0;

	t = strtok(s, d);
	while (t != NULL && i < n) {
		p[i++] = t;
		t = strtok(NULL, d);
	}
	if (i) {
		p[i] = p[i-1] + strlen(p[i-1]) + 1;
	}
	return i;
}

static int parse_ladder(char *p, int k)
{
	int		i;
	int		n = -1;
	char	*t;

	//	obtain a rung
	do {
		if (fgets(p, (int)(tbuf + TBUF_SZ - p), stdin) == NULL) {
			break;
		}
		n = split(&tok[k], TOK_SZ - k, p, "\t\r\n -)]");

	} while (n < 3 || *tok[k] != '|' || *tok[k + n - 1] != '|');
	if (n < 3) {
		return -1;
	}
	n -= 2;

	//	generate substitution
	t = tok[k + n];
	if (*t != '/') {
		t += gen_obj(t);
	}

	//	count '|' branch
	for (i = 1; i < n; i++) {
		t = tok[k + i];

		if (*t == '|' && *(t + 1) == 0) {
			*cp++ = ' ';
			*cp++ = '(';
		}
	}

	//	generate C formula
	for (i = 1; i < n; i++) {
		t = tok[k + i];

		if (*t == '|' && *(t + 1) == 0) {
			if (i == 1) {
				*cp++ = ' ';
				*cp++ = '1';
			}
			*cp++ = ' ';
			*cp++ = '|';
			*cp++ = '|';
			parse_ladder(tok[k + n], k + n);
			*cp++ = ' ';
			*cp++ = ')';
		}
		else {
			if (i != 1) {
				*cp++ = ' ';
				*cp++ = '&';
				*cp++ = '&';
			}
			t += gen_obj(t);
		}
	}
	return n;
}


int main(int argc, char* argv[])
{
	for (;;) {
		cp = cbuf;
		rp = 0;
		pls = 0;
		cnt = NULL;
		*cp++ = '\t';
		if (parse_ladder(tbuf, 0) < 0) {
			break;
		}
		if (rp) {
			if (cnt) {
				*cp++ = ',';
				gen_obj(cnt);
			}
			*cp++ = ')';
		}
		*cp++ = ';';
		*cp = 0;
		puts(cbuf);
	}
	return 0;
}
