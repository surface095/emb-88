/*
 * list3_15.c	-- Traffic Light 1
 *
 * Author : Osamu Tamura @ Recursion Co., Ltd.
 */

#include "common.h"

enum {
	NS_GREEN,
	NS_YELLOW,
	NS_RED,
};

enum {
	TM_GREEN		= 12,
	TM_YELLOW		= 4,
	TM_RED			= (TM_GREEN+TM_YELLOW),
};

/*
	接続：
		PB2 -> 抵抗 -> 緑 LED（車両用）-> GND
		PB1 -> 抵抗 -> 黄 LED（車両用）-> GND
		PB0 -> 抵抗 -> 赤 LED（車両用）-> GND
*/
enum {
	LED_GREEN		= (1 << 2),
	LED_YELLOW		= (1 << 1),
	LED_RED			= (1 << 0),
};


int main(void)
{
	unsigned char	s = NS_GREEN;
	unsigned char	wait = TM_GREEN;

	init_clock(CLKPS_62500Hz);
	init_port();

	PORTB	= LED_GREEN;

	TCCR1A	= 0x00;
	TCCR1B	= 0x01;
	OCR1A	= 31249;		// 0.5 Sec

	for (;;)
	{
		wdt_reset();
		if ((TIFR1 & (1 << OCF1A)) == 0) {
			continue;
		}
		TIFR1 = (1 << OCF1A);

		switch (s) {
			case NS_GREEN:
				if (--wait == 0) {		// 次の状態に移る条件
					PORTB = LED_YELLOW;	// 次の状態の初期化処理
					wait = TM_YELLOW;
					s = NS_YELLOW;
					break;
				}
				// いまの状態での処理
				break;

			case NS_YELLOW:
				if (--wait == 0) {
					PORTB = LED_RED;
					wait = TM_RED;
					s = NS_RED;
				}
				break;

			case NS_RED:
				if (--wait == 0) {
					PORTB = LED_GREEN;
					wait = TM_GREEN;
					s = NS_GREEN;
				}
				break;
		}
	}
}


