#include <avr/io.h>
#include <avr/wdt.h>

#define CTOP  5000UL
#define STEP  100

int main(void)
{
	unsigned long cnt = 0;		// 32bit
	unsigned long cmp = 0;

	DDRC = 0x30;	// PC5/4を出力ピンに設定
	PORTC = 0x10;	// PC5/4の出力をL/Hに設定

	for (;;) {
		wdt_reset();	// WDTをリセット
		cnt++;
		if (cnt >= CTOP) {
			cnt = 0;
			PORTC |= 0x10;	// PC4の出力をH
			cmp += STEP;
			if (cmp >= CTOP) {
				cmp = 0;
			}
		}
		if (cnt == cmp) {
			PORTC &= ~0x10;	// PC4の出力をL
		}
	}

	return 0;
}
