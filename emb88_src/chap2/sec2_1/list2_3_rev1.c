#include <avr/io.h>
#include <avr/wdt.h>

#define CTOP 200000UL

int main(void)
{
	unsigned long cnt = 0;		// 32bit
	unsigned int p = 0;

	DDRC = 0x30;	// PC5/4を出力ピンに設定
	PORTC = 0x30;	// PC5/4の出力をH/Hに設定

	for (;;) {
		wdt_reset();	// WDTをリセット
		p++;
		if (p == 4) {	// プリスケール 4
			p = 0;
			cnt++;
		}
		if (cnt >= CTOP) {
			cnt = 0;
			PORTC ^= 0x10;	// PC4の出力値を反転
		}
	}
	return 0;
}
