#include <avr/io.h>
#include <avr/wdt.h>

#define DUTY  0.8
#define CTOP  250000UL
#define CMP   ((unsigned long)(DUTY*CTOP))	// 比較値

int main(void)
{
	unsigned long cnt = 0;		// 32bit

	DDRC = 0x30;	// PC5/4を出力ピンに設定
	PORTC = 0x10;	// PC5/4の出力をL/Hに設定

	for (;;) {
		wdt_reset();	// WDTをリセット
		cnt++;
		if (cnt >= CTOP) {
			cnt = 0;
			PORTC |= 0x10;	// PC4の出力をH
		}
		if (cnt == CMP) {
			PORTC &= ~0x10;	// PC4の出力をL
		}
	}

	return 0;
}
