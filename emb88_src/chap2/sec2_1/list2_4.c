#include <avr/io.h>
#include <avr/wdt.h>

#define CTOP   200000UL
#define CMP    150000UL	// 比較値

int main(void)
{
	unsigned long cnt = 0;		// 32bit

	DDRC = 0x30;	// PC5/4を出力ピンに設定
	PORTC = 0x30;	// PC5/4の出力をH/Hに設定

	for (;;) {
		wdt_reset();	// WDTをリセット
		cnt++;
		if (cnt >= CTOP) {
			cnt = 0;
			PORTC ^= 0x10;	// PC4の出力反転
		}
		if (cnt == CMP) {
			PORTC ^= 0x20;	// PC5の出力反転
		}
	}

	return 0;
}
