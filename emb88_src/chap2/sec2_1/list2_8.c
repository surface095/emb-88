#include <avr/io.h>
#include <avr/wdt.h>

#define LED_CTOP 100000UL
#define BZ_CTOP 1000UL

void proc_led()
{
	static unsigned long cnt = 0;	// 32bit
	cnt++;
	if (cnt < LED_CTOP) {
		return;
	}
	cnt = 0;
	PORTC ^= 0x10;
}

void proc_bz()
{
	static unsigned long cnt = 0;	// 32bit
	cnt++;
	if (cnt < BZ_CTOP) {
		return;
	}
	cnt = 0;
	PORTD ^= 0x08;
}

int main(void)
{
	DDRC = 0x30;
	DDRD = 0x08;

	for (;;) {
		wdt_reset();
		proc_led();
		if (PORTC & 0x10) {	// LED1�_�����Ȃ�
			proc_bz();
		}
	}

	return 0;
}
