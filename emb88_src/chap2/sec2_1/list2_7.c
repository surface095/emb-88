#include <avr/io.h>
#include <avr/wdt.h>

#define CTOP 2000UL

int main(void)
{
	unsigned long cnt = 0;		// 32bit

	DDRD = 0x08;	// PD3を出力ピンに設定

	for (;;) {
		wdt_reset();	// WDTをリセット
		cnt++;
		if (cnt >= CTOP) {
			cnt = 0;
			PORTD ^= 0x08;
		}
	}

	return 0;
}
