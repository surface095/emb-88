#include <string.h>
#include "user.h"

/*	ローカル関数	*/
static void MoveFort(void);
static void MoveEnemy(void);
static void MoveBullet(void);
static void UpdateLED(void);

/*	ローカル変数	*/
static uchar enemy[LED_SZ];		// 敵の位置      
static uchar bullet[LED_SZ];	// 弾の位置       
static uchar fort;				// 砲台の中心位置  

/* ユーザー処理の初期化 */
void user_init(void)
{
	uchar i;

	for (i = 0; i < LED_SZ; i++) {
		enemy[i] = 0;
		bullet[i] = 0;
	}
	fort = 3;
}

/*	ユーザー処理（100mS 毎に呼ばれる）*/
void user_main(void)
{
	MoveFort();
	MoveEnemy();
	MoveBullet();
	UpdateLED();
}

/*	砲台の移動 */
static void MoveFort(void)
{
	switch (sw) {
	case 1:	// 左へ移動 
		if (fort > 1) {
			fort--;
		}
		break;
	case 2:	//  右へ移動
		if (fort < (LED_SZ - 2)) {
			fort++;
		}
		break;
	case 3:	//  弾の発射
		if (sw_flag) {
			bullet[7] |= 0x140 >> fort;
			sw_flag = 0;
		}
		break;
	}
	sw_flag = 0;
}

/*	敵の移動 */
static void MoveEnemy(void)
{
	static uchar tick = 0;
	static uchar dir = 0;
	uchar i;

	if (++tick < 5)
		return;
	tick = 0;
	_sound(_rand() | 0x80, 1);
	switch (dir) {
	case 0:	//  右へ移動
		for (i = 0; i < LED_SZ; i++) {
			enemy[i] >>= 1;
		}
		break;
	case 2:	//  左へ移動
		for (i = 0; i < LED_SZ; i++) {
			enemy[i] <<= 1;
		}
		break;
	default:	//  下へ移動
		if (enemy[7]) {
			_sound(10, 1);
			memset(enemy, 0, 8);
			_wait(5);
		}
		for (i = LED_SZ - 1; i; i--) {
			enemy[i] = enemy[i - 1];
		}
		enemy[i] = dir == 3 ? (_rand() & 0xFE) : 0;
		break;
	}
	dir = (dir + 1) & 3;
}

/*	弾の移動 */
static void MoveBullet(void)
{
	uchar i, j;

	//  上へ移動 
	for (i = 0; i < (LED_SZ - 1); i++) {
		bullet[i] = bullet[i + 1];
	}
	bullet[i] = 0;

	//  敵に当たれば消滅
	for (i = 0; i < LED_SZ; i++) {
		j = enemy[i] & bullet[i];
		if (j) {
			enemy[i] ^= j;
			bullet[i] ^= j;
			_sound(60, 1);
		}
	}
}

/*	LED表示の更新*/
static void UpdateLED(void)
{
	uchar i;

	//  敵と弾の合成
	for (i = 0; i < LED_SZ; i++) {
		led[i] = enemy[i] | bullet[i];
	}

	//  砲台の合成
	led[6] |= (uchar)(0x80 >> fort);
	led[7] |= (uchar)(0x1C0 >> fort);
}
