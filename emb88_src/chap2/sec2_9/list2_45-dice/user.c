#include <string.h>
#include "user.h"

static uchar pat[7][8] = { 
{0xE4, 0x94, 0x94, 0xE0, 0x06, 0x6F, 0x88, 0x66},	// タイトル
{0x00, 0x00, 0x00, 0x18, 0x18, 0x00, 0x00, 0x00},	// 1の目
{0x00, 0x0C, 0x0C, 0x00, 0x00, 0x30, 0x30, 0x00},	// 2の目
{0x06, 0x06, 0x00, 0x18, 0x18, 0x00, 0x60, 0x60},	// 3の目
{0x00, 0x66, 0x66, 0x00, 0x00, 0x66, 0x66, 0x00},	// 4の目
{0x66, 0x66, 0x00, 0x18, 0x18, 0x00, 0x66, 0x66},	// 5の目
{0x66, 0x66, 0x00, 0x66, 0x66, 0x00, 0x66, 0x66},	// 6の目
};

void user_init(void)
{
	_sound(33, 2);
}

void user_main(void)
{
	static char stat = 0, cnt = 0, n = 0;
	switch (stat) {
	case 0:	// タイトル画面
		n = 0;
		if (sw_flag) {
			sw_flag = 0;
			switch (sw) {
			case 1:
				_sound(30, 1);
				stat = 1;
				break;
			case 2:
				_sound(250, 1);
				break;
			}
		}
		goto draw;
	case 1:	// サイコロ回転中
		n = _rand() % 6 + 1;
		if (sw_flag) {
			sw_flag = 0;
			switch (sw) {
			case 1:
				_sound(250, 1);
				stat = 0;
				break;
			case 2:
				cnt = (_rand() & 0xF) + 4;
				stat = 2;
				break;
			}
		}
		goto draw;
	case 2:	// サイコロ停止
		cnt--;
		n = (n + 1) % 6 + 1;
		if (cnt & 0x01) {
			_sound(20, 1);
		}
		if (cnt == 0) {
			_sound(22, 1);
			_wait(15);
			stat = 1;
		}
		goto draw;
	}
  draw:
	memcpy(led, pat[n], 8);
}
