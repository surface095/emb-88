#include <avr/io.h>
#include <avr/wdt.h>
#include <avr/interrupt.h>

#define BAUD 38400

volatile unsigned char c;

/* 定期的にスイッチ情報状態をPCに送信する */
ISR(TIMER1_COMPA_vect)
{
	c = ((~PINC >> 4) & 3) + 0x30;	// スイッチ状態をASCIIコードに直す
	UCSR0B |= _BV(UDRIE0);	// 空き割り込み有効化
}

ISR(USART_UDRE_vect)
{
	UDR0 = c;
	UCSR0B &= ~_BV(UDRIE0);	// 空き割り込み無効化
}

int main(void)
{
	DDRD = 0xFE;
	DDRC = 0x0F;
	DDRB = 0xFF;

	PORTD = 0x00;
	PORTC = 0x30;
	PORTB = 0x00;

	/* 通信速度設定 */
	UBRR0 = (F_CPU >> 4) / BAUD - 1;
	UCSR0A = 0;

	/* データ8ビット，パリティなし，ストップビット1個 */
	UCSR0C = 0x06;

	/* 送信器の有効化 */
	UCSR0B = _BV(TXEN0);
	sei();

	/* タイマ割り込みの設定 */
	OCR1A = 3000;
	TCCR1A = 0x00;
	TCCR1B = 0x0D;
	TIMSK1 = _BV(OCIE1A);	// COMP A割り込み有効

	for (;;) {
		wdt_reset();
	}
	return 0;
}
