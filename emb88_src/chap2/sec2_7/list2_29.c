#include <avr/io.h>
#include <avr/wdt.h>

#define BAUD 38400

int main(void)
{
	char c;

	DDRD = 0xFE;
	DDRC = 0x3F;
	DDRB = 0xFF;

	PORTD = 0x00;
	PORTC = 0x00;
	PORTB = 0x00;

	/* 通信速度設定 */
	UBRR0 = (F_CPU >> 4) / BAUD - 1;
	UCSR0A = 0;

	/* データ8ビット，パリティなし，ストップビット1個 */
	UCSR0C = 0x06;

	/* 送受信有効化 */
	UCSR0B = _BV(RXEN0) | _BV(TXEN0);

	for (;;) {
		wdt_reset();

		/* 受信待ち */
		while ((UCSR0A & _BV(RXC0)) == 0) {
			wdt_reset();
		}
		c = UDR0;	// 受信データ取り出し

		/* 送信バッファ空きを待つ */
		while ((UCSR0A & _BV(UDRE0)) == 0) {
			wdt_reset();
		}
		UDR0 = c;	// 送信バッファへ書き込む(エコーバック)

		PORTB++;	// エコーバック回数をマトリクスLEDに表示
	}
	return 0;
}
