#include<avr/io.h>
#include<avr/wdt.h>
#include<avr/interrupt.h>
#include<stdlib.h>	// atoi

#define BAUD 38400
#define N 8	// バッファサイズ

/*  文字受信割り込み    */
ISR(USART_RX_vect)
{
	static char buf[N];			// 文字バッファ
	static int n = 0;			// 文字バッファ中のデータ数

	buf[n] = UDR0;
	n++;
	if (N <= n) {
		n = 0;
		return;
	}
	if (buf[n - 1] == '\r') {
		PORTB = OCR2A = atoi(buf);
		n = 0;
	}
}

int main(void)
{
	DDRD = 0xFE;
	DDRC = 0x0F;
	DDRB = 0xFF;

	PORTD = 0x00;
	PORTC = 0x30;
	PORTB = 0x00;

	/* 通信速度設定 */
	UBRR0 = (F_CPU >> 4) / BAUD - 1;
	UCSR0C = 0x06;

	// 受信（割り込み）
	UCSR0B = _BV(RXCIE0) | _BV(RXEN0);

	// タイマ2(CTC,COM)
	TCCR2B = 0x44;
	TCCR2A = 0x12;
	OCR2A = 0;

	sei();
	for (;;) {
		wdt_reset();
	}
	return 0;
}
