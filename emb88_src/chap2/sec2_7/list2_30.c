#include <avr/io.h>
#include <avr/wdt.h>

#define BAUD 38400

int main(void)
{
	unsigned char c, recv_flag = 0;

	DDRD = 0xFE;
	DDRC = 0x0F;
	DDRB = 0xFF;

	PORTD = 0x00;
	PORTC = 0x30;
	PORTB = 0x00;

	/* 通信速度設定 */
	UBRR0 = (F_CPU >> 4) / BAUD - 1;
	UCSR0A = 0;

	/* データ8ビット，パリティなし，ストップビット1個 */
	UCSR0C = 0x06;

	/* 送受信有効化 */
	UCSR0B = _BV(RXEN0) | _BV(TXEN0);

	for (;;) {
		wdt_reset();

		/* データを受信したら，マトリクスLEDに表示する */
		if (UCSR0A & _BV(RXC0)) {
			c = UDR0;
			recv_flag = 1;
		}

		if (recv_flag && (UCSR0A & _BV(UDRE0))) {
			recv_flag = 0;
			UDR0 = c;
			PORTB++;
		}

	}
	return 0;
}
