#include<avr/io.h>
#include<avr/wdt.h>
#include<avr/interrupt.h>

#include "ringbuf.h"
#define BAUD 38400

/* * * * * * * * * 受信専用処理 * * * * * * * * * * * */
ISR(USART_RX_vect)
{	/*  文字受信割り込み    */
	char c;
	c = UDR0;
	if (!rx_write(c)) {	// 受信リングバッファに移す書き込む
		PORTB = (PORTB << 1) | 1;	// バッファ状態を表示
	}
}

/* * * * * * * * * * 送信専用処理 * * * * * * * * * * * */
ISR(USART_UDRE_vect)
{	/*  送信データレジスタ空割り込み */
	char c;
	if (!tx_read(&c)) {	// 送信リングバッファから取り出す
		UDR0 = c;	// 送信
	}
	else {
		UCSR0B &= ~_BV(UDRIE0);	// 空き割り込み無効化
	}
}

/* * * * * * * * *  アプリケーション * * * * * * * * * */

ISR(TIMER1_COMPA_vect)
{
	char x;
	char tone[] = { 238, 212, 189, 174, 158, 141, 126, 118 };

	OCR2A = 0;
	if (!rx_read(&x)) {	// 受信リングバッファから取り出す
		UCSR0B |= _BV(UDRIE0);	// 空き割り込み有効化 
		PORTB >>= 1;

		if ((x >= '1') && (x <= '8')) {
			OCR2A = tone[x - '1'];
			tx_write(x);	// 送信リングバッファに書き込む            
		}
		else {
			tx_write('*');
		}
	}
}

int main(void)
{
	DDRD = 0xFE;
	DDRC = 0x0F;
	DDRB = 0xFF;

	PORTD = 0x00;
	PORTC = 0x30;
	PORTB = 0x00;

	/* 通信速度設定 */
	UBRR0 = (F_CPU >> 4) / BAUD - 1;
	UCSR0A = 0;

	/* データ8ビット，パリティなし，ストップビット1個 */
	UCSR0C = 0x06;
	UDR0 = 0;

	/* 送受信の有効化，受信割り込み有効化 */
	UCSR0B = _BV(RXCIE0) | _BV(RXEN0) | _BV(TXEN0);

	// タイマ1(定期割り込み用)
	TCNT1 = 0;
	OCR1A = 2000;	// 比較値(チャンネルA)
	TCCR1A = 0x00;
	TCCR1B = 0x0D;
	TIMSK1 = _BV(OCIE1A);	// COMP A 割り込み有効

	// タイマ2(ブザー用)
	TCCR2B = 0x04;
	TCCR2A = 0x12;
	OCR2B = OCR2A = 0;

	sei();

	for (;;) {
		wdt_reset();
	}
	return 0;
}
