#include"ringbuf.h"

struct ringbuffer
{
	char buf[N];				// バッファ
	int n;						// リングバッファ中のデータ数
	int w;						// 次にデータを書きこむ場所
	int r;						// 次にデータを読み出す場所
};

static int write(struct ringbuffer *rb, char v)
{
	if (N <= rb->n)
		return 1;	// データ満杯
	rb->n++;
	rb->buf[rb->w++] = v;
	if (rb->w == N)
		rb->w = 0;
	return 0;
}

static int read(struct ringbuffer *rb, char *vp)
{
	if (rb->n <= 0)
		return 1;	// データ空
	rb->n--;
	*vp = rb->buf[rb->r++];
	if (rb->r == N)
		rb->r = 0;
	return 0;
}


/* * * * * * * * * * * * * * */
static struct ringbuffer fifo1;	// 受信用リングバッファ
static struct ringbuffer fifo2;	// 送信用リングバッファ

int rx_read(char *vp)
{
	return read(&fifo1, vp);
}

int rx_write(char v)
{
	return write(&fifo1, v);
}

int tx_read(char *vp)
{
	return read(&fifo2, vp);
}

int tx_write(char v)
{
	return write(&fifo2, v);
}
