#ifndef RINGBUF_H
#   define RINGBUF_H

#   define N 8
			// リングバッファサイズ

int rx_read(char *);
int rx_write(char);
int tx_read(char *);
int tx_write(char);

#endif
