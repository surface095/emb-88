#include <avr/io.h>
#include <avr/wdt.h>
#include <avr/interrupt.h>

#define BAUD 38400

volatile unsigned char c;

ISR(USART_UDRE_vect)
{
	UDR0 = c;
	UCSR0B &= ~_BV(UDRIE0);	// 空き割り込み無効化
}

ISR(USART_RX_vect)
{
	PORTB++;
	c = UDR0;
	UCSR0B |= _BV(UDRIE0);	// 空き割り込み有効化
}

int main(void)
{
	DDRD = 0xFE;
	DDRC = 0x3F;
	DDRB = 0xFF;

	PORTD = 0x00;
	PORTC = 0x30;
	PORTB = 0x00;

	/* 通信速度設定 */
	UBRR0 = (F_CPU >> 4) / BAUD - 1;
	UCSR0A = 0;

	/* データ8ビット，パリティなし，ストップビット1個 */
	UCSR0C = 0x06;

	/* 送受信の有効化，受信割り込み有効化 */
	UCSR0B = _BV(RXCIE0) | _BV(RXEN0) | _BV(TXEN0);

	sei();

	for (;;) {
		wdt_reset();
	}
	return 0;
}
