#include <avr/io.h>
#include <avr/wdt.h>
#include <avr/interrupt.h>

#define CTOP 1000000UL

int main()
{
	unsigned char stat = 0;
	unsigned long cnt = 0;		// 32bit   

	DDRB = 0xFF;
	DDRC = 0x0F;
	DDRD = 0xFE;

	PORTB = 0x00;
	PORTC = 0x30;	// 入力ピンをプルアップ
	PORTD = 0x00;

	PCMSK1 = 0x30;	// PCINT12/13を許可

	for (;;) {
		wdt_reset();
		PORTB = PCIFR;	// フラグレジスタを表示

		switch (stat) {
		case 0:	// アイドル状態
			if (PCIFR & _BV(PCIF1)) {
				cnt = CTOP;
				stat = 1;
			}
			break;
		case 1:	// カウント
			cnt--;
			if (cnt == 0) {
				PCIFR = _BV(PCIF1);	// フラグクリア
				stat = 0;
			}
			break;
		}
	}
	return 0;

}
