#include <avr/io.h>
#include <avr/wdt.h>
#include <avr/interrupt.h>

ISR(PCINT1_vect)
{
	switch ((~PINC >> 4) & 0x3) {
	case 0:
		break;
	case 1:
		PORTB = (PORTB >> 7) | (PORTB << 1);
		break;
	case 2:
		PORTB = (PORTB << 7) | (PORTB >> 1);
		break;
	case 3:
		PORTB = ~PORTB;
		break;
	}
}

int main()
{
	DDRB = 0xFF;
	DDRC = 0x0F;
	DDRD = 0xFE;

	PORTB = 0x18;
	PORTC = 0x30;	// 入力ピンをプルアップ
	PORTD = 0x00;

	PCICR = _BV(PCIE1);	// ピン変化割り込み1有効
	PCMSK1 = 0x30;	// PCINT12/13を許可
	sei();	// 割り込み受付開始

	for (;;) {
		wdt_reset();
	}
	return 0;

}
