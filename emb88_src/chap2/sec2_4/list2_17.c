#include <avr/io.h>
#include <avr/wdt.h>
#include <avr/interrupt.h>

#define CTOP 10000UL

volatile unsigned char stat;
unsigned char sw;				// スイッチ変数
unsigned char sw_flag;			// スイッチ変化を示すフラグ

ISR(PCINT1_vect)	// ピン変化割り込みハンドラ
{
	stat = 1;
}

void update_sw()
{
	static unsigned long cnt;
	switch (stat) {
	case 0:
		return;
	case 1:
		cnt = CTOP;
		stat = 2;
		return;
	case 2:
		cnt--;
		if (cnt == 0) {
			sw = ~(PINC >> 4) & 3;	// 変数swを更新
			sw_flag = 1;	// フラグを立てる
			stat = 0;
		}
		return;
	}
}

int main()
{
	DDRB = 0xFF;
	DDRC = 0x0F;
	DDRD = 0xFE;

	PORTB = 0x18;
	PORTC = 0x30;	// 入力ピンをプルアップ
	PORTD = 0x00;

	PCICR = _BV(PCIE1);
	PCMSK1 = 0x30;
	sei();

	for (;;) {	// イベントループ
		wdt_reset();
		update_sw();
		if (sw_flag) {
			sw_flag = 0;	// フラグをクリア
			switch (sw) {
			case 0:
				break;
			case 1:
				PORTB = (PORTB >> 7) | (PORTB << 1);
				break;
			case 2:
				PORTB = (PORTB << 7) | (PORTB >> 1);
				break;
			case 3:
				PORTB = ~PORTB;
				break;
			}
		}
	}
	return 0;
}
