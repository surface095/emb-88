#include<avr/io.h>
#include<avr/wdt.h>
#include <avr/interrupt.h>

ISR(TIMER2_COMPA_vect)
{
	PORTD ^= _BV(PORTD3);
}

int main()
{
	DDRB = 0xFF;
	DDRC = 0x0F;
	DDRD = 0xFE;

	PORTB = 0xFF;
	PORTC = 0x00;
	PORTD = 0x00;

	TCCR2B = 0x04;
	TCCR2A = 0x02;
	TIMSK2 = _BV(OCIE2A);

	OCR2A = 62;	// �J�E���^���g��(2kHz)
	OCR2B = 0;

	sei();

	for (;;) {
		wdt_reset();
	}
	return 0;
}
