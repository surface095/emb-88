#include<avr/io.h>
#include<avr/wdt.h>
#include<avr/interrupt.h>

ISR(TIMER1_OVF_vect)
{
	PORTC ^= 0x20;	// 0.52秒毎にオーバーフロー
}

ISR(TIMER1_COMPA_vect)
{
	OCR1A = TCNT1 + 62499;	// 今から0.5秒後に再び割り込む
	TIFR1 = _BV(OCF1A);	// コンペアマッチAフラグクリア（重要）
	PORTC ^= 0x10;
}

ISR(TIMER1_COMPB_vect)
{
	OCR1B = TCNT1 + 1249;	// 今から0.01秒後に再び割り込む
	TIFR1 = _BV(OCF1B);	// コンペアマッチBフラグクリア（重要）
	PORTB++;
}

int main()
{
	DDRB = 0xFF;
	DDRC = 0x3F;
	DDRD = 0xFE;

	PORTD = 0x00;
	PORTC = 0x30;	// 入力ピンをプルアップ
	PORTB = 0x01;

	TCNT0 = 0;	// カウンタクリア
	TIFR0 = 0x07;	// タイマフラグを全クリア
	OCR1A = 1;
	OCR1B = 1;

	TIMSK1 = _BV(OCIE1A) | _BV(OCIE1B) | _BV(TOIE1);

	// タイマ1をノーマルモード, プリスケーラ64で開始
	TCCR1A = 0;
	TCCR1B = 3;
	sei();	// システム全体の割り込み許可

	for (;;) {
		wdt_reset();
	}
	return 0;
}
