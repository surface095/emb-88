#include<avr/io.h>
#include<avr/wdt.h>

int main()
{
	DDRB = 0xFF;
	DDRC = 0x0F;
	DDRD = 0xFE;

	PORTD = 0x00;
	PORTC = 0x30;	// 入力ピンをプルアップ
	PORTB = 0xFF;

	TCNT1 = 0;	// カウンタクリア
	OCR1A = 50000;
	OCR1B = 30000;
	TIFR1 = 0x07;	// タイマフラグを全クリア

	// タイマ1をCTCモード, プリスケーラ1024で開始
	TCCR1A = 0x00;
	TCCR1B = 0x0D;

	for (;;) {
		wdt_reset();

		// フラグ，カウンタの上位ビット4ビット，の連結表示
		PORTB = (TIFR1 << 5) | (TCNT1 >> 12);

		// オーバフローを検知したらタイマを停止
		if (TIFR1 & _BV(TOV1)) {
			TCCR1B &= ~0x07;	// プリスケーラ0
		}

		if ((~PINC >> 4) & 3) {	// スイッチ読み取り
			TIFR1 = _BV(TOV1);	// オーバフローフラグクリア
			TCCR1B |= 0x05;	// プリスケーラ1024でタイマ開始
		}
	}
	return 0;
}
