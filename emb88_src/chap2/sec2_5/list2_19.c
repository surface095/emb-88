#include<avr/io.h>
#include<avr/wdt.h>
#include<avr/interrupt.h>

ISR(TIMER1_COMPA_vect)
{
	PORTC ^= 0x10;
	PORTB++;
}

ISR(TIMER1_COMPB_vect)
{
	PORTC ^= 0x20;
}

int main()
{
	DDRB = 0xFF;
	DDRC = 0x3F;
	DDRD = 0xFE;

	PORTD = 0x00;
	PORTC = 0x00;
	PORTB = 0x00;

	TCNT1 = 0;	// カウンタクリア
	OCR1A = 7812;	// 比較値(チャンネルA)
	OCR1B = 3905;	// 比較値(チャンネルB)
	TIFR1 = 0x07;	// タイマフラグを全クリア

	// タイマ1をCTCモード, プリスケーラ1024で開始
	TCCR1A = 0x00;
	TCCR1B = 0x0D;

	TIMSK1 = _BV(OCIE1A) | _BV(OCIE1B);	// COMPA/COMPB 割り込み有効
	sei();	// システム全体の割り込み許可

	for (;;) {
		wdt_reset();
	}
	return 0;
}
