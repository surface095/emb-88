#include<avr/io.h>
#include<avr/wdt.h>
#include<avr/interrupt.h>

volatile unsigned char sw;		// スイッチ変数
volatile unsigned char sw_flag;	// スイッチ変化を示すフラグ

ISR(PCINT1_vect)	// ピン変化割り込みハンドラ
{
	// チャタリング対策用
	OCR1A = TCNT1 + 500;	// 比較値A設定(今から64ms後)
	TIFR1 = _BV(OCF1A);	// コンペマッチAフラグクリア
	TIMSK1 |= _BV(OCIE1A);	// コンペアマッチA割り込み有効化
}

ISR(TIMER1_COMPA_vect)
{
	// チャタリング終了と判断
	sw = ~(PINC >> 4) & 3;	// 変数swを更新
	sw_flag = 1;
	TIMSK1 &= ~_BV(OCIE1A);	// コンペアマッチA割り込み無効化
}

int main()
{
	DDRB = 0xFF;
	DDRC = 0x0F;
	DDRD = 0xFE;

	PORTD = 0x00;
	PORTC = 0x30;	// 入力ピンをプルアップ
	PORTB = 0x18;
	PCICR = _BV(PCIE1);
	PCMSK1 = 0x30;

	TCNT0 = 0;	// カウンタクリア
	TIFR0 = 0x07;	// タイマフラグを全クリア

	// タイマ1をノーマルモード, プリスケーラ1024で開始
	TCCR1A = 0x00;
	TCCR1B = 0x05;
	sei();	// システム全体の割り込み許可

	for (;;) {
		wdt_reset();
		if (sw_flag) {
			sw_flag = 0;	// フラグをクリア
			switch (sw) {
			case 0:
				break;
			case 1:
				PORTB = (PORTB >> 7) | (PORTB << 1);
				break;
			case 2:
				PORTB = (PORTB << 7) | (PORTB >> 1);
				break;
			case 3:
				PORTB = ~PORTB;
				break;
			}
		}
	}
	return 0;
}
