#include <avr/io.h>
#include <avr/wdt.h>

int main(void)
{
	// LED および SW1,2 のポート設定
	DDRD = 0xFE;
	DDRC = 0x0F;
	DDRB = 0xFF;

	PORTD = 0x00;
	PORTC = 0x00;
	PORTB = 0x00;

	ADMUX = _BV(REFS0);	// REF電圧(AVCC)
	ADMUX |= 0x05;	// ADC入力をADC5に設定
	ADCSRB = 0;
	ADCSRA = _BV(ADEN) | 0x7;
	ADCSRA |= _BV(ADSC);	// ADIFクリアと変換開始
	for (;;) {	// 単一変換（の繰り返し）
		wdt_reset();
		if (ADCSRA & _BV(ADIF)) {	// 変換が完了したら
			PORTB = ~(ADC >> 2);	// 変換値を取り出して LED に表示
			ADCSRA |= _BV(ADSC);	// ADIFクリアと変換開始
		}
	}
	return 0;
}
