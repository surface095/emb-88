#include <avr/io.h>
#include <avr/wdt.h>

int main(void)
{
	DDRB = 0xFF;
	DDRC = 0x0F;
	DDRD = 0xFE;
	PORTB = 0x0F;
	PORTC = 0x00;
	PORTD = 0x00;

	TCCR2A = 0x83;
	TCCR2B = 0x01;
	OCR2A = 20;	// ここで明るさ調整

	for (;;) {
		wdt_reset();
	}
}
