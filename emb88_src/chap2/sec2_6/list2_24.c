#include<avr/io.h>
#include<avr/wdt.h>

int main()
{
	DDRB = 0xFF;
	DDRC = 0x0F;
	DDRD = 0xFE;

	PORTB = 0xFF;
	PORTC = 0x00;
	PORTD = 0x00;

	TCCR2B = 0x04;
	TCCR2A = 0x12;	// コンペアマッチ出力B有効化
	OCR2A = 62;	// カウンタ周波数(2kHz)
	OCR2B = 0;

	for (;;) {
		wdt_reset();
	}
	return 0;
}
