#include <avr/io.h>
#include <avr/wdt.h>
#include <avr/interrupt.h>

#define CTOP 300
ISR(TIMER2_OVF_vect)
{
	static int cnt;
	cnt++;
	if (cnt > CTOP) {
		cnt = 0;
		if (OCR2A == 100) {
			OCR2A = 0;
		}
		else {
			OCR2A++;
		}
	}
}

int main(void)
{
	DDRB = 0xFF;
	DDRC = 0x0F;
	DDRD = 0xFE;
	PORTB = 0x00;
	PORTC = 0x00;
	PORTD = 0x00;

	TCCR2A = 0x83;
	TCCR2B = 0x01;
	OCR2A = 20;	// ここで明るさ調整

	TIMSK2 = 1 << TOIE2;

	sei();
	for (;;) {
		wdt_reset();
	}
}
