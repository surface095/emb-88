#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/wdt.h>

volatile unsigned char tone = 4;	// ����
volatile unsigned char level = 200;	// ����

ISR(TIMER2_OVF_vect)
{
	static unsigned char cnt = 0;
	cnt++;
	if (cnt >= tone) {
		cnt = 0;
		OCR2B = OCR2B ? 0 : level;
	}
}

int main(void)
{
	DDRD = 0xFE;

	TCCR2A = 0x23;
	TCCR2B = 0x01;
	OCR2B = 0;

	TIMSK2 = 1 << TOIE2;

	sei();
	for (;;) {
		wdt_reset();
	}
}
