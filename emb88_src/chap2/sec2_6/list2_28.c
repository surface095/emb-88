#include <avr/io.h>
#include <avr/wdt.h>

int main(void)
{
	DDRD = 0xFA;
	DDRC = 0x0F;
	DDRB = 0xFF;

	PORTD = 0;
	PORTC = 0;
	PORTB = 0x0F;

	// �ʑ��␳PWM(8bit), COM, PRESCALE 64
	TCCR1A = 0x81;
	TCCR1B = 0x03;
	OCR1A = 40;

	for (;;) {
		wdt_reset();
	}

	return 0;
}
