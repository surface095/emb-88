#include<avr/io.h>
#include<avr/wdt.h>

#define CTOP 200000UL

int main()
{
	unsigned long cnt = 0;

	DDRB = 0xFF;
	DDRC = 0x0F;
	DDRD = 0xFE;

	PORTB = 0xFF;
	PORTC = 0x00;
	PORTD = 0x00;

	for (;;) {
		wdt_reset();
		cnt++;
		if (cnt >= CTOP) {
			cnt = 0;
			PORTB = ~PORTB;
		}
	}
	return 0;
}
