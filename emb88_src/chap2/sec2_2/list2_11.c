#include<avr/io.h>
#include<avr/wdt.h>

#define CTOP 100000UL

int main()
{
	unsigned long cnt = 0;
	unsigned char sc = 0xFE;

	DDRB = 0xFF;
	DDRC = 0x0F;
	DDRD = 0xFE;

	PORTB = 0x81;
	PORTC = 0x00;
	PORTD = 0x00;

	for (;;) {
		wdt_reset();
		cnt++;
		if (cnt >= CTOP) {
			cnt = 0;
			sc = (sc << 1) | (sc >> 7);
			PORTD = (PORTD & 0x0F) | (sc & 0xF0);	// 上位4ビット書き換え           
			PORTC = (PORTC & 0xF0) | (sc & 0x0F);	// 下位4ビット書き換え           
		}
	}
	return 0;
}
