#include<avr/io.h>
#include<avr/wdt.h>

#define CTOP 100000UL

int main()
{
	unsigned long cnt = 0;
	unsigned char scan = 0;

	DDRB = 0xFF;
	DDRC = 0x0F;
	DDRD = 0xFE;

	PORTB = 0xFF;
	PORTC = 0x00;
	PORTD = 0x00;

	for (;;) {
		wdt_reset();
		cnt++;
		if (cnt >= CTOP) {
			cnt = 0;
			scan = (scan + 1) & 7;
			PORTB = 1 << scan;
		}
	}
	return 0;
}
