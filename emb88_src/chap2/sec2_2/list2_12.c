#include<avr/io.h>
#include<avr/wdt.h>

#define CTOP 2000UL

unsigned char led[8] = {
	0b00011000,
	0b00111000,
	0b01111000,
	0b00011000,
	0b00011000,
	0b00011000,
	0b00011000,
	0b01111110
};

void update_led()
{
	static unsigned char sc = 0xFE;
	static unsigned char scan = 0;

	PORTB = 0;	// 残像対策
	sc = (sc << 1) | (sc >> 7);
	PORTD = (PORTD & 0x0F) | (sc & 0xF0);	// 上位4ビット書き換え           
	PORTC = (PORTC & 0xF0) | (sc & 0x0F);	// 下位4ビット書き換え           
	scan = (scan + 1) & 7;
	PORTB = led[scan];
}

int main()
{
	unsigned long cnt = 0;


	DDRB = 0xFF;
	DDRC = 0x0F;
	DDRD = 0xFE;

	PORTB = 0xFF;
	PORTC = 0x00;
	PORTD = 0x00;

	for (;;) {
		wdt_reset();
		cnt++;
		if (cnt >= CTOP) {
			cnt = 0;
			update_led();
		}
	}
	return 0;
}
