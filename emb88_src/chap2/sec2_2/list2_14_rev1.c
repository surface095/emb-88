#include <avr/io.h>
#include <avr/wdt.h>

unsigned char sw;				// スイッチ変数
unsigned char sw_flag;			// スイッチ変化フラグ

#define CTOP 10000UL
void update_sw()
{
	unsigned char tmp;
	static unsigned long cnt = 0;

	// スイッチの読み取りのインターバルの調整
	cnt++;
	if (cnt < CTOP) {
		return;
	}
	cnt = 0;

	tmp = (~PINC >> 4) & 3;	//現在の入力ピンを読み取る
	if (tmp != sw) {
		sw_flag = 1;	// フラグを立てる
		sw = tmp;	// スイッチ変数を更新
	}
}

int main()
{
	DDRB = 0xFF;
	DDRC = 0x0F;
	DDRD = 0xFE;

	PORTB = 0x18;
	PORTC = 0x30;	// 入力ピンをプルアップ
	PORTD = 0x00;

	for (;;) {	// イベントループ
		wdt_reset();
		update_sw();	// スイッチ変数を更新
		if (sw_flag) {	// スイッチ変化を検出したら
			sw_flag = 0;	// フラグをクリア
			switch (sw) {	// アクション
			case 0:
				break;
			case 1:
				PORTB = (PORTB >> 7) | (PORTB << 1);
				break;
			case 2:
				PORTB = (PORTB << 7) | (PORTB >> 1);
				break;
			case 3:
				PORTB = ~PORTB;
				break;
			}
		}
	}
	return 0;
}
