#include <avr/io.h>
#include <avr/wdt.h>

unsigned char sw;				// スイッチ変数

void update_sw()
{
	sw = (~PINC >> 4) & 3;	//入力ピンを読み取り，スイッチ変数を更新
}

int main()
{
	DDRB = 0xFF;
	DDRC = 0x0F;
	DDRD = 0xFE;

	PORTB = 0xFF;
	PORTC = 0x30;	// 入力ピンをプルアップ
	PORTD = 0x00;

	for (;;) {
		wdt_reset();
		update_sw();
		switch (sw) {	// スイッチ状態に応じたアクション
		case 0:
			PORTB = 0x00;
			break;
		case 1:
			PORTB = 0xF0;
			break;
		case 2:
			PORTB = 0x0F;
			break;
		case 3:
			PORTB = 0xFF;
			break;
		}
	}

	return 0;
}
