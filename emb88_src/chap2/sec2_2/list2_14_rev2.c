#include <avr/io.h>
#include <avr/wdt.h>

unsigned char sw;				// スイッチ変数
unsigned char sw_flag;			// スイッチ変化フラグ

#define CTOP 10000UL
void update_sw()
{
	static char stat = 0;
	static unsigned long cnt = 0;
	unsigned char tmp;			// 現在のポート入力   

	tmp = (~PINC >> 4) & 3;

	switch (stat) {
	case 0:
		if (sw != tmp) {
			cnt = CTOP;
			stat = 1;
		}
		break;
	case 1:
		cnt--;
		if (cnt == 0) {
			if (sw != tmp) {
				sw_flag = 1;	// フラグを立てる
				sw = tmp;	// 変数swを更新  
			}
			stat = 0;
		}
		break;
	}
	return;
}

int main()
{
	DDRB = 0xFF;
	DDRC = 0x0F;
	DDRD = 0xF8;

	PORTB = 0x18;
	PORTC = 0x30;	// 入力ピンをプルアップ
	PORTD = 0x00;

	for (;;) {	// イベントループ
		wdt_reset();
		update_sw();
		  // スイッチ変数を更新 
		  if (sw_flag) {	// スイッチ変化を検出したら
			sw_flag = 0;	// フラグをクリア
			switch (sw) {	// アクション
			case 0:
				break;
			case 1:
				PORTB = (PORTB >> 7) | (PORTB << 1);
				break;
			case 2:
				PORTB = (PORTB << 7) | (PORTB >> 1);
				break;
			case 3:
				PORTB = ~PORTB;
				break;
			}
		}
	}
	return 0;
}
