﻿
/*
	hockey.c : ホッケーゲーム（一人用）

	Copyright(C) 2011-2018 Osamu Tamura @ Recursion Co., Ltd.
	All rights reserved.
*/

#include "user.h"

#define	LOCAL_SIZE		127

/*	マレット定義	*/
#define	MALLET_MIN		24		//	中心位置の最小
#define	MALLET_MAX		104		//	中心位置の最大
#define	MALLET_STEP		16		//	移動量
#define	MALLET_WIDTH	24		//	幅


/*	ローカル変数	*/
static int8_t	x;			// パックの現在位置
static int8_t	y;
static int8_t	dx;			// パックの移動速度
static int8_t	dy;
static uint8_t	mallet;		// マレットの中心位置

/*	ローカル関数	*/
static void		UpdateLED(void);


/*
	ホッケー処理の初期化
*/
void user_init(void)
{
	mallet	= 72;
	x	= 64;
	y	= 64;
	dx	= 3;
	dy	= -3;
}


/*
	ホッケー処理（100mS 毎に呼ばれる）
*/
void user_main(void)
{
	
	//	マレット移動
	switch (sw) {
		case 1:	//	左へ移動
			if (mallet >= (MALLET_MIN + MALLET_STEP)) {
				mallet	-= MALLET_STEP;
			}
			break;

		case 2:	//	右へ移動
			if (mallet <= (MALLET_MAX - MALLET_STEP)) {
				mallet	+= MALLET_STEP;
			}
			break;
	}

	// マレット反射（下辺）
	if ((y + dy) < 0) {

		// マレット区間か判定
		if ((mallet-MALLET_WIDTH) <= x &&
		x < (mallet+MALLET_WIDTH)) {

			_sound( BEEP_HIGH, 1 );
			y	= 0;
			dy	= -dy;
			return;
		}
		// 失点
		else {
			_sound( BEEP_LOW, 1 );
			y	= LOCAL_SIZE;
		}
	}

	// 壁反射
	else {
		// 奥
		if (y <= (LOCAL_SIZE - dy)) {
			y	+= dy;
		}
		else {
			// ランダム速度で反射
			y	= LOCAL_SIZE;
			dx	= (int8_t)(_rand() >> 3) - 7;
			dy	= -(int8_t)((_rand() >> 4) + 4);
		}

		// 右
		if (x > (LOCAL_SIZE - dx)) {
			x	= LOCAL_SIZE;
			dx	= -dx;
		}
		// 左
		else if ((x + dx) < 0) {
			x	= -x;
			dx	= -dx;
		}
		else {
			x	+= dx;
		}
	}

	UpdateLED();
}


/*
ＬＥＤ表示パターンの更新
*/
static void UpdateLED( void )
{
	uint8_t		i;
	
	for (i = 0; i < LED_SZ - 1; i++) {
		led[i] = 0;
	}

	// マレット
	led[LED_SZ - 1]	= (uint8_t)(0x1c0 >> (mallet >> 4));

	// パック
	led[LED_SZ - 1 - ((uint8_t)y >> 4)] ^= 0x80 >> ((uint8_t)x >> 4);
}


