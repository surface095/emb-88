﻿/*
	reversi.c : オセロゲーム

	Copyright(C) 2015-2018 Osamu Tamura @ Recursion Co., Ltd.
	All rights reserved.
	
	make PROJECT=reversi ARG=-D_HTONE
*/

#include "user.h"

static const int8_t dy[] = { -1, -1, -1, 0, 0, 1, 1, 1 };
static const int8_t dx[] = { -1, 0, 1, -1, 1, -1, 0, 1 };
static const uint8_t	mask[] = { 0x80, 0x40, 0x20, 0x10, 0x08, 0x04, 0x02, 0x01 };

static uint8_t	cx, cy;		//	自分の駒の現在位置
static int8_t	turn, found, pass;


//	指定位置で駒を置けるか確認し、reverse=1 で置く
static int8_t check(int8_t turn, uint8_t py, uint8_t px, int8_t reverse)
{
	uint8_t	*mapT, *mapS;
	uint8_t	i, k, m, n, y, x, count;

	if ((led[py] | led2[py]) & mask[px]) {
		return 0;
	}

	if (turn == 1) {
		mapT = led;
		mapS = led2;
	}
	else {
		mapT = led2;
		mapS = led;
	}

	if (reverse) {
		mapS[py] |= mask[px];
	}

	// 8方向走査
	count = 0;
	for (i = 0; i < 8; i++) {
		// dx[i], dy[i]方向の相手のコマ数を確認
		y = py;
		x = px;
		k = 0;
		for (n = 0;; n++) {
			y += dy[i];
			x += dx[i];
			if (y > 7 || x > 7) {
				break;
			}
			if (!(mapT[y] & mask[x])) {
				k = n;
				break;
			}
		}
		// 1枚以上存在し、その上端が自分のコマなら
		if (k && (mapS[y] & mask[x])) {
			if (!reverse) {
				return 1;
			}
			// ひっくり返す
			y = py;
			x = px;
			for (;;) {
				y += dy[i];
				x += dx[i];
				m = mask[x];
				if (!(mapT[y] & m)) {
					break;
				}
				_sound(BEEP_HIGH, 1);
				_wait(5);

				mapT[y] &= ~m;
				mapS[y] |= m;
				count++;
			}
		}
	}

	if (reverse && count == 0) {
		mapS[py] &= ~mask[px];
	}

	return count;
}


//	駒を置ける位置を探す
static int8_t find0(uint8_t *py, uint8_t *px)
{
	uint8_t	y, x;

	for (y = 0; y < LED_SZ; y++) {
		for (x = 0; x < LED_SZ; x++) {
			if (check(turn, y, x, 0)) {
				*py = y;
				*px = x;
				return 1;
			}
		}
	}
	return 0;
}

//	駒を置ける位置を探す
static int8_t find1(uint8_t *py, uint8_t *px)
{
	uint8_t	x, y;
	int8_t	xx, yy, zz, z;

	z = 0;
	for (y = 0; y < LED_SZ; y++) {
		for (x = 0; x < LED_SZ; x++) {
			if (check(turn, y, x, 0)) {
				xx = (int8_t)(x << 1) - 7;
				yy = (int8_t)(y << 1) - 7;
				zz = xx * xx + yy * yy;
				if (z < zz) {
					z = zz;
					*py = y;
					*px = x;
				}
			}
		}
	}

	return z;
}


void user_init(void)
{
	uint8_t 	y;

	//	盤面を初期化
	for (y = 0; y < LED_SZ; y++) {
		led[y] = led2[y] = 0;
	}
	led[3] = led2[4] = 0x10;
	led[4] = led2[3] = 0x08;

	cx = cy = 0;
	found = find1(&cy, &cx);
	led3[cy] = mask[cx];

	//　自分から始める
	turn = 2;
	pass = 0;
}



static void take_turns(void)
{
	// 駒を置ける
	if (found) {
		_sound(BEEP_HIGH, 1);

		pass &= ~turn;
		found = 0;
	}
	// 駒を置けない
	else {
		_sound(BEEP_LOW, 2);
		_wait(3);
		_sound(BEEP_LOW, 2);

		pass |= turn;
		if (pass == 3) {
			// 両者とも置けないので終了
			return;
		}
	}

	//	相手の番
	turn ^= 3;
}


static void user_button(void)
{
	//	置く位置を移動・決定
	switch (sw) {
		case 1:	//	上移動
		led3[cy] = 0;
		cy = (cy - 1) & 7;
		led3[cy] = mask[cx];
		break;
		
		case 2:	//	右移動
		cx = (cx+1) & 7;
		led3[cy] = mask[cx];
		break;
		
		case 3:	//　決定
		if (check(turn, cy, cx, 1)) {
			//	点滅終了
			led3[cy] = 0;
			take_turns();
		}
		//	置ける位置に置くまで繰り返す
		_sound(BEEP_LOW, 2);
		break;
	}
}


void user_main(void)
{
	if (turn == 2) {
		if (sw_flag) {
			user_button();
			sw_flag = 0;
		}
		return;
	}

	//	コンピュータ側
	//	駒が置ける場所に置く
	found = find0(&cy, &cx);
	if (found) {
		check(turn, cy, cx, 1);
	}

	take_turns();

	//	自分側
	found = find1(&cy, &cx);
	//	現在位置で点滅開始
	if (found) {
		led3[cy] = mask[cx];
	}
	//　置けなければ交替
	else {
		take_turns();
	}
}

