﻿
/*
	block.c : ブロック崩し

	Copyright(C) 2011-2018 Osamu Tamura @ Recursion Co., Ltd.
					All rights reserved.
*/

#include "user.h"

#define	LOCAL_SIZE		127

/*	マレット定義	*/
#define	MALLET_MIN		24		//	中心位置の最小
#define	MALLET_MAX		104		//	中心位置の最大
#define	MALLET_STEP		16		//	移動量
#define	MALLET_WIDTH	24		//	幅

typedef struct {
	int8_t	x;
	int8_t	y;
} Point;

typedef struct {
	int8_t	cx;
	int8_t	cy;
} Size;

/*	ローカル変数	*/
static Point	current;			// パックの現在位置
static Size		delta;			 	// パックの移動速度
static uint8_t	mallet;				// マレットの中心位置
static uint8_t	start;
static uint8_t	block[LED_SZ];		// LEDブロック配列

/* ローカル関数	*/
static void		MovePuck(void);		//ボールの移動処理
static void		MoveMallet(uint8_t button);	//ラケットの移動
static void		UpdateLED(void);	//LED表示パターン関数


/*	初期化	*/
void user_init(void)
{
	uint8_t		i;

	mallet		= 72;
	current.x	= 64;
	current.y	= 96;
	delta.cx	= (int8_t)(_rand() >> 4) - 7;
	delta.cy	= -2;
	start		= 0;

	/*ブロック表示*/
	for (i = 1; i < 4; i++) {
		block[i] = 0x7e;
	}
}

/* ホッケー処理（100mS 毎に呼ばれる）*/
void user_main(void)
{
	if (start) {
		user_init();
	}

	// パック移動
	MovePuck();
	
	// ボタン操作でマレット移動
	if (sw) {
		MoveMallet(sw);
	}
	
	UpdateLED();
}

/* パック移動	*/
static void MovePuck(void)
{
	// マレット反射
	if (current.y >= (LOCAL_SIZE - delta.cy)) {

		// マレット区間か判定
		if ((mallet - MALLET_WIDTH) <= current.x &&
			current.x < (mallet + MALLET_WIDTH)) {

			_sound(BEEP_HIGH, 1);
			current.y	= LOCAL_SIZE;
			delta.cy	= -delta.cy;
			return;
		}
		// 失点
		_sound(BEEP_LOW, 1);
		current.y	= 0;
		return;
	}

	// 壁
	else if (current.y <= -delta.cy) {
		// ランダム速度で反射
		current.y	= -current.y;
		delta.cx	= (int8_t)(_rand() >> 3) - 7;
		delta.cy	= (int8_t)((_rand() >> 4) + 4);
	}
	else {
		current.y	+= delta.cy;
	}

	// 右反射
	if (current.x > (LOCAL_SIZE - delta.cx)) {
		current.x	= LOCAL_SIZE;
		delta.cx	= -delta.cx;
	}
	// 左反射
	else if (current.x <= -delta.cx) {
		current.x	= 0;
		delta.cx	= -delta.cx;
	}
	else {
		current.x	+= delta.cx;
	}

	// ブロック崩し
	if (block[current.y >> 4] & (0x80 >> (current.x >> 4))) {
		uint8_t		i;

		block[current.y >> 4] ^= 0x80 >> (current.x >> 4);
		delta.cx = -delta.cx;
		delta.cy = -delta.cy;
		_sound(BEEP_HIGH, 1);

		//	ブロックの数判定
		start	= 0;
		for (i = 0; i < LED_SZ; i++) {
			start	|= block[i];
		}
		start	= start? 0:0xff;
	}
}

/* マレット移動	*/
static void MoveMallet(uint8_t button)
{
	switch (sw) {
	case 1:	//	左へ移動
		if (mallet >= (MALLET_MIN + MALLET_STEP)) {
			mallet	-= MALLET_STEP;
		}
		break;

	case 2:	//	右へ移動
		if (mallet <= (MALLET_MAX - MALLET_STEP)) {
			mallet	+= MALLET_STEP;
		}
		break;
	}
}


/* LED表示パターンの更新	*/
static void UpdateLED(void)
{
	uint8_t		i;

	for (i = 0; i < LED_SZ; i++) {
		led[i]	= block[i];
	}
	
	// マレット
	led[LED_SZ - 1] = (uint8_t)(0x1c0 >> (mallet >> 4));

	// パック
	led[(uint8_t)current.y >> 4] ^= 0x80 >> ((uint8_t)current.x >> 4);
}

