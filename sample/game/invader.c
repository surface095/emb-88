﻿
/*
	invader.c : ゲーム

	Copyright(C) 2011-2018 Osamu Tamura @ Recursion Co., Ltd.
	All rights reserved.
*/

#include "user.h"

/*	ローカル関数	*/
static void		MoveEnemy(void);
static void		MoveBullet(void);
static void		MoveFort(void);
static void		UpdateLED(void);

/*	ローカル変数	*/
static uint8_t	enemy[LED_SZ];	/* 敵	*/
static uint8_t	bullet[LED_SZ];	/* 弾と砲台	*/

/*
	ユーザー処理の初期化
*/
void user_init(void)
{
	uint8_t		i;

	for (i = 0; i < LED_SZ; i++) {
		enemy[i]	= 0;
		bullet[i]	= 0;
	}
	bullet[LED_SZ - 2]	= 0x10;
	bullet[LED_SZ - 1]	= 0x38;
}

/*
	ユーザー処理（100mS 毎に呼ばれる）
*/
void user_main(void)
{
	MoveEnemy();
	MoveBullet();
	MoveFort();

	UpdateLED();
}

/*
	敵の移動
*/
static void MoveEnemy (void)
{
	static uint8_t	tick = 0;
	static uint8_t	dir = 0;
	uint8_t			i;

	if (++tick < 10) {
		return;
	}
	tick	= 0;

	switch (dir) {
		case 0:		/*	右へ移動	*/
			for (i = 0; i < LED_SZ; i++) {
				enemy[i]	>>= 1;
			}
			break;
		
		case 2:		/*	左へ移動	*/
			for (i = 0; i < LED_SZ; i++) {
				enemy[i]	<<= 1;
			}
			break;
		
		default:	/*	下へ移動	*/
			if (enemy[0]) {
				_sound (BEEP_LOW, 1);
			}
			for (i = (LED_SZ - 1); i; i--) {
				enemy[i]	= enemy[i - 1];
			}
			enemy[0]	= dir==3? 0xaa:0;
			break;
	}
	dir	= (dir+1) & 3;
}

/*
	弾の移動
*/
static void MoveBullet (void)
{
	uint8_t		i;

	/*	上へ移動	*/
	for (i = 0; i < (LED_SZ - 3); i++) {
		bullet[i]	= bullet[i + 1];
	}
	bullet[LED_SZ - 3]	= 0;
}

/*
	砲台の移動
*/
static void MoveFort (void)
{
	switch (sw) {
		case 1:		/*	左へ移動	*/
			if ((bullet[LED_SZ - 1] & 0x80) == 0) {
				bullet[LED_SZ - 2] <<= 1;
				bullet[LED_SZ - 1] <<= 1;
			}
			break;
			
		case 2:		/*	右へ移動	*/
			if ((bullet[LED_SZ - 1] & 0x01) == 0) {
				bullet[LED_SZ - 2] >>= 1;
				bullet[LED_SZ - 1] >>= 1;
			}
			break;
		
		case 3:		/*	弾の発射	*/
			bullet[LED_SZ - 3]	|= bullet[LED_SZ - 2];
			break;
	}
}

/*
	ＬＥＤ表示の更新
*/
static void UpdateLED (void)
{
	uint8_t		i, j;

	/*	敵に当たれば消滅	*/
	for (i = 0; i < (LED_SZ - 2); i++) {
		j	= enemy[i] & bullet[i];
		if (j) {
			enemy[i]	^= j;
			bullet[i]	^= j;
			_sound(BEEP_HIGH, 1);
		}
	}

	/*	敵と弾の合成	*/
	for (i = 0; i < LED_SZ; i++) {
		led[i]	= enemy[i] | bullet[i];
	}
}

