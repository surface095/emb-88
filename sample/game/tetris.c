﻿
/*
	tetris.c : テトリス

	Copyright(C) 2013-2018 Osamu Tamura @ Recursion Co., Ltd.
	All rights reserved.


	着地判定
		ｙ+１位置で背景とぶつかるか

	ピースの形状設定（３ｘ３ ブロックを８ｘ３に描画）
		ｙ：描画最下端（７）
		着地判定（ｙ＋１）｜着地判定（ｙ）
		終了

	ピースの移動操作
		移動方向に隙があるか
		落下パターンを左右移動

	ピースの落下（ｙ++）
		ｙ＝＝7｜着地判定（ｙ）
		落下パターンを背景にコピー
		背景で１行揃ったら１段下げる
		つぎのピースを設定

	再描画
		背景をコピー
		ｙ位置でピースを合成
*/

#include "user.h"

#define	PIECE_SZ	3

/*	ローカル関数	*/
static void		ClearField(void);
static uint8_t	CheckPiece(int8_t yy);
static void		SetPiece(void);
static void		MovePiece(void);
static void		FallPiece(void);
static void		UpdateLED(void);


/*	ローカル変数	*/
static uint8_t	field[LED_SZ];		/* 背景			*/
static uint8_t	piece[PIECE_SZ];	/* 落下ピースのパターン		*/
static int8_t	x, y;				/* 落下ピースの中央x座標と下辺ｙ座標	*/
static uint8_t	m;					/* 落下ピースのマスクパターン		*/
static uint8_t	start, next;

static const uint8_t	pattern[7][PIECE_SZ] = {
	{ 0x00, 0x07, 0x00 },
	{ 0x01, 0x07, 0x00 },
	{ 0x02, 0x07, 0x00 },
	{ 0x04, 0x07, 0x00 },
	{ 0x03, 0x03, 0x00 },
	{ 0x03, 0x06, 0x00 },
	{ 0x06, 0x03, 0x00 }
};

/*
	ユーザー処理の初期化
*/
void user_init( void )
{
	start	= 1;
}

/*
	ユーザー処理（100mS 毎に呼ばれる）
*/
void user_main(void)
{
	static uint8_t	tick = 0;

	if (start) {
		ClearField();
	}
	if (next) {
		SetPiece();
	}
	if (sw_flag) {
		MovePiece();
		sw_flag = 0;
	}
	if (++tick >= 10) {
		tick	= 0;
		FallPiece();
	}

	UpdateLED();
}

/*
	背景の初期化
*/
static void ClearField( void )
{
	uint8_t		i;

	for (i = 0; i < LED_SZ; i++) {
		field[i]	= 0;
	}

	_sound( BEEP_HIGH, 1);

	start	= 0;
	next	= 1;
}

/*
	ピースの着地判定
*/
static uint8_t CheckPiece(int8_t yy)
{
	int8_t		i;

	for (i = 0; i < PIECE_SZ; i++, yy++) {
		if (!piece[i]) {
			continue;
		}
		if (yy >= LED_SZ) {
			return 1;
		}
		if (x >= 0 && ((piece[i] << x) & field[yy])) {
			return 1;
		}
		if (x < 0 && ((piece[i] >> (-x)) & field[yy])) {
			return 1;
		}
	}
	return 0;
}

/*
	ピースの形状設定
*/
static void SetPiece( void )
{
	uint8_t		i, r;

	do {
		r	= (uint8_t)(_rand() & 7);
	} while (r == 7);

	for (i = 0; i < PIECE_SZ; i++) {
		piece[i]	= pattern[r][i];
	}

	//	ピースのマスクパターン生成
	m	= 0;
	for (i = 0; i < PIECE_SZ; i++) {
		m	|= piece[i];
	}

	x	= 3;
	y	= 0;

	if (CheckPiece(y)) {
		start	= 1;
	}

	next	= 0;
}

/*
	ピースの移動操作
*/
static void MovePiece( void )
{
	uint8_t		i, j;
	uint8_t		tmp[PIECE_SZ];

	switch (sw) {
		case 1:		/*	左へ移動	*/
		if (((m << x) & 0x80) == 0) {
			x++;
		}
		break;
		case 2:		/*	右へ移動	*/
		if (((m << x) & 1) == 0) {
			x--;
		}
		break;
		case 3:		/*	回転	*/
		for (i = 0; i < PIECE_SZ; i++) {
			tmp[i]	= piece[i];
			piece[i]	= 0;
		}
		for (i = 0; i < PIECE_SZ; i++) {
			for (j = 0; j < PIECE_SZ; j++) {
				if (tmp[i] & (1 << j) ) {
					piece[j]	|= 4 >> i;
				}
			}
		}
		//	マスクパターン生成
		m	= 0;
		for (i = 0; i < PIECE_SZ; i++) {
			m	|= piece[i];
		}
		break;
	}
}

/*
	ピースの落下
*/
static void FallPiece(void)
{
	int8_t		i, j;

	y++;
	if (CheckPiece(y + 1)) {
		//	ピースを背景に合成
		for (i = 0, j = y; i < PIECE_SZ; i++, j++) {
			if (j < LED_SZ && piece[i]) {
				if (x >= 0) {
					field[j]	|= piece[i] << x;
				}
				else {
					field[j]	|= piece[i] >> (-x);
				}
			}
		}
		//	揃った行を消去
		for (i = j = LED_SZ - 1; j >= 0; i--, j--) {
			while (field[j] == 0xff) {
				j--;
			}
			if (j < i) {
				field[i]	= field[j];
			}
		}
		for (; i >= 0; i--) {
			field[i]	= 0;
		}

		next	= 1;

		_sound( BEEP_HIGH, 1 );
	}
	else {
		_sound( BEEP_LOW, 1 );
	}
}

/*
	ＬＥＤ表示の更新
*/
static void UpdateLED(void)
{
	int8_t		i, j;

	/*	背景	*/
	for (i = 0; i < LED_SZ; i++) {
		led[i]	= field[i];
	}

	/*	ピース	*/
	for (i = 0, j = y; i < PIECE_SZ; i++, j++) {
		if (j < LED_SZ && piece[i]) {
			if (x >= 0) {
				led[j]	|= piece[i] << x;
			}
			else {
				led[j]	|= piece[i] >> (-x);
			}
		}
	}
}

