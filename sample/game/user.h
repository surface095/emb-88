
#include <stdint.h>

#define	LED_SZ		8

/*	ブザー用	*/
enum
{
	BEEP_HIGH = 46,
	BEEP_LOW = 168,

	BEEP_C4 = 238,
	BEEP_D4 = 212,
	BEEP_E4 = 189,
	BEEP_F4 = 178,
	BEEP_G4 = 158,
	BEEP_A4 = 141,
	BEEP_B4 = 126,
	BEEP_C5 = 118
};

/*	システム定義関数	*/
extern void _wait(uint8_t wait);	/*  時間待ち        */
extern uint8_t _rand(void);		/*  擬似乱数生成  */
extern void _sound(uint8_t tone, uint8_t length);	/*  ブザー           */

/*	システム定義変数	*/
extern volatile uint8_t sound_flag;		/* ブザー音の発生を示すフラグ	*/
extern volatile uint8_t sw_flag;		/* スイッチ変化を示すフラグ(クリアは手動) */
extern volatile uint8_t sw;		/*  押しボタン状態       */

extern uint8_t led[LED_SZ];		/*  マトリクスLED（明）	*/
#ifdef _HTONE
extern uint8_t led2[LED_SZ];	/*  マトリクスLED（暗）	*/
extern uint8_t led3[LED_SZ];	/*  マトリクスLED（点滅）	*/
#endif

/*	ユーザー定義関数	*/
extern void user_init(void);
extern void user_main(void);
