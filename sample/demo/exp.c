/*
	exp.c

    自然対数の底

    Copyright(c) Recursion Co., Ltd. Partial rights reserved.
*/

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <avr/wdt.h>

#define BAUD    38400

/*
    USART0 設定（文字の出力）
*/
static void usart_init(void)
{
	UCSR0A  |= 1 << U2X0;
	UCSR0B  = 0;
	UCSR0C  = 3 << UCSZ00;
	UBRR0L  = ((F_CPU >> 3) + (BAUD >> 1)) / BAUD - 1;
	UBRR0H  = 0;
	UCSR0B  = 1 << TXEN0;
}

static void _putc(char c)
{
	if (c == '\n') {
		_putc('\r');
	}

	while ((UCSR0A & (1 << UDRE0)) == 0) {
		wdt_reset();
	}
	UDR0    = c;
}

static void _puts(char *str)
{
    while (*str) {
        _putc(*str++);
	}
}


/*
    Timer0 設定（ウォッチドッグタイマのクリア）
*/
ISR(TIMER0_COMPA_vect)
{
    wdt_reset();
}


static void timer_init(void)
{
    OCR0A   = 249;          /* 2mS    */
    TCCR0A  = 2;
    TCCR0B  = 3;            /* 1/64    */
    TIMSK0  |= 1 << OCIE0A;
}


/***********************************************************
    multprec.c -- 多倍長演算

    C言語による最新アルゴリズム事典　奥村晴彦著より
***********************************************************/

#define RADIXBITS 15                    /* 基数のビット数 */
#define RADIX (1U << RADIXBITS)                   /* 基数 */
#define N  240    /* RADIX進法で小数第N位まで */
#define M  270    /* 10進で小数第 4×M 位まで */

typedef unsigned int   uint;    /* 16ビット以上 */
typedef unsigned short ushort;  /* 16ビット以上 */
typedef unsigned long  ulong;   /* 32ビット以上 */

void error(char message[])
{
    _puts(message);
    _putc('\n');
    for(;;)
    ;
}

void add(ushort a[], ushort b[], ushort c[])
{
    int i;
    uint u;

    u = 0;
    for (i = N; i >= 0; i--) {
        u += a[i] + b[i];
        c[i] = u & (RADIX - 1);  u >>= RADIXBITS;
    }
//    if (u) error("Overflow");
}

void sub(ushort a[], ushort b[], ushort c[])
{
    int i;
    uint u;

    u = 0;
    for (i = N; i >= 0; i--) {
        u = a[i] - b[i] - u;
        c[i] = u & (RADIX - 1);
        u = (u >> RADIXBITS) & 1;
    }
//    if (u) error("Overflow");
}

void muls(ushort a[], uint x, ushort b[])
{
    int i;
    ulong t;

    t = 0;
    for (i = N; i >= 0; i--) {
        t += (ulong) a[i] * x;
        b[i] = (uint) t & (RADIX - 1);  t >>= RADIXBITS;
    }
//    if (t) error("Overflow");
}

int divs(int m, ushort a[], uint x, ushort b[])
{
    int i;
    ulong t;

    t = 0;
    for (i = m; i <= N; i++) {
        t = (t << RADIXBITS) + a[i];
        b[i] = t / x;  t %= x;
    }
    if (2 * t >= x)  /* 四捨五入 */
        for (i = N; ++b[i] & RADIX; i--) b[i] &= RADIX - 1;
    return (b[m] != 0) ? m : (m + 1);  /* 0でない最左位置 */
}

void print(ushort a[])
{
    int     i, j;
    ushort  k;

    _puts("\n\n\t");
    _putc('0'+a[0]);
    _putc('.');
    j = 50;
    for (i = 0; i < M; i++) {
        a[0] = 0;  muls(a, 10000, a);
/*        printf("%04u", a[0]);    */

        k   = a[0];
        _putc('0'+k/1000);
        k   %= 1000;
        _putc('0'+k/100);

        j   -= 2;                /*    50桁ごとに改行    */
        if( j==0 ) {
            _puts("\n\t  ");
            j   = 50;
        }    

        k   %= 100;
        _putc('0'+k/10);
        k   %= 10;
        _putc('0'+k);

        j   -= 2;                /*    50桁ごとに改行    */
        if( j==0 ) {
            _puts("\n\t  ");
            j   = 50;
        }    
    }
    _puts("\n\n");
}

ushort a[N+1], t[N+1];

void e(void)  /* 自然対数の底 */
{
    int m;
    uint k;

    _putc('\n');
    for (m = 0; m <= N; m++) a[m] = t[m] = 0;  /* a := t := 0 */
    a[0] = 2;  a[1] = t[1] = RADIX / 2;        /* a := 2.5, t := 0.5 */
    k = 3;  m = 1;
    while ((m = divs(m, t, k, t)) <= N) {      /* t := t/k */
        add(a, t, a);                          /* a := a + t */
//        if (++k == RADIX) error("桁数が多すぎます");
        k++;
        _putc('#');
    }
    print(a);
}

int main(void)
{

    wdt_enable(WDTO_1S);

    usart_init();
    timer_init();
    sei();

    e();

    for(;;)
    ;

    return 0;
}

