﻿/*
	queen.c

	8 Queens problem

	Copyright(c) Recursion Co., Ltd. Partial rights reserved.
*/

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/wdt.h>

#define CLK_PER_SEC 500

static volatile unsigned int     tm_clk, tm_sec;
static unsigned char			 led[8];


ISR(TIMER0_COMPA_vect)
{
	static unsigned char sc = 0xfe;
	static unsigned char scan = 0;

	// scan matrix_LED
	PORTB	= 0;
	scan    = (scan + 1) & 7;
	sc		= (sc << 1) | (sc >> 7);
	PORTC   = sc & 0x0f;
	PORTD   = sc & 0xf0;
	PORTB   = led[scan];

	// generate clock (tm_sec)
	if (--tm_clk == 0) {
		tm_clk  = CLK_PER_SEC;
		tm_sec++;
	}
}

static void avr_init()
{
	// Ports direction for LED
	DDRB    = 0xff;
	DDRC    = 0x0f;
	DDRD    = 0xfa;

	// Timer for LED scanning
	OCR0A   = 249;	// 2mS
	TCCR0A  = 2;
	TCCR0B  = 3;	// 1/64
	TIMSK0  |= (1 << OCIE0A);

	tm_clk  = CLK_PER_SEC;
	tm_sec  = 0;

	sei();
}

static void delay(void)
{
	static unsigned int now;

	while (now == tm_sec) {
		wdt_reset();
	}
	now    = tm_sec;
}

/***********************************************************
	nqueens.c -- N王妃の問題

	C言語による最新アルゴリズム事典　奥村晴彦著より
***********************************************************/

#define N  8  /* N×N の盤面 */
int a[N], b[2 * N - 1], c[2 * N - 1], x[N];

void found(void)
{
	int		i;

	// update on second
	delay();

	for (i = 0; i < N; i++) {
		led[i]    = 0x80 >> x[i];
	}
}

void try(int i)
{
	int		j;

	for (j = 0; j < N; j++) {
		if (a[j] && b[i + j] && c[i - j + N - 1]) {
			x[i] = j;
			if (i < N - 1) {
				a[j] = b[i + j] = c[i - j + N - 1] = 0;
				try(i + 1);
				a[j] = b[i + j] = c[i - j + N - 1] = 1;
			}
			else {
				found();
			}
		}
	}
}

int main()
{
	int		i;

	avr_init();

	for (;;) {
		for (i = 0; i < N; i++)			{ a[i] = 1; }
		for (i = 0; i < 2 * N - 1; i++) { b[i] = 1; }
		for (i = 0; i < 2 * N - 1; i++) { c[i] = 1; }

		try(0);
	}

	return 0;
}

