/*
	haiku.c (Shift-JIS)

    俳句生成

    Copyright(c) Recursion Co., Ltd. All rights reserved.
*/

#include <stdlib.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <avr/wdt.h>

#define BAUD    38400

static const char *upper[]    = {
    "陽炎や",
    "畑うつや",
    "春の夕",
    "みじか夜や",
    "蚊の声す",
    "四五人に",
    "起て居て",
    "水鳥や"
};

static const char *middle[]    = {
    "名もしらぬ虫の",
    "うごかぬ雲も",
    "たえなむとする",
    "毛むしの上に",
    "忍冬の花の",
    "月落かかる",
    "もう寝たといふ",
    "てうちんひとつ"
};

static const char *lower[]    = {
    "白き飛",
    "なくなりぬ",
    "香をつぐ",
    "露の玉",
    "散るたびに",
    "おどり哉",
    "夜寒哉",
    "城を出る"
};


/*
    USART0 設定（文字の出力）
*/
static void usart_init(void)
{
	UCSR0A  |= 1 << U2X0;
	UCSR0B  = 0;
	UCSR0C  = 3 << UCSZ00;
	UBRR0L  = ((F_CPU >> 3) + (BAUD >> 1)) / BAUD - 1;
	UBRR0H  = 0;
	UCSR0B  = 1 << TXEN0;
}

static void _putc(char c)
{
	if (c == '\n') {
		_putc('\r');
	}

	while ((UCSR0A & (1 << UDRE0)) == 0) {
		wdt_reset();
	}
	UDR0    = c;
}

static void _puts(const char *str)
{
    while (*str) {
        _putc(*str++);
	}
}

//    LFSR で乱数生成
static unsigned int _rand(void)
{
	static unsigned int    lfsr = 1234;
	
	lfsr = (lfsr >> 1) ^ (-(int)(lfsr & 1) & 0xb400);
	return lfsr;
}

/*
    Timer0 設定
*/
ISR(TIMER1_COMPA_vect)
{
	unsigned int	r;
	
	r = _rand();
	
	//	成句間隔を変化
	OCR1A	= (r & 0x7fff) + 31249;
	TCNT1	= 0;

	//	俳句の生成
    _putc('\t');

    _puts(upper[r & 7]);
    _puts(middle[(r >> 3) & 7]);
    _puts(lower[(r >> 6) & 7]);

    _putc('\n');
    _putc('\n');
}

static void timer_init()
{
    TCCR1A  = 0;		// CTC
    TCCR1B  = 0x0d;		// 1/1024
    OCR1A   = (F_CPU >> 8) - 1;
    TIMSK1  |= 1 << OCIE1A;
}


int main(void)
{
    usart_init();
    timer_init();

    _putc('\n');
    _putc('\n');

    sei();
    for (;;) {
        wdt_reset();
    }

    return 0;
}

