/*
	number.c

    数字表示

    Copyright(c) Recursion Co., Ltd. Partial rights reserved.
*/

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <avr/wdt.h>

#define CLK_PER_SEC 500

static volatile unsigned int     tm_clk, tm_sec;
static unsigned char			 led[8];


//    数字のドットパターン 8x8
static PROGMEM const char number[] = "\
\x3C\x42\x42\x42\x42\x42\x3C\x00\
\x08\x18\x08\x08\x08\x08\x1C\x00\
\x3C\x42\x02\x0C\x10\x20\x7E\x00\
\x3C\x42\x02\x3C\x02\x42\x3C\x00\
\x04\x0C\x14\x24\x44\x7E\x04\x00\
\x7E\x40\x40\x7C\x02\x02\x7C\x00\
\x3C\x42\x40\x7C\x42\x42\x3C\x00\
\x7E\x02\x04\x08\x10\x10\x10\x00\
\x3C\x42\x42\x3C\x42\x42\x3C\x00\
\x3C\x42\x42\x3E\x02\x42\x3C\x00";


ISR(TIMER0_COMPA_vect)
{
	static unsigned char sc = 0xfe;
	static unsigned char scan = 0;

	// scan matrix_LED
	PORTB	= 0;
	scan    = (scan + 1) & 7;
	sc		= (sc << 1) | (sc >> 7);
	PORTC   = sc & 0x0f;
	PORTD   = sc & 0xf0;
	PORTB   = led[scan];

	// generate clock (tm_sec)
	if (--tm_clk == 0) {
		tm_clk  = CLK_PER_SEC;
		tm_sec++;
	}
}

static void avr_init()
{
	// Ports direction for LED
	DDRB    = 0xff;
	DDRC    = 0x0f;
	DDRD    = 0xfa;

	// Timer for LED scanning
	OCR0A   = 249;	// 2mS
	TCCR0A  = 2;
	TCCR0B  = 3;	// 1/64
	TIMSK0  |= 1 << OCIE0A;

	tm_clk  = CLK_PER_SEC;
	tm_sec  = 0;

	sei();
}


int main(void)
{
	unsigned int	now = 0;
	unsigned char	n = 0;
	unsigned char	i, k;

	avr_init();

	for(;;) {
		wdt_reset();
		if (now == tm_sec) {
			continue;
		}
		now    = tm_sec;
		
		for (i = 0, k = n << 3; i < 8; i++, k++) {
			led[i]    = pgm_read_byte(number + k);
		}
		
		if (++n > 9) {
			n    = 0;
		}	
	}

    return 0;
}

