﻿
/*
	dice.c

    さいころ

    Copyright(c) Recursion Co., Ltd. All rights reserved.
*/

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <avr/wdt.h>

//    さいころのドットパターン 8x8
static PROGMEM const char pattern[] = {
    0, 0x18, 0,            // 1
    3, 0, 0xc0,            // 2
    3, 0x18, 0xc0,         // 3
    0xc3, 0, 0xc3,         // 4
    0xc3, 0x18, 0xc3,      // 5
    0xc3, 0xc3, 0xc3       // 6
};

#define CLK_PER_SEC		64		//500

static volatile unsigned int     tm_clk, tm_sec;
static unsigned char			 led[8];


ISR(TIMER0_COMPA_vect)
{
	static unsigned char sc = 0xfe;
	static unsigned char scan = 0;

	// scan matrix_LED
	PORTB	= 0;
	scan    = (scan + 1) & 7;
	sc		= (sc << 1) | (sc >> 7);
	PORTC   = sc & 0x0f;
	PORTD   = sc & 0xf0;
	PORTB   = led[scan];

	// generate clock (tm_sec)
	if (--tm_clk == 0) {
		tm_clk  = CLK_PER_SEC;
		tm_sec++;
	}
}

static void avr_init()
{
	// Ports direction for LED
	DDRB    = 0xff;
	DDRC    = 0x0f;
	DDRD    = 0xfa;

	// Timer for LED scanning
	OCR0A   = 249;	// 2mS
	TCCR0A  = 2;
	TCCR0B  = 3;	// 1/64
	TIMSK0  |= (1 << OCIE0A);

	tm_clk  = CLK_PER_SEC;
	tm_sec  = 0;

	sei();
}

// sw1,2 チェック
static unsigned char is_button(void)
{
	return ~PINC & 0x30;
}

static void delay(void)
{
	static unsigned int now;

	while (now == tm_sec) {
		wdt_reset();
	}
	now    = tm_sec;
}

//    LFSR で乱数生成
static unsigned int _rand(void)
{
    static unsigned int    lfsr = 1;
	
	lfsr = (lfsr >> 1) ^ (-(int)(lfsr & 1) & 0xb400);
	return lfsr;
}

/***********************************************************
***********************************************************/

static void set_dice(unsigned char n)
{
    n    *= 3;
	led[0]    = led[1]    = pgm_read_byte(pattern+n++);
	led[3]    = led[4]    = pgm_read_byte(pattern+n++);
	led[6]    = led[7]    = pgm_read_byte(pattern+n++);
}

static unsigned char roll_dice(void)
{
	static unsigned char	offset = 0;
	unsigned char    n;

	n = _rand() & 7;
	if (n > 5) {
		n = offset + (n & 1);
		offset += 2;
		if (offset >= 5) {
			offset = 0;
		}
	}
	return n;
}

int main(void)
{
	avr_init();

    for (;;) {

        if (is_button()) {
			// 賽を振る
			set_dice(roll_dice());
        }

		delay();
    }

    return 0;
}

