﻿/*
	melody.c

	演奏

	Copyright(c) Recursion Co., Ltd. All rights reserved.
*/

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <avr/wdt.h>

PROGMEM const char melody1[] = "\
C3H3H<L2J2H2L<L3J3H<E6C<C3H3H<L2J2H2L<J3L3ONL3O3O<L2J2H2L<L3J3H<E6C<C3H3H<L2J2H2L<J6HB p";

PROGMEM const char melody2[] = "\
C2E2G2J2H2H2L2J2J2O2N2O2J2G2C2E2G2H2J2L2J2H2G2E2G2C2B2C2E2>2B2E2H2G2E2G4\
C2E2G2J2H2H2L2J2J2O2N2O2J2G2C2E2G2@2J2H2G2E2C2>2C2B2C2G2J2O2J2G2C6 p";

static const char *melody[] = { melody1, melody2 };

static const unsigned short pitch[] = { 480, 600 };


PROGMEM const unsigned char tone[] = {
	142,134,127,119,113,106,100,95,89,84,80,75,
	71,67,63,60,56,53,50,47,45,42,40,38,
	36,34,32,30,28,27,25,24,22,21,20,19,
	18,17,16,15,14,13,13,12,11,11,10,9,
	9
};

static const unsigned char	wave[8] = {
	192, 255, 223, 191, 127, 31, 15, 7
};

volatile unsigned char note;	// 音程
volatile unsigned char level;	// 音量

static char	*p;
static unsigned short	q;


ISR(TIMER2_OVF_vect)
{
	static unsigned char cnt = 1;

	if (--cnt == 0) {
		cnt = note;
		OCR2B = OCR2B ? 0 : level;
	}
}

ISR(TIMER1_COMPA_vect)
{
	static unsigned char	cnt = 0;
	static unsigned char	sc = 0xfe;
	static unsigned char	gb = 0x10;
	unsigned char	n;
	unsigned short	d;

	if (++cnt < 8) {
		if (level) {
			level	= wave[cnt];
			sc		= (sc << 1) | (sc >> 7);
			gb		^= 0x30;
			PORTC   = (sc & 0x0f) | gb;
			PORTD   = sc & 0xf0;
		}
		return;
	}
	cnt = 0;
	
	n    = pgm_read_byte(p++);
	d    = (unsigned short)pgm_read_byte(p++);

	OCR1A = (d - '0') * q - 1;
	if (n) {
		if (n == ' ') {
			level = 0;
		}
		else {
			PORTB	= n;
			n -= 45;
			note	= pgm_read_byte(&tone[n]);
			level = wave[cnt];
			sc		= 0xfe;
			gb		= 0x10;
			PORTC   = 0x1e;
			PORTD   = 0xf0;
		}
	}
	else {
		TCCR2B = 0;
	}
}

int main(void)
{
	unsigned char	i = 0;

	DDRB	= 0xff;
	DDRC	= 0x3f;
	DDRD    = 0xfe;             // PORTD3->OC2B
	PORTB	= 0xff;
	
	// TIMER1 音の長さ
	OCR1A   = 1000;
	TCCR1A  = 0;                // CTC
	TCCR1B  = 0x0c;             // 1/256
	TIMSK1  = 1 << OCIE1A;

	// TIMER2 音程
	OCR2B	= 0;
	TCCR2A	= 0x23;				// PWM
	TCCR2B	= 0;
	TIMSK2	= 1 << TOIE2;

	sei();
	for (;;) {
		wdt_reset();
		
		//	曲選択
		if (TCCR2B == 0) {
			p = (char *)melody[i];
			q = pitch[i];
			if (++i >= 2) {
				i = 0;
			}
			TCCR2B	= 0x01;
		}
	}

	return 0;
}

