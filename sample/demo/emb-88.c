﻿/*
 * emb-88.c		検査プログラム
 *
 * Created: 2016/07/11 17:04:25
 * Author : Osamu Tamura
 */ 

#include <stdlib.h>
#include <string.h>
#include <avr/io.h>
#include <avr/wdt.h>
#include <avr/interrupt.h>

static const unsigned char tone[8] = { 238, 212, 189, 178, 158, 141, 126, 118 };

static volatile unsigned char	n;
static volatile unsigned char	adc_ready = 0;

static volatile unsigned char	led[8];

static volatile char	*tptr = NULL;
static char	buff[10];


// 行出力
static void _puts(char *s)
{
	tptr = s;
	UCSR0B |= _BV(UDRIE0);
}

ISR(USART_UDRE_vect)
{
	UDR0 = *tptr++;

	if (*tptr == 0) {
		UCSR0B &= ~_BV(UDRIE0);
	}
}

ISR(USART_RX_vect)
{
	unsigned char	c;
	
	c = UDR0;

	if (*tptr == 0) {
		UDR0 = c;
	}
}

ISR(TIMER0_COMPA_vect)
{
	static unsigned char scan;
	unsigned char sc;

	// ダイナミックスキャンで表示更新
	PORTB = 0;
	scan = (scan + 1) & 7;
	sc = ~(1 << scan);
	PORTC = (sc & 0x0f) | 0x30;	// プルアップは維持
	PORTD = sc & 0xf0;
	PORTB = led[scan];
}

ISR(TIMER1_COMPA_vect)
{
	ADCSRA |= _BV(ADSC)|_BV(ADIF);	// ADCスタート
	adc_ready = 0;
}

/* ADC完了割り込み */
ISR(ADC_vect)
{
	unsigned short val;

	val = ADC;

	// 取得値の範囲調整
	if (PINC & 0x10) {
		n = (unsigned char)(val < 768? 0:(val - 768));
	}
	else {
		n = 0xff;
	}
	adc_ready = 1;
}

int main(void)
{
	unsigned char i, k;

	/* LED および SW1,2 のポート設定    */
	DDRB = 0xff;
	DDRC = 0x3f;
	DDRD = 0xfe;	// PD2 を OUT

	PORTB = 0x55;
	PORTC = 0x15;
	PORTD = 0x50;	// PD2 を L

	/* タイマ0 (マトリクスLED 走査 2ms) */
	OCR0A = 249;
	TCCR0A = 2;
	TCCR0B = 3;
	TIMSK0 |= _BV(OCIE0A);

	// タイマ1（１秒計測）
	TCCR1A = 0x00;
	TCCR1B = 0x0c;
	OCR1A = 15624;
	TIMSK1 |= _BV(OCIE1A);

	// タイマ2（ブザー用）
	TCCR2A = 0x12;
	TCCR2B = 0x04;	// ブザー開始

	// シリアルポート設定
	UCSR0B = 0;
	UCSR0C = 0x06;
	UBRR0 = 12;
	UCSR0B = _BV(RXCIE0) | _BV(RXEN0) | _BV(TXEN0);


	// LED の点滅
	for (i = 0; i < 8;) {
		wdt_reset();

		if (TIFR1 & _BV(OCF1A)) {
			TIFR1 = _BV(OCF1A);

			PINB = 0xff;
			PINC = 0x3f;
			PIND = 0xf0;
			i++;
			if (i & 1) {
				PORTB = ~PORTB;
			}
		}
	}

	DDRC = 0x0f;
	PORTC = 0x10;

	// ADC を設定し起動
	DIDR0 = _BV(ADC5D);
	ADMUX = 0x45;
	ADCSRA = _BV(ADIE) | _BV(ADEN) | 0b110;

	// ADC 計測間隔の設定
	OCR1A = 7812;


	sei();
	_puts( "\r\n\r\n\t*** EMB-88 Test Program ***\r\n");

	for (;;) {
		wdt_reset();

		if (adc_ready) {
			k = n >> 5;

			// マトリクス LED 表示の左シフト
			for (i = 0; i < 8; i++) {
				led[i] <<= 1;
			}

			if (n) {
				for (i = 0; i <= k; i++) {
					led[7 - i]++;
				}

				// 照度に応じた音階発生
				OCR2A = tone[k];

				// 照度値のシリアル送信
				itoa((unsigned short)n, buff, 10);
				strcat(buff, "\r\n");
				_puts(buff);
			}
			else {
				OCR2A = 0;
			}

			adc_ready = 0;
		}
	}
	
	return 0;
}
