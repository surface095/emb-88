/*
	kitt.c

    Knight 2000

    Copyright(c) Recursion Co., Ltd. Partial rights reserved.
*/

#include <avr/io.h>
#include <avr/wdt.h>
#include <avr/interrupt.h>

#define CTOP	4200
#define NTOP	12

static const unsigned char pattern[NTOP] = {
	0, 0x80, 0xc0, 0xe0, 0x70, 0x38, 0x1c, 0x0e, 0x07, 0x03, 0x01, 0
};

static const unsigned char light[NTOP] = {
	0x01, 0x02, 0x04, 0x10, 0x20, 0x80, 0x80, 0x20, 0x10, 0x04, 0x02, 0x01
};

static const unsigned char volume[NTOP] = {
	0x01, 0x03, 0x07, 0x1f, 0x3f, 0xff, 0xff, 0x3f, 0x1f, 0x07, 0x03, 0x01
};

volatile unsigned char tone = 224;	// 音程
volatile unsigned char level;		// 音量

ISR(TIMER0_OVF_vect)
{
	static unsigned char	dir = 1;
	static unsigned char	n = 0;
	static int cnt = 0;
	
	cnt++;
	if (cnt > CTOP) {
		cnt = 0;
		
		if (dir) {
			if (n < NTOP-1) {
				n++;
			}
			else {
				dir = 0;
				PINC	= 0x30;
			}
		}
		else {
			if (n) {
				n--;
			}
			else {
				dir = 1;
				PINC	= 0x30;
			}
		}
		PORTB	= pattern[n];
		OCR0A	= light[n];
		OCR0B	= light[n];
//		tone	= light[n];
		level	= volume[n];
	}
}

ISR(TIMER2_OVF_vect)
{
	static unsigned char cnt = 0;

	cnt++;
	if (cnt >= tone) {
		cnt = 0;
		OCR2B = OCR2B ? 0 : level;
	}
}

int main(void)
{
	DDRB	= 0xff;
	DDRC	= 0x3f;
	DDRD	= 0xfe;
	PORTC	= 0x1f;
	PORTD	= 0x90;

	// タイマ0（LED 調光用）
	TCCR0A	= 0xf3;
	TCCR0B	= 0x01;
	TIMSK0	= _BV(TOIE0);

	// タイマ2（ブザー用）
	TCCR2A = 0x23;
	TCCR2B = 0x01;
	TIMSK2	= _BV(TOIE2);

	sei();
	for (;;) {
		wdt_reset();
	}
}
