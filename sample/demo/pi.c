﻿/*
	pi.c

    円周率計算（７２０桁）

    Copyright(c) Recursion Co., Ltd. Partial rights reserved.
*/

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <avr/wdt.h>

#define BAUD    38400

/*
    USART0 設定（文字の出力）
*/
static void usart_init(void)
{
	UCSR0A  |= 1 << U2X0;
	UCSR0B  = 0;
	UCSR0C  = 3 << UCSZ00;
	UBRR0L  = ((F_CPU >> 3) + (BAUD >> 1)) / BAUD - 1;
	UBRR0H  = 0;
	UCSR0B  = 1 << TXEN0;
}

static void _putc(char c)
{
	if (c == '\n') {
		_putc('\r');
	}

	while ((UCSR0A & (1 << UDRE0)) == 0) {
		wdt_reset();
	}
	UDR0    = c;
}

static void _puts(char *str)
{
    while (*str) {
        _putc(*str++);
	}
}

static void _puts_P(PGM_P str)
{
	char    c;

	while ((c = pgm_read_byte(str++)) != 0) {
		_putc(c);
	}
}

/*
    Timer0 設定（ウォッチドッグタイマのクリア）
*/
ISR(TIMER0_COMPA_vect)
{
    wdt_reset();
}


static void timer_init(void)
{
    OCR0A   = 249;          /* 2mS    */
    TCCR0A  = 2;
    TCCR0B  = 3;            /* 1/64    */
    TIMSK0  |= 1 << OCIE0A;
}


/***********************************************************
    multprec.c -- 多倍長演算

    C言語による最新アルゴリズム事典　奥村晴彦著より
***********************************************************/

#define RADIXBITS 15                    /* 基数のビット数 */
#define RADIX (1U << RADIXBITS)                   /* 基数 */
#define N  160    /* RADIX進法で小数第N位まで */
#define M  180    /* 10進で小数第 4×M 位まで */

typedef unsigned int   uint;    /* 16ビット以上 */
typedef unsigned short ushort;  /* 16ビット以上 */
typedef unsigned long  ulong;   /* 32ビット以上 */

void error(const char message[])
{
    _puts_P(message);
    _putc('\n');

    for(;;)
    ;
}

void add(ushort a[], ushort b[], ushort c[])
{
    int i;
    uint u;

    u = 0;
    for (i = N; i >= 0; i--) {
        u += a[i] + b[i];
        c[i] = u & (RADIX - 1);  u >>= RADIXBITS;
    }
    if (u) error(PSTR("Overflow"));
}

void sub(ushort a[], ushort b[], ushort c[])
{
    int i;
    uint u;

    u = 0;
    for (i = N; i >= 0; i--) {
        u = a[i] - b[i] - u;
        c[i] = u & (RADIX - 1);
        u = (u >> RADIXBITS) & 1;
    }
    if (u) error(PSTR("Overflow"));
}

void muls(ushort a[], uint x, ushort b[])
{
    int i;
    ulong t;

    t = 0;
    for (i = N; i >= 0; i--) {
        t += (ulong) a[i] * x;
        b[i] = (uint) t & (RADIX - 1);  t >>= RADIXBITS;
    }
    if (t) error(PSTR("Overflow"));
}

int divs(int m, ushort a[], uint x, ushort b[])
{
    int i;
    ulong t;

    t = 0;
    for (i = m; i <= N; i++) {
        t = (t << RADIXBITS) + a[i];
        b[i] = t / x;  t %= x;
    }
    if (2 * t >= x)  /* 四捨五入 */
        for (i = N; ++b[i] & RADIX; i--) b[i] &= RADIX - 1;
    return (b[m] != 0) ? m : (m + 1);  /* 0でない最左位置 */
}

void print(ushort a[])
{
    int     i, j;
    ushort  k, d;

    _puts_P(PSTR("\n\n\t"));
    _putc('0'+a[0]);
    _putc('.');

    j = 50;
    for (i = 0; i < M; i++) {
        a[0] = 0;  muls(a, 10000, a);
/*        printf("%04u", a[0]);    */

        k   = a[0];
        d    = 1000;

        for (d = 1000; d; d /= 10) {
            _putc('0'+k/d);
            k   %= d;

            /*    50桁ごとに改行    */
            if (--j == 0) {
                _puts_P(PSTR("\n\t  "));
                j   = 50;
            }
        }    
    }
    _putc('\n');
    _putc('\n');
}

ushort a[N+1], t[N+1], u[N+1];  /* e() だけなら u[] は不要. */


void pi(void)  /* 円周率 (Machinの公式) */
{
    int i, m;
    uint k;

    _putc('\n');
    t[0] = 16;  for (i = 1; i <= N; i++) { t[i] = 0; } /* t := 16 */
    divs(0, t, 5, t);                                /* t := t/5 */
    for (i = 0; i <= N; i++) { a[i] = t[i]; }        /* a := t */
    i = m = 0;  k = 1;
    for (;;) {
        if ((m = divs(m, t, 25, t)) > N) { break; }  /* t := t/25 */
//        if ((k += 2) >= RADIX) error(PSTR("桁数が多すぎます"));
        k    += 2;
        while (i < m) { u[i++] = 0; }
        if (divs(m, t, k, u) > N) { break; }         /* u := t/k */
        if (k & 2) { sub(a, u, a); } else { add(a, u, a); } /* a := a -+ u */

        _putc('#');
    }

    _putc('\n');
    t[0] = 4;  for (i = 1; i <= N; i++) { t[i] = 0; }   /* t := 4 */
    divs(0, t, 239, t);                              /* t := t/239 */
    sub(a, t, a);                                    /* a :=a - t */
    i = m = 0;  k = 1;
    for (;;) {
        if ((m = divs(m, t, 239, t)) > N) { break; } /* t := t/239 */
        if ((m = divs(m, t, 239, t)) > N) { break; } /* t := t/239 */
//        if ((k += 2) >= RADIX) error(PSTR("桁数が多すぎます"));
        k    += 2;
        while (i < m) { u[i++] = 0; }
        if (divs(m, t, k, u) > N) { break; }         /* u := t/k */
        if (k & 2) { add(a, u, a); } else { sub(a, u, a); } /* a := a +- u */

        _putc('$');
    }

    print(a);
}

int main(void)
{
    wdt_enable(WDTO_1S);

    usart_init();
    timer_init();
    sei();

    pi();

    for(;;) {
	}

    return 0;
}

