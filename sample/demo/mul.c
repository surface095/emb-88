﻿
/*
	mul.c

	乗算

	Copyright(c) Recursion Co., Ltd. All rights reserved.
*/

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <avr/wdt.h>

#include <stdio.h>

#define BAUD    38400

static int uart_putchar( char c, FILE *stream );

static FILE mystdout = FDEV_SETUP_STREAM(uart_putchar, NULL,
_FDEV_SETUP_WRITE);


static int uart_putchar(char c, FILE *stream)
{
	if (c == '\n') {
		uart_putchar('\r', stream);
	}

	while ((UCSR0A & (1 << UDRE0)) == 0) {
		wdt_reset();
	}
	UDR0    = c;

	return 0;
}

/*
USART0 設定（文字の出力）
*/
static void uart_init(void)
{
	UCSR0A  |= 1 << U2X0;
	UCSR0B  = 0;
	UCSR0C  = 3 << UCSZ00;
	UBRR0L  = ((F_CPU >> 3) + (BAUD >> 1)) / BAUD - 1;
	UBRR0H  = 0;
	UCSR0B  = 1 << TXEN0;

	stdout	= &mystdout;
}

int main(void)
{
	int        x, y;

	uart_init();
	sei();

	putchar('\n');
	puts_P( PSTR("\n    Multiply Table 1\n") );

	for (y = 1; y < 10; y++) {
		for (x = 1; x < 10; x++) {
			printf( "%3d", x*y );
			if (x == 1) {
				printf( " |" );
			}
		}
		putchar('\n');
		if (y == 1) {
			putchar(' ');
			for (x = 0; x < 28; x++) {
				putchar(x == 3? '+':'-');
			}
			putchar('\n');
		}
	}

	puts_P( PSTR("\n    Multiply Table 2\n") );

	y    = 1;
	while (y <= 9) {
		x    = 1;
		while (x <= 9) {
			printf( "%3d", x * y );
			if (x == 1) {
				printf( " |" );
			}
			x++;
		}
		putchar('\n');
		if (y == 1) {
			putchar(' ');
			for (x = 0; x < 28; x++) {
				putchar(x == 3? '+':'-');
			}
			putchar('\n');
		}
		y++;
	}
	putchar('\n');

	for (;;) {
		wdt_reset();
	}

	return 0;
}


