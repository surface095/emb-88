/*
	hanoi.c

	Tower of Hanoi

	Copyright(c) Recursion Co., Ltd. All rights reserved.
*/

#include <avr/io.h>
#include <avr/wdt.h>
#include <avr/interrupt.h>

#define	N	4

unsigned char	p[4];
unsigned char	mat[4][N];


#define CLK_PER_SEC 500

static volatile unsigned int     tm_clk, tm_sec;
static unsigned char			 led[8];


ISR(TIMER0_COMPA_vect)
{
	static unsigned char sc = 0xfe;
	static unsigned char scan = 0;

	// scan matrix_LED
	PORTB	= 0;
	scan    = (scan + 1) & 7;
	sc		= (sc << 1) | (sc >> 7);
	PORTC   = sc & 0x0f;
	PORTD   = sc & 0xf0;
	PORTB   = led[scan];

	// generate clock (tm_sec)
	if (--tm_clk == 0) {
		tm_clk  = CLK_PER_SEC;
		tm_sec++;
	}
}

static void avr_init()
{
	// Ports direction for LED
	DDRB    = 0xff;
	DDRC    = 0x0f;
	DDRD    = 0xfa;

	// Timer for LED scanning
	OCR0A   = 249;	// 2mS
	TCCR0A  = 2;
	TCCR0B  = 3;	// 1/64
	TIMSK0  |= (1 << OCIE0A);

	tm_clk  = CLK_PER_SEC;
	tm_sec  = 0;

	sei();
}

static void delay(void)
{
	static unsigned int now;

	while (now == tm_sec) {
		wdt_reset();
	}
	now    = tm_sec;
}

/***********************************************************
***********************************************************/

static void merge(void)
{
	unsigned char	i, j, k, x, y;
	unsigned char	*z;

	for (y = 0; y < 8; y++) {
		led[y] = 0;
	}

	z = mat[0];
	for (y = 0, i = 0x80; y < p[0]; y++, i >>= 1) {
		k = z[y];
		for (x = 0, j = 0x80; x < 7; x++, j >>= 1) {
			if (k & j) {
				led[x] |= i;
			}
		}
	}
	z = mat[1];
	for (y = 0; y < p[1]; y++) {
		led[7 - y] |= z[y];
	}
	z = mat[2];
	for (y = 0, i = 1; y < p[2]; y++, i <<= 1) {
		k = z[y];
		for (x = 1, j = 2; x < 8; x++, j <<= 1) {
			if (k & j) {
				led[x] |= i;
			}
		}
	}
	z = mat[3];
	for (y = 0; y < p[3]; y++) {
		led[y] |= z[y] >> 1;
	}

	delay();
}

static void hanoi(unsigned char n, unsigned char from, unsigned char work, unsigned char dest)
{
	if (n > 1) {
		hanoi(n - 1, from, dest, work);
	}

	mat[dest][p[dest]++] = mat[from][--p[from]];
	merge();

	if (n > 1) {
		hanoi(n - 1, work, from, dest);
	}
}

int main(void)
{
	unsigned char	i, j, n;

	avr_init();

	for (n = 2;;) {

		for (i = 0; i < 4; i++) {
			mat[0][i] = 0;
		}
		for (i = n, j = 0x10; i;) {
			mat[0][--i] = j;
			j |= (j << 1) | (j >> 1);
		}
		p[0] = n;

		merge();

		hanoi(p[0], 0, 1, 2);
		hanoi(p[2], 2, 3, 0);
		
		if (++n > 4) {
			n = 2;
		}
	}

	return 0;
}
