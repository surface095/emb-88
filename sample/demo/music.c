﻿/*
	music.c

	演奏

	Copyright(c) Recursion Co., Ltd. All rights reserved.
*/

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <avr/wdt.h>

//	何の曲でしょう？
static char *music = "fhjfmmmjhhmmhhfcjjjfeee   cceefhaaffhjkkkjhfhhh   fhjfmmmjhhmmhhfcccefaaa   cceefhaaffhjkkkjhffff   jkm m m m momkj j j j jkjhf f fec e efh hjhjh    jkm m m m momkj j j jkjhfec cefha f hjhhh hffff   ";

static char *tone = "\x8d\x85\x7e\x76\x70\x69\x63\x5e\x59\x54\x4f\x4a\x46\x42\x3e\x3b\x37\x34\x31\x2e";

static char	*p;

ISR( TIMER1_COMPA_vect )
{
	char	n;

	n	= *p++;
	if (n) {
		if (n == ' ') {
			TCCR2B	= 0;			// off
			PORTB	= 0;
		}
		else {
			OCR2A	= tone[n-'a'];
			if (TCCR2B == 0) {
				TCCR2B	= 0x44;		// on
			}
			PORTB	= n;
		}
		PORTC	^= 0x30;
	}
	else {
		TCCR1B	= 0;				// stop
	}
}

int main(void)
{
	DDRB	= 0xff;
	DDRC	= 0x3f;
	DDRD    = 0xfe;         // PORTD3->OC2B
	PORTC   = 0x17;
	PORTD   = 0xe0;

	OCR1A   = 7812;			// 250mS
	TCCR1A  = 0;            // CTC	1/256
	TCCR1B  = 0;
	TIMSK1  = 1 << OCIE1A;

	TCCR2A	= 0x12;
	TCCR2B	= 0;

    sei();
    for (;;) {
    	wdt_reset();

		if (TCCR1B == 0) {
			p = music;
			TCCR1B	= 0x0c;	// restart
		}
	}
	
    return 0;
}

