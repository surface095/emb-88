﻿
/*
	wave.c

    ゆらゆら

    Copyright(c) Recursion Co., Ltd. All rights reserved.
*/

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/wdt.h>

static const unsigned char gy[7][8] = {
{   1,   1,   1,   1,   3,  15,  63, 255 },
{   1,   3,   7,  15,  31,  63, 127, 255 },
{  22,  31,  44,  63,  90, 127, 180, 255 },
{  90,  90,  90,  90,  90,  90,  90,  90 },
{ 255, 180, 127,  90,  63,  44,  31,  22 },
{ 255, 127,  63,  31,  15,   7,   3,   1 },
{ 255,  63,  15,   3,   1,   1,   1,   1 }
};

static volatile signed char		n = 0;


ISR(TIMER0_COMPA_vect)
{
	static unsigned char sc = 0xfe;
	static unsigned char scan = 0;

	// scan matrix_LED
	scan    = (scan + 1) & 7;
	sc		= (sc << 1) | (sc >> 7);
	PORTC   = sc & 0x0f;
	PORTD   = sc & 0xf0;
	OCR0A   = gy[n][scan];
}

ISR(TIMER1_COMPA_vect)
{
	static signed char	 d = 1;
	
	n += d;
	if (n == 0 || 6 == n) {
		d = -d;
		PORTC	|= 0x10;
	}

	// Adjust display timing and restart ADC
	OCR1A	= (1023 - ADC) << 4;
	TCNT1	= 0;
	ADCSRA |= _BV(ADSC)|_BV(ADIF);
}

static void avr_init()
{
	// Ports direction for LED
	DDRB    = 0xff;
	DDRC    = 0x1f;
	DDRD    = 0xfa;
	PORTB	= 0xff;

	// Timer for LED scanning
	OCR0A   = 249;	// 2mS
	TCCR0A  = 2;
	TCCR0B  = 3;	// 1/64
	TIMSK0  |= (1 << OCIE0A);

	// Timer for ADC interval
	OCR1A   = (F_CPU >> 11) - 1;
    TCCR1A  = 0;		// CTC
    TCCR1B  = 0x0c;		// 1/256
    TIMSK1  |= 1 << OCIE1A;

	// Start ADC
	DIDR0	= _BV(ADC5D);
	ADMUX	= 0x45;
	ADCSRA	= _BV(ADEN) | 0b110;
}


int main(void)
{
	avr_init();

	sei();
    for (;;) {
		wdt_reset();
    }
    return 0;
}

