﻿/*
	haiku.c (UTF-8)

    俳句生成

    Copyright(c) Recursion Co., Ltd. All rights reserved.
*/

#include <stdlib.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <avr/wdt.h>

#define BAUD    38400

static const wchar_t *upper[]    = {
    L"陽炎や",
    L"畑うつや",
    L"春の夕",
    L"みじか夜や",
    L"蚊の声す",
    L"四五人に",
    L"起て居て",
    L"水鳥や"
};

static const wchar_t *middle[]    = {
    L"名もしらぬ虫の",
    L"うごかぬ雲も",
    L"たえなむとする",
    L"毛むしの上に",
    L"忍冬の花の",
    L"月落かかる",
    L"もう寝たといふ",
    L"てうちんひとつ"
};

static const wchar_t *lower[]    = {
    L"白き飛",
    L"なくなりぬ",
    L"香をつぐ",
    L"露の玉",
    L"散るたびに",
    L"おどり哉",
    L"夜寒哉",
    L"城を出る"
};


/*
    USART0 設定（文字の出力）
*/
static void usart_init(void)
{
	UCSR0A  |= 1 << U2X0;
	UCSR0B  = 0;
	UCSR0C  = 3 << UCSZ00;
	UBRR0L  = ((F_CPU >> 3) + (BAUD >> 1)) / BAUD - 1;
	UBRR0H  = 0;
	UCSR0B  = 1 << TXEN0;
}


static void _putc(uint8_t c)
{
	while ((UCSR0A & (1 << UDRE0)) == 0) {
		wdt_reset();
	}
	UDR0    = c;
}

//	Convert UTF-16 -> UTF-8
static void _putwc(wchar_t c)
{
	if ((uint16_t)c < 0x80) {
		if (c == '\n') {
			_putc('\r');
		}
		_putc((uint8_t)c);
		return;
	}
	if ((uint16_t)c < 0x800) {
		_putc(0xc0 | (uint8_t)((uint16_t)c >> 6));
	}
	else {
		_putc(0xe0 | (uint8_t)((uint16_t)c >> 12));
		_putc(0x80 | ((uint8_t)((uint16_t)c >> 6) & 0x3f));
	}
	_putc(0x80 | ((uint8_t)c & 0x3f));
}

static void _putws(const wchar_t *str)
{
    while (*str) {
        _putwc(*str++);
	}
}

//	LFSR で乱数生成
static unsigned int _rand(void)
{
	static unsigned int    lfsr = 1234;
	
	lfsr = (lfsr >> 1) ^ (-(int)(lfsr & 1) & 0xb400);
	return lfsr;
}

/*
    Timer1 設定
*/
ISR(TIMER1_COMPA_vect)
{
	unsigned int	r;
	
	r = _rand();
	
	//	成句間隔を変化
	OCR1A	= (r & 0x7fff) + 31249;
	TCNT1	= 0;

    //	俳句の生成
    _putwc('\t');

	_putws(upper[r & 7]);
    _putws(middle[(r >> 3) & 7]);
    _putws(lower[(r >> 6) & 7]);

    _putwc('\n');
    _putwc('\n');
}

static void timer_init()
{
    TCCR1A  = 0;		// CTC
    TCCR1B  = 0x0d;		// 1/1024
    OCR1A   = (F_CPU >> 8) - 1;
    TIMSK1  |= 1 << OCIE1A;
}


int main(void)
{
    usart_init();
    timer_init();

    _putwc('\n');
    _putwc('\n');

    sei();
    for (;;) {
        wdt_reset();
    }

    return 0;
}

