
		EMB-88 サンプルプログラム集


│  Makefile
│  readme.txt
│  
├─demo
│      dice.c			さいころ
│      emb-88.c			出荷時サンプル
│      exp.c			指数
│      haiku.c			俳句
│      haiku_sj.c		俳句（Shift-JIS形式）
│      hanoi.c			ハノイの塔
│      kitt.c			ナイトライダー
│      Makefile
│      makeone.bat
│      melody.c			演奏
│      message.c		メッセージ表示
│      message_sj.c		メッセージ表示（Shift-JIS形式）
│      mul.c			九九表
│      music.c			音楽
│      number.c			数字表示
│      pi.c				円周率
│      queen.c			８クィーン問題
│      wave.c			ゆらゆら
│      
├─game
│      block.c			ブロック崩し
│      hockey.c			ホッケー
│      invader.c		インベーダー
│      main.c
│      Makefile
│      makeone.bat
│      reversi.c		オセロ
│      tetris.c			テトリス
│      user.h
│      
└─hex
        block.hex
        dice.hex
        emb-88.hex
        exp.hex
        haiku.hex
        hanoi.hex
        hockey.hex
        invader.hex
        kitt.hex
        melody.hex
        message.hex
        mul.hex
        music.hex
        number.hex
        pi.hex
        queen.hex
        reversi.hex
        tetris.hex
        wave.hex


(c)Copyright 2018 Osamu Tamura @ Recursion Co., Ltd. All rights reserved.
